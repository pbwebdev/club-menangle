<?php

/**
 * Actions and Filters
 *
 * @package Club Menangle
 * @since 0.1.0
 */

// Allow SVG upload
if ( ! function_exists( 'club_menangle_mime_types' ) ) {
	function club_menangle_mime_types( $mimes ) {
		$mimes['svg']  = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';
		return $mimes;
	}
	add_filter( 'upload_mimes', 'club_menangle_mime_types' );
}

// Add SVG as image
if ( ! function_exists( 'club_menangle_ext_types' ) ) {
	function club_menangle_ext_types( $mimes ) {
		$mimes['image'][] = 'svg';
		return $mimes;
	}
	add_filter( 'ext2type', 'club_menangle_ext_types' );
}

// Correctly identify SVGs
if ( ! function_exists( 'club_menangle_type_svg' ) ) {
	function club_menangle_type_svg( $data = null, $file = null, $filename = null ) {
		$ext = isset( $data['ext'] ) ? $data['ext'] : '';

		if ( strlen( $ext ) < 1 ) {
			$exploded = explode( '.', $filename );
			$ext      = strtolower( end( $exploded ) );
		}

		if ( 'svg' === $ext ) {
			$data['type'] = 'image/svg+xml';
			$data['ext']  = 'svg';
		} elseif ( 'svgz' === $ext ) {
			$data['type'] = 'image/svg+xml';
			$data['ext']  = 'svgz';
		}

		return $data;
	}
	add_filter( 'wp_check_filetype_and_ext', 'club_menangle_type_svg', 10, 3 );
}

// Number of revisions to keep
if ( ! function_exists( 'club_menangle_keep_revisions' ) ) {
	function club_menangle_keep_revisions( $num, $post ) {
		return 30;
	}
	add_filter( 'wp_revisions_to_keep', 'club_menangle_keep_revisions', 10, 2 );
}

// Remove WP icon from the admin bar.
if ( ! function_exists( 'club_menangle_remove_wp_icon' ) ) {
	function club_menangle_remove_wp_icon() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu( 'wp-logo' );
	}
	add_action( 'wp_before_admin_bar_render', 'club_menangle_remove_wp_icon' );
}

// Makes WordPress-generated emails appear 'from' the site name.
if ( ! function_exists( 'club_menangle_mail_from_name' ) ) {
	function club_menangle_mail_from_name() {
		return get_option( 'blogname' );
	}
	add_filter( 'wp_mail_from_name', 'club_menangle_mail_from_name' );
}

// Makes WordPress-generated emails appear 'from' the site admin email address.
if ( ! function_exists( 'club_menangle_wp_mail_from' ) ) {
	function club_menangle_wp_mail_from() {
		return get_option( 'admin_email' );
	}
	add_filter( 'wp_mail_from', 'club_menangle_wp_mail_from' );
}
