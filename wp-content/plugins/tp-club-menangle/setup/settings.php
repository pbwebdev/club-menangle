<?php

/**
 * Register settings
 *
 * @package Club Menangle
 * @since 0.1.0
 */

ThemePlate()->settings( array(
	'id'      => 'global',
	'title'   => __( 'Global Settings', 'club-menangle' ),
	'context' => 'normal',
	'fields'  => array(
		'logo' => array(
			'title' => __( 'Site Logo', 'club-menangle' ),
			'type'  => 'file',
		),
		'menu' => array(
			'title' => __( 'Burger Menu Text', 'club-menangle' ),
			'type'  => 'text',
			'default' => 'Menu',
		),
		'book' => array(
			'title' => __( 'Header Booking Text', 'club-menangle' ),
			'type'  => 'text',
			'default' => 'Bookings',
		),
		'events' => array(
			'title'    => __( 'Featured Events', 'club-menangle' ),
			'type'     => 'post',
			'multiple' => true,
			'options'  => array(
				'post_type' => 'event',
			),
		),
		'tickets' => array(
			'title' => __( 'All Events Page', 'club-menangle' ),
			'type'  => 'page',
		),
		'reservations' => array(
			'title'      => __( 'Reservations', 'club-menangle' ),
			'type'       => 'link',
			'repeatable' => true,
			'default'    => array(
				array(
					'url'    => '#',
					'text'   => 'Menangle Country Club',
					'target' => '_self',
				),
				array(
					'url'    => '#',
					'text'   => 'Club Menangle Trackside',
					'target' => '_self',
				),
			),
		),
		'map'  => array(
			'title' => __( 'Map API Key', 'club-menangle' ),
			'type'  => 'text',
		),
	),
) );

ThemePlate()->settings( array(
	'id'      => 'sponsors',
	'title'   => __( 'Sponsors Settings', 'club-menangle' ),
	'context' => 'normal',
	'hide_on' => array(
		'key' => '#cm-rebuild',
		'value' => CLUB_MENANGLE_REBUILD,
	),
	'fields'  => array(
		'heading' => array(
			'title' => __( 'Heading', 'club-menangle' ),
			'type'  => 'text',
		),
		'number'  => array(
			'title'       => __( 'Number', 'club-menangle' ),
			'description' => __( 'Show per slide', 'club-menangle' ),
			'type'        => 'number',
			'options'     => array(
				'min' => 3,
				'max' => 12,
			),
			'default'     => 8,
		),
	),
) );

ThemePlate()->settings( array(
	'id'      => 'instagram',
	'title'   => __( 'Instagram Settings', 'club-menangle' ),
	'context' => 'normal',
	'hide_on' => array(
		'key' => '#cm-rebuild',
		'value' => CLUB_MENANGLE_REBUILD,
	),
	'fields'  => array(
		'handle' => array(
			'title' => __( 'Handle', 'club-menangle' ),
			'type'  => 'text',
		),
		'token'  => array(
			'title' => __( 'Token', 'club-menangle' ),
			'type'  => 'text',
		),
	),
) );


$forms = array();

if ( class_exists( 'GFAPI' ) ) {
	foreach ( GFAPI::get_forms() as $form ) {
		$forms[ $form['id'] ] = $form['title'];
	}
}

ThemePlate()->settings( array(
	'id'      => 'subscribe',
	'title'   => __( 'Subscribe Settings', 'club-menangle' ),
	'context' => 'normal',
	'fields'  => array(
		'heading' => array(
			'title' => __( 'Heading', 'club-menangle' ),
			'type'  => 'text',
		),
		'content' => array(
			'title' => __( 'Content', 'club-menangle' ),
			'type'  => 'textarea',
		),
		'form'    => array(
			'title'   => __( 'Form', 'club-menangle' ),
			'type'    => 'select',
			'options' => $forms,
			'hide_on' => array(
				'key' => '#cm-rebuild',
				'value' => CLUB_MENANGLE_REBUILD,
			),
		),
		'logo'    => array(
			'title' => __( 'Logo', 'club-menangle' ),
			'type'  => 'file',
			'hide_on' => array(
				'key' => '#cm-rebuild',
				'value' => CLUB_MENANGLE_REBUILD,
			),
		),
		'link'    => array(
			'title'   => __( 'Link', 'club-menangle' ),
			'type'    => 'link',
			'show_on' => array(
				'key' => '#cm-rebuild',
				'value' => CLUB_MENANGLE_REBUILD,
			),
			'default' => array(
				'url'    => '#',
				'text'   => 'Subscribe',
				'target' => '_self',
			),
		),
	),
) );

ThemePlate()->settings( array(
	'id'      => 'contact',
	'title'   => __( 'Contact Settings', 'club-menangle' ),
	'context' => 'normal',
	'fields'  => array(
		'items' => array(
			'type'       => 'group',
			'repeatable' => true,
			'fields'     => array(
				'heading' => array(
					'title' => __( 'Venue', 'club-menangle' ),
					'type'  => 'text',
				),
				'content' => array(
					'title' => __( 'Content', 'club-menangle' ),
					'type'  => 'textarea',
				),
				'social' => array(
					'title'      => __( 'Socials', 'club-menangle' ),
					'type'       => 'group',
					'repeatable' => true,
					'fields'     => array(
						'provider' => array(
							'title'   => __( 'Provider', 'club-menangle' ),
							'type'    => 'select',
							'options' => array(
								'facebook'  => 'Facebook',
								'twitter'   => 'Twitter',
								'instagram' => 'Instagram',
								'linkedin'  => 'LinkedIn',
								'youtube'   => 'Youtube',
								'pinterest' => 'Pinterest',
								'x-twitter' => 'X - Twitter',
							),
						),
						'link'     => array(
							'title' => __( 'Link', 'club-menangle' ),
							'type'  => 'url',
						),
					),
				),
			),
		),
	),
) );

if ( CLUB_MENANGLE_REBUILD ) :
ThemePlate()->settings( array(
	'id'      => 'opening',
	'title'   => __( 'Opening Settings', 'club-menangle' ),
	'context' => 'normal',
	'fields'  => array(
		'heading' => array(
			'title'   => __( 'Heading', 'club-menangle' ),
			'type'    => 'text',
			'default' => 'Open Hours<br> Menangle Country Club',
		),
		'venue'   => array(
			'type'       => 'group',
			'repeatable' => true,
			'fields'     => array(
				'heading' => array(
					'title' => __( 'Venue', 'club-menangle' ),
					'type'  => 'text',
				),
				'hours'   => array(
					'title'      => __( 'Hours', 'club-menangle' ),
					'type'       => 'group',
					'repeatable' => true,
					'fields'     => array(
						'day'  => array(
							'title' => __( 'Day', 'club-menangle' ),
							'type'  => 'text',
						),
						'time' => array(
							'title' => __( 'Time', 'club-menangle' ),
							'type'  => 'text',
						),
					),
				),
			),
		),
	),
) );
endif;


ThemePlate()->settings( array(
	'id'          => 'google',
	'title'       => __( 'Google Codes', 'club-menangle' ),
	'description' => __( 'Enter the tracking IDs to use in site.', 'club-menangle' ),
	'context'     => 'side',
	'fields'      => array(
		'analytics'  => array(
			'title'       => __( 'Analytics', 'club-menangle' ),
			'description' => __( 'UA-XXXXX-Y', 'club-menangle' ),
			'type'        => 'text',
		),
		'tagmanager' => array(
			'title'       => __( 'Tag Manager', 'club-menangle' ),
			'description' => __( 'GTM-XXXX', 'club-menangle' ),
			'type'        => 'text',
		),
	),
) );

ThemePlate()->settings( array(
	'id'          => 'social',
	'title'       => __( 'Social Profiles', 'club-menangle' ),
	'description' => __( 'Enter the links to the associated service.', 'club-menangle' ),
	'context'     => 'normal',
	'hide_on' => array(
		'key' => '#cm-rebuild',
		'value' => CLUB_MENANGLE_REBUILD,
	),
	'fields'      => array(
		'profiles' => array(
			'title'      => __( 'Service', 'club-menangle' ),
			'type'       => 'group',
			'repeatable' => true,
			'fields'     => array(
				'provider' => array(
					'title'   => __( 'Provider', 'club-menangle' ),
					'type'    => 'select',
					'options' => array(
						'facebook'    => 'Facebook',
						'twitter'     => 'Twitter',
						'instagram'   => 'Instagram',
						'linkedin'    => 'LinkedIn',
						'youtube'     => 'Youtube',
						'pinterest'   => 'Pinterest',
						'google-plus' => 'Google+',
						'x-twitter'   => 'X - Twitter',
					),
				),
				'link'     => array(
					'title' => __( 'Link', 'club-menangle' ),
					'type'  => 'url',
				),
			),
		),
	),
) );
