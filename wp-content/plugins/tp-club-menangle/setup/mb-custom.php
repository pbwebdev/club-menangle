<?php

/**
 * Register meta boxes
 *
 * @package Club Menangle
 * @since 0.1.0
 */

ThemePlate()->post_meta(
	array(
		'id'       => 'venue',
		'title'    => __( 'Venue Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => array(
				'sub-restaurants.php',
				'page-functions.php',
			),
		),
		'fields'   => array(
			'links'   => array(
				'title'      => __( 'Links', 'club-menangle' ),
				'type'       => 'link',
				'repeatable' => true,
				'default'    => array(
					array(
						'url'    => '#',
						'text'   => 'Item #1',
						'target' => '_self',
					),
					array(
						'url'    => '#',
						'text'   => 'Item #2',
						'target' => '_self',
					),
					array(
						'url'    => '#',
						'text'   => 'Item #3',
						'target' => '_self',
					),
				),
			),
			'info'   => array(
				'title'      => __( 'Infos', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'sub-restaurants.php',
					),
				),
				'fields'     => array(
					'icon' => array(
						'title' => __( 'FA Icon', 'club-menangle' ),
						'type'  => 'text',
					),
					'heading' => array(
						'title'   => __( 'Heading', 'club-menangle' ),
						'type'    => 'text',
					),
					'content' => array(
						'title' => __( 'Content', 'club-menangle' ),
						'type'  => 'textarea',
					),
					'link'    => array(
						'title' => __( 'Link', 'club-menangle' ),
						'type'  => 'link',
					),
				),
			),
			'images'  => array(
				'title'    => __( 'Images', 'club-menangle' ),
				'type'     => 'file',
				'multiple' => true,
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'sub-restaurants.php',
					),
				),
			),
			'heading' => array(
				'title'   => __( 'Heading', 'club-menangle' ),
				'type'    => 'text',
				'default' => 'About Club Menangle'
			),
			'content' => array(
				'title' => __( 'Content', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'image'   => array(
				'title' => __( 'Image', 'club-menangle' ),
				'type'  => 'file',
				'hide_on'  => array(
					'key'   => 'template',
					'value' => array(
						'sub-restaurants.php',
						'page-functions.php',
					),
				),
			),
			'video'   => array(
				'title' => __( 'Video', 'club-menangle' ),
				'type'  => 'url',
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'page-functions.php',
					),
				),
			),
			'button'  => array(
				'title'  => __( 'Button', 'club-menangle' ),
				'type'   => 'group',
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'sub-restaurants.php',
					),
				),
				'fields' => array(
					'url'  => array(
						'title'   => __( 'URL', 'club-menangle' ),
						'type'    => 'url',
						'default' => 'https://' . $_SERVER['SERVER_NAME'],
					),
					'text' => array(
						'title'   => __( 'Text', 'club-menangle' ),
						'type'    => 'text',
						'default' => 'Virtual Tour',
					),
				),
			),
			'spaces'       => array(
				'title'      => __( 'Spaces', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'page-functions.php',
					),
				),
				'fields'     => array(
					'heading' => array(
						'title'   => __( 'Heading', 'club-menangle' ),
						'type'    => 'text',
					),
					'info'   => array(
						'title'      => __( 'Info', 'club-menangle' ),
						'type'       => 'group',
						'repeatable' => true,
						'fields'     => array(
							'icon' => array(
								'title' => __( 'FA Icon', 'club-menangle' ),
								'type'  => 'text',
							),
							'lines' => array(
								'title' => __( 'Lines', 'club-menangle' ),
								'type'  => 'textarea',
							),
						),
					),
					'content' => array(
						'title' => __( 'Content', 'club-menangle' ),
						'type'  => 'textarea',
					),
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
					'button'  => array(
						'title'   => __( 'Button', 'club-menangle' ),
						'type'    => 'link',
						'default' => array(
							'url'    => '#',
							'text'   => 'Enquire Now',
							'target' => '_self',
						),
					),
				),
			),
			'tour' => array(
				'title'    => __( 'Virtual Tour', 'club-menangle' ),
				'type'     => 'group',
				'show_on'  => array(
					'key'   => 'template',
					'value' => 'page-functions.php',
				),
				'fields'     => array(
					'image' => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
					'link'  => array(
						'title' => __( 'Link', 'club-menangle' ),
						'type'  => 'url',
					),
				),
			),
			'gallery'      => array(
				'title'    => __( 'Gallery', 'club-menangle' ),
				'type'     => 'file',
				'multiple' => true,
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'page-functions.php',
					),
				),
			),
			'testimonials' => array(
				'title'      => __( 'Testimonials', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'show_on'  => array(
					'key'   => 'template',
					'value' => array(
						'page-functions.php',
					),
				),
				'fields'     => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title' => __( 'Content', 'club-menangle' ),
						'type'  => 'textarea',
					),
					'person'  => array(
						'title' => __( 'Person', 'club-menangle' ),
						'type'  => 'text',
					),
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
				),
			),
			'events'   => array(
				'title' => __( 'Events', 'club-menangle' ),
				'type'  => 'select',
				'none'  => true,
				'options' => array_column(
					array_map(
						function( $value ) {
							return compact( 'value' );
						},
						club_menangle_values( 'cmau_event_venue', 'event' )
					),
					'value',
					'value'
				),
			),
		),
	)
);
