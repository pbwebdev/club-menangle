<?php

/**
 * Register meta boxes
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$forms = array();

if ( class_exists( 'GFAPI' ) ) {
	foreach ( GFAPI::get_forms() as $form ) {
		$forms[ $form['id'] ] = $form['title'];
	}
}


ThemePlate()->post_meta(
	array(
		'id'       => 'banner',
		'title'    => __( 'Banner Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page', 'event', 'post' ),
		'fields'   => array(
			'slides' => array(
				'title'      => __( 'Slides', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'fields'    => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title' => __( 'Content', 'club-menangle' ),
						'type'  => 'textarea',
					),
					'button'  => array(
						'title' => __( 'Button', 'club-menangle' ),
						'type'  => 'link',
					),
					'image'  => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
				),
			),
			'super'   => array(
				'title' => __( 'Super', 'club-menangle' ),
				'type'  => 'text',
				'hide_on' => array(
					'key' => '#cm-rebuild',
					'value' => CLUB_MENANGLE_REBUILD,
				),
			),
			'heading' => array(
				'title' => __( 'Heading', 'club-menangle' ),
				'type'  => 'text',
			),
			'content' => array(
				'title' => __( 'Content', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'button'  => array(
				'title'  => __( 'Button', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'url'  => array(
						'title' => __( 'URL', 'club-menangle' ),
						'type'  => 'url',
					),
					'text' => array(
						'title' => __( 'Text', 'club-menangle' ),
						'type'  => 'text',
					),
				),
			),
			'images'  => array(
				'title'       => __( 'Images', 'club-menangle' ),
				'description' => __( 'Falls back to Featured Image set', 'club-menangle' ),
				'type'        => 'file',
				'multiple'    => true,
			),
			'scroll' => array(
				'title'   => __( 'Enable Scroll Down?', 'club-menangle' ),
				'type'    => 'radio',
				'options' => array( 'Yes', 'No' ),
				'default' => 1,
				'hide_on' => array(
					'key' => '#cm-rebuild',
					'value' => CLUB_MENANGLE_REBUILD,
				),
			),
		),
	)
);

if ( CLUB_MENANGLE_REBUILD ) :
require_once 'mb-custom.php';
endif;

ThemePlate()->post_meta(
	array(
		'id'       => 'zigzag',
		'title'    => __( 'Zig Zag Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page'),
		// 'hide_on' => array(
		// 	'key' => '#cm-rebuild',
		// 	'value' => CLUB_MENANGLE_REBUILD,
		// ),
		'fields'   => array(
			'function_button'   => array(
				'title'      => __( 'Custom Link', 'club-menangle' ),
				'description' => __( 'Link for zigzag item', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => false,
				'fields'     => array(
					'link'    => array(
						'title'  => __( 'Link', 'club-menangle' ),
						'type'   => 'group',
						'fields' => array(
							'url'  => array(
								'title' => __( 'URL', 'club-menangle' ),
								'type'  => 'url',
							),
							'text' => array(
								'title' => __( 'Text', 'club-menangle' ),
								'type'  => 'text',
							),
						),
					),
				),
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'flipbook',
		'title'    => __( '3D FlipBook', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page'),
		'hide_on' => array(
			'key' => '#cm-rebuild',
			'value' => CLUB_MENANGLE_REBUILD,
		),
		'fields'   => array(
			'flipbook_code'   => array(
				'title'      => __( 'Flipbook', 'club-menangle' ),
				'description' => __( 'Shortcode Generated', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => false,
				'fields'     => array(
					'code'    => array(
						'title'  => __( 'Shortcode', 'club-menangle' ),
						'type'   => 'group',
						'fields' => array(
							'text' => array(
								'title' => __( 'Code', 'club-menangle' ),
								'type'  => 'textarea',
							),
						),
					),
				),
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'event',
		'title'    => __( 'Event Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'event' ),
		'fields'   => array(
			'venue'     => array(
				'title' => __( 'Venue', 'club-menangle' ),
				'type'  => 'text',
			),
			'date'     => array(
				'title' => __( 'Date', 'club-menangle' ),
				'type'  => 'date',
			),
			'time'     => array(
				'title' => __( 'Time', 'club-menangle' ),
				'type'  => 'time',
			),
			'location' => array(
				'title' => __( 'Location', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'sticky' => array(
				'title'   => __( 'Sticky Event?', 'club-menangle' ),
				'type'    => 'radio',
				'options' => array( 'Yes', 'No' ),
				'show_on' => array(
					'key' => '#cm-rebuild',
					'value' => CLUB_MENANGLE_REBUILD,
				),
			),
			'price'    => array(
				'title' => __( 'Pricing', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'book'     => array(
				'title' => __( 'Book URL', 'club-menangle' ),
				'type'  => 'url',
			),
			'transport' => array(
				'title' => __( 'Transport Button', 'club-menangle' ),
				'type'  => 'link',
			),
			'sections'  => array(
				'title'      => __( 'Extra Sections', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'fields' => array(
					'info'    => array(
						'title' => __( 'Info', 'club-menangle' ),
						'type'  => 'editor',
						'hide_on' => array(
							'key' => '#cm-rebuild',
							'value' => CLUB_MENANGLE_REBUILD,
						),
					),
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title'   => __( 'Content', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
					'link'    => array(
						'title'   => __( 'Button', 'club-menangle' ),
						'type'    => 'link',
					),
				),
			),
			'video'   => array(
				'title' => __( 'Video', 'club-menangle' ),
				'type'  => 'url',
			),
			'packages' => array(
				'title'  => __( 'Packages', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'items'   => array(
						'title'    => __( 'Items', 'club-menangle' ),
						'type'     => 'post',
						'options'  => 'package',
						'multiple' => true,
					),
				),
			),
			'gallery' => array(
				'title'  => __( 'Gallery', 'club-menangle' ),
				'type'     => 'file',
				'multiple' => true,
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'package',
		'title'    => __( 'Package Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'package' ),
		'show_on'  => array(
			'key' => '#cm-rebuild',
			'value' => CLUB_MENANGLE_REBUILD,
		),
		'fields'   => array(
			'location' => array(
				'title' => __( 'Venue', 'club-menangle' ),
				'type'  => 'text',
			),
			'date'     => array(
				'title' => __( 'Date', 'club-menangle' ),
				'type'  => 'date',
			),
			'time'     => array(
				'title' => __( 'Time', 'club-menangle' ),
				'type'  => 'time',
			),
			'price'    => array(
				'title' => __( 'Pricing', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'book'     => array(
				'title' => __( 'Book URL', 'club-menangle' ),
				'type'  => 'url',
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'eventbrite',
		'title'    => __( 'Eventbrite Widget', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'event' ),
		'hide_on' => array(
			'key' => '#cm-rebuild',
			'value' => CLUB_MENANGLE_REBUILD,
		),
		'fields'   => array(
			'id'      => array(
				'title' => __( 'Event ID', 'club-menangle' ),
				'type'  => 'text',
			),
			'heading' => array(
				'title'   => __( 'Heading', 'club-menangle' ),
				'type'    => 'text',
				'default' => __( 'Tickets by Eventbrite', 'club-menangle' ),
			),
			'content' => array(
				'title'   => __( 'Content', 'club-menangle' ),
				'type'    => 'editor',
				'options' => array(
					'editor_height' => 200,
				),
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'featured',
		'title'    => __( 'Featured Image', 'club-menangle' ),
		'context'  => 'side',
		'priority' => 'high',
		'screen'   => array( 'event' ),
		'fields'   => array(
			'overlay' => array(
				'title' => __( 'Overlay Content', 'club-menangle' ),
				'type'  => 'textarea',
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'director',
		'title'    => __( 'Director', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'director' ),
		'fields'   => array(
			'position' => array(
				'title'  => __( 'Position', 'club-menangle' ),
				'type'   => 'text',
				'column' => true,
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'featured',
		'title'    => __( 'Featured Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'id',
			'value' => get_option( 'page_on_front' ),
		),
		'fields'   => array(
			'supers' => array(
				'title'      => __( 'Super', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'maximum'    => 2,
				'fields'     => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title' => __( 'Content', 'club-menangle' ),
						'type'  => 'editor',
					),
					'link'    => array(
						'title' => __( 'Link', 'club-menangle' ),
						'type'  => 'url',
						'none'  => true,
					),
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
				),
			),
			'events' => array(
				'title'  => __( 'Events', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'title' => array(
						'title'   => __( 'Title', 'club-menangle' ),
						'type'    => 'text',
						'default' => 'What\'s On',
					),
					'items'   => array(
						'title'    => __( 'Items', 'club-menangle' ),
						'type'     => 'post',
						'multiple' => true,
						'options'  => array(
							'post_type' => 'event',
						),
					),
					'button'  => array(
						'title'   => __( 'Button Text', 'club-menangle' ),
						'type'    => 'text',
						'default' => 'See All Events',
					),
				),
			),
			'spaces' => array(
				'title'      => __( 'Spaces', 'club-menangle' ),
				'type'       => 'group',
				'fields'     => array(
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
					'button'  => array(
						'title'   => __( 'Button', 'club-menangle' ),
						'type'    => 'link',
						'default' => array(
							'url'    => '#',
							'text'   => 'Event Spaces',
							'target' => '_self',
						),
					),
					'items'   => array(
						'title'      => __( 'Items', 'club-menangle' ),
						'type'       => 'group',
						'repeatable' => true,
						'fields'     => array(
							'title' => array(
								'title'   => __( 'Title', 'club-menangle' ),
								'type'    => 'text',
							),
							'heading' => array(
								'title'   => __( 'Heading', 'club-menangle' ),
								'type'    => 'text',
							),
							'content' => array(
								'title' => __( 'Content', 'club-menangle' ),
								'type'  => 'textarea',
							),
						),
					),
				),
			),
			'news' => array(
				'title'  => __( 'News', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'title' => array(
						'title'   => __( 'Title', 'club-menangle' ),
						'type'    => 'text',
						'default' => 'Featured News',
					),
					'button'  => array(
						'title'   => __( 'Button Text', 'club-menangle' ),
						'type'    => 'text',
						'default' => 'See All News',
					),
				),
			),
			'about'  => array(
				'title' => __( 'About', 'club-menangle' ),
				'type'  => 'page',
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'contact',
		'title'    => __( 'Contact Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => 'contact.php',
		),
		'fields'   => array(
			'phone'   => array(
				'title' => __( 'Phone', 'club-menangle' ),
				'type'  => 'text',
			),
			'time'    => array(
				'title' => __( 'Time', 'club-menangle' ),
				'type'  => 'text',
			),
			'address' => array(
				'title' => __( 'Address', 'club-menangle' ),
				'type'  => 'text',
			),
			'uber'    => array(
				'title'  => __( 'Uber Mobile', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'deeplink' => array(
						'title' => __( 'Deep Link', 'club-menangle' ),
						'type'  => 'url',
					),
					'linktext' => array(
						'title'   => __( 'Link Text', 'club-menangle' ),
						'type'    => 'text',
						'default' => __( 'Book an Uber', 'club-menangle' ),
					),
				),
			),
			'postal'  => array(
				'title' => __( 'Postal', 'club-menangle' ),
				'type'  => 'text',
			),
			'email'   => array(
				'title' => __( 'Email', 'club-menangle' ),
				'type'  => 'text',
			),
		),
	)
);

ThemePlate()->post_meta(
	array(
		'id'       => 'map',
		'title'    => __( 'Map Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => 'contact.php',
		),
		'fields'   => array(
			'lat' => array(
				'title' => __( 'Latitude', 'club-menangle' ),
				'type'  => 'text',
			),
			'lng' => array(
				'title' => __( 'Longitude', 'club-menangle' ),
				'type'  => 'text',
			),
			'zml' => array(
				'title' => __( 'Zoom', 'club-menangle' ),
				'type'  => 'number',
			),
		),
	)
);

ThemePlate()->post_meta( array(
	'id'       => 'enquiry',
	'title'    => __( 'Enquiry Settings', 'club-menangle' ),
	'context'  => 'normal',
	'priority' => 'high',
	'screen'   => array( 'page' ),
	'show_on'  => array(
		'key'   => 'template',
		'value' => 'contact.php',
	),
	'fields'   => array(
		'image'   => array(
			'title' => __( 'Image', 'club-menangle' ),
			'type'  => 'file',
		),
		'heading' => array(
			'title' => __( 'Heading', 'club-menangle' ),
			'type'  => 'text',
		),
		'form'    => array(
			'title'   => __( 'Form', 'club-menangle' ),
			'type'    => 'select',
			'options' => $forms,
		),
	),
) );

ThemePlate()->post_meta(
	array(
		'id'       => 'about',
		'title'    => __( 'About Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => 'about.php',
		),
		'fields'   => array(
			'sections' => array(
				'title'      => __( 'Extra Sections', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'minimum'    => 3,
				'maximum'    => 3,
				'fields'     => array(
					'image'   => array(
						'title' => __( 'Image', 'club-menangle' ),
						'type'  => 'file',
					),
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title'   => __( 'Content', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
				),
			),
		),
	)
);


ThemePlate()->post_meta(
	array(
		'id'       => 'page',
		'title'    => __( 'Page Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => array(
				'restaurants.php',
				'sub-functions.php',
				'sub-restaurants.php',
			),
		),
		'fields'   => array(
			'sections'   => array(
				'title'      => __( 'Extra Sections', 'club-menangle' ),
				'type'       => 'group',
				'repeatable' => true,
				'minimum'    => 1,
				'maximum'    => 8,
				'fields'     => array(
					'image'   => array(
						'title'   => __( 'Image', 'club-menangle' ),
						'type'    => 'file',
						'show_on' => array(
							'key'   => 'template',
							'value' => array(
								'sub-functions.php',
								'sub-restaurants.php',
							),
						),
					),
					'images'  => array(
						'title'    => __( 'Images', 'club-menangle' ),
						'type'     => 'file',
						'multiple' => true,
						'show_on'  => array(
							'key'   => 'template',
							'value' => 'restaurants.php',
						),
					),
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title'   => __( 'Content', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
					'link'    => array(
						'title'  => __( 'Link', 'club-menangle' ),
						'type'   => 'group',
						'fields' => array(
							'url'  => array(
								'title' => __( 'URL', 'club-menangle' ),
								'type'  => 'url',
							),
							'text' => array(
								'title' => __( 'Text', 'club-menangle' ),
								'type'  => 'text',
							),
						),
					),
				),
			),
			'gallery'    => array(
				'title'   => __( 'Gallery', 'club-menangle' ),
				'type'    => 'group',
				'fields'  => array(
					'heading' => array(
						'title'   => __( 'Heading', 'club-menangle' ),
						'type'    => 'text',
					),
					'images'  => array(
						'title'    => __( 'Images', 'club-menangle' ),
						'type'     => 'file',
						'multiple' => true,
					),
				),
				'show_on' => array(
					'key'   => 'template',
					'value' => array(
						'sub-functions.php',
					),
				),
			),
			'contact'    => array(
				'title'   => __( 'Contact', 'club-menangle' ),
				'type'    => 'group',
				'fields'  => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title'   => __( 'Content', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
					'form'    => array(
						'title'   => __( 'Form', 'club-menangle' ),
						'type'    => 'select',
						'options' => $forms,
						'show_on' => array(
							'key'   => 'template',
							'value' => 'sub-functions.php',
						),
						'hide_on' => array(
							'key' => '#cm-rebuild',
							'value' => CLUB_MENANGLE_REBUILD,
						),
					),
					'label'    => array(
						'title'   => __( 'Book Label', 'club-menangle' ),
						'type'    => 'text',
						'show_on' => array(
							'key'   => 'template',
							'value' => 'sub-restaurants.php',
						),
						'hide_on' => array(
							'key' => '#cm-rebuild',
							'value' => CLUB_MENANGLE_REBUILD,
						),
					),
					'book'    => array(
						'title'   => __( 'Book Link', 'club-menangle' ),
						'type'    => 'url',
						'show_on' => array(
							'key'   => 'template',
							'value' => 'sub-restaurants.php',
						),
						'hide_on' => array(
							'key' => '#cm-rebuild',
							'value' => CLUB_MENANGLE_REBUILD,
						),
					),
					'map'     => array(
						'title'   => __( 'Map', 'club-menangle' ),
						'type'    => 'group',
						'fields'  => array(
							'lat' => array(
								'title' => __( 'Latitude', 'club-menangle' ),
								'type'  => 'text',
							),
							'lng' => array(
								'title' => __( 'Longitude', 'club-menangle' ),
								'type'  => 'text',
							),
							'zml' => array(
								'title' => __( 'Zoom', 'club-menangle' ),
								'type'  => 'number',
							),
						),
						'show_on' => array(
							'key'   => 'template',
							'value' => 'sub-restaurants.php',
						),
						'hide_on' => array(
							'key' => '#cm-rebuild',
							'value' => CLUB_MENANGLE_REBUILD,
						),
					),
				),
				'show_on' => array(
					'key'   => 'template',
					'value' => array(
						'sub-functions.php',
						'sub-restaurants.php',
					),
				),
			),
			'sportsyear' => array(
				'title'   => __( 'Sports Year', 'club-menangle' ),
				'type'    => 'group',
				'fields'  => array(
					'intro' => array(
						'title'   => __( 'Intro', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
					'code'  => array(
						'title' => __( 'Code', 'club-menangle' ),
						'type'  => 'textarea',
					),
				),
				'show_on' => array(
					'key'   => 'template',
					'value' => 'sub-restaurants.php',
				),
			),
			'thefork'    => array(
				'title'       => __( 'TheFork', 'club-menangle' ),
				'description' => __( 'Widget Code', 'club-menangle' ),
				'type'        => 'textarea',
				'show_on'     => array(
					'key'   => 'template',
					'value' => 'sub-restaurants.php',
				),
			),
		),
	)
);


ThemePlate()->post_meta(
	array(
		'id'       => 'function',
		'title'    => __( 'Function Carousel', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => array(
				'sub-restaurants.php'
			),
		),
		'fields'   => array(
			'carousel' => array(
				'type'   => 'group',
				'fields' => array(
					'heading' => array(
						'title' => __( 'Heading', 'club-menangle' ),
						'type'  => 'text',
					),
					'content' => array(
						'title'   => __( 'Content', 'club-menangle' ),
						'type'    => 'editor',
						'options' => array(
							'editor_height' => 200,
						),
					),
					'link'    => array(
						'title' => __( 'Link', 'club-menangle' ),
						'type'  => 'link',
						'none'  => true,
					),
					'images'  => array(
						'title'    => __( 'Images', 'club-menangle' ),
						'type'     => 'file',
						'multiple' => true,
					),
				),
			),
		),
	)
);


ThemePlate()->post_meta(
	array(
		'id'       => 'download',
		'title'    => __( 'Download Links', 'club-menangle' ),
		'context'  => 'side',
		'priority' => 'high',
		'screen'   => array( 'page' ),
		'show_on'  => array(
			'key'   => 'template',
			'value' => array(
				'sub-restaurants.php'
			),
		),
		'fields'   => array(
			'links' => array(
				'type'       => 'group',
				'repeatable' => true,
				'fields'     => array(
					'url'  => array(
						'title' => __( 'URL', 'club-menangle' ),
						'type'  => 'url',
					),
					'text' => array(
						'title' => __( 'Text', 'club-menangle' ),
						'type'  => 'text',
					),
				),
			),
		),
	)
);


ThemePlate()->post_meta(
	array(
		'id'       => 'sponsor',
		'title'    => __( 'Sponsor Settings', 'club-menangle' ),
		'screen'   => array( 'sponsor' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			'logo' => array(
				'title'   => __( 'Logo Size', 'club-menangle' ),
				'type'    => 'radio',
				'options' => array( '(450) Boxed', '(600) Wide' ),
				'default' => 1,
			),
			'link' => array(
				'title' => __( 'URL Link', 'club-menangle' ),
				'type'  => 'url',
			),
		),
	)
);
