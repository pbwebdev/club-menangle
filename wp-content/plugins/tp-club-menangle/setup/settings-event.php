<?php

/**
 * Register settings
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( CLUB_MENANGLE_REBUILD ) :
ThemePlate()->settings(
	array(
		'id'       => 'archive',
		'title'    => __( 'Archive Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'page'     => 'event',
		'fields'   => array(
			'images'   => array(
				'title' => __( 'Banner Image', 'club-menangle' ),
				'type'  => 'file',
			),
			'heading'  => array(
				'title'   => __( 'Heading', 'club-menangle' ),
				'type'    => 'text',
				'default' => 'What\'s On at Club Menangle',
			),
			'featured' => array(
				'title'   => __( 'Featured', 'club-menangle' ),
				'type'    => 'number',
				'options' => array(
					'min' => 1
				),
				'default' => 3
			),
		),
	)
);
else :
ThemePlate()->settings(
	array(
		'id'       => 'banner',
		'title'    => __( 'Banner Settings', 'club-menangle' ),
		'context'  => 'normal',
		'priority' => 'high',
		'page'     => 'event',
		'fields'   => array(
			'image'   => array(
				'title'    => __( 'Images', 'club-menangle' ),
				'type'     => 'file',
				'multiple' => true,
			),
			'heading' => array(
				'title' => __( 'Heading', 'club-menangle' ),
				'type'  => 'text',
			),
			'content' => array(
				'title' => __( 'Content', 'club-menangle' ),
				'type'  => 'textarea',
			),
			'button'  => array(
				'title'  => __( 'Button', 'club-menangle' ),
				'type'   => 'group',
				'fields' => array(
					'url'  => array(
						'title' => __( 'URL', 'club-menangle' ),
						'type'  => 'url',
					),
					'text' => array(
						'title' => __( 'Text', 'club-menangle' ),
						'type'  => 'text',
					),
				),
			),
		),
	)
);
endif;


$forms = array();

if ( class_exists( 'GFAPI' ) ) {
	foreach ( GFAPI::get_forms() as $form ) {
		$forms[ $form['id'] ] = $form['title'];
	}
}

ThemePlate()->settings( array(
	'id'       => 'enquiry',
	'title'    => __( 'Enquiry Settings', 'club-menangle' ),
	'context'  => 'normal',
	'priority' => 'high',
	'page'     => 'event',
	'fields'   => array(
		'images'  => array(
			'title'    => __( 'Images', 'club-menangle' ),
			'type'     => 'file',
			'multiple' => true,
		),
		'heading' => array(
			'title' => __( 'Heading', 'club-menangle' ),
			'type'  => 'text',
		),
		'form'    => array(
			'title'   => __( 'Form', 'club-menangle' ),
			'type'    => 'select',
			'options' => $forms,
		),
	),
) );
