<?php

/**
 * Register custom post types
 *
 * @package Club Menangle
 * @since 0.1.0
 */

add_post_type_support( 'page', 'excerpt' );

add_action( 'init', function () {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;

	$labels->name = 'News';
	$labels->singular_name = 'News Item';
	$labels->add_new = 'Add New Item';
	$labels->add_new_item = 'Add New Item';
	$labels->edit_item = 'Edit News';
	$labels->new_item = 'New Item';
	$labels->view_item = 'View News';
	$labels->update_item = 'Update News';
	$labels->search_items = 'News';
	$labels->not_found = 'News not found';
	$labels->not_found_in_trash = 'News not found in Trash';
	$labels->parent_item_colon = 'Parent News:';
	$labels->all_items = 'All News';
	$labels->archives = 'News Archives';
	$labels->insert_into_item = 'Insert into News';
	$labels->uploaded_to_this_item = 'Uploaded to this News';
	$labels->featured_image = 'News Featured Image';
	$labels->set_featured_image = 'Set News Featured Image';
	$labels->remove_featured_image = 'Remove News Featured Image';
	$labels->use_featured_image = 'Use as News Featured Image';
	$labels->menu_name = 'News';
	$labels->name_admin_bar = 'News';
});

ThemePlate()->post_type( array(
	'name'     => 'event',
	'plural'   => __( 'Events', 'club-menangle' ),
	'singular' => __( 'Event', 'club-menangle' ),
	'args'     => array(
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-calendar',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'has_archive'   => true,
		'labels'        => array(
			'archives' => __( 'What\'s On', 'club-menangle' ),
		),
		'rewrite'       => array(
			'slug'       => 'events',
			'with-front' => false,
		),
	),
) );

if ( CLUB_MENANGLE_REBUILD ) :
ThemePlate()->taxonomy( array(
	'name'     => 'event-category',
	'plural'   => __( 'Categories', 'club-menangle' ),
	'singular' => __( 'Category', 'club-menangle' ),
	'type'     => 'event',
	'args'     => array(
		'public'  => false,
		'show_ui' => true,
	),
) );
endif;


ThemePlate()->column( array(
	'id'        => 'event-date',
	'title'     => 'Event Date',
	'post_type' => 'event',
	'position'  => 2,
	'callback'  => function( $post_id ) {
		$date = get_post_meta( $post_id, 'cmau_event_date', true );

		if ( ! $date ) {
			$date = get_the_date( 'Y-m-d', $post_id );
		}

		echo date( 'l jS F Y', strtotime( $date ) );
	},
) );

if ( CLUB_MENANGLE_REBUILD ) :
ThemePlate()->column( array(
	'id'        => 'event-category',
	'title'     => 'Category',
	'post_type' => 'event',
	'position'  => 3,
	'callback'  => function( $post_id ) {
		$terms = get_the_term_list( $post_id, 'event-category', '', ', ' );

		if ( get_post_meta( $post_id, 'cmau_event_sticky', true ) === '1' ) {
			echo 'Meta: Sticky';
			echo '<br><br>';
		} elseif ( false === $terms || is_wp_error( $terms ) ) {
			$terms = '&mdash;';
		}

		echo $terms;
	},
) );
endif;


ThemePlate()->post_type( array(
	'name'     => 'director',
	'plural'   => __( 'Directors', 'club-menangle' ),
	'singular' => __( 'Director', 'club-menangle' ),
	'args'     => array(
		'public'        => false,
		'show_ui'       => true,
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-businessman',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'revisions' ),
	),
) );


ThemePlate()->post_type( array(
	'name'     => 'sponsor',
	'plural'   => __( 'Sponsors', 'club-menangle' ),
	'singular' => __( 'Sponsor', 'club-menangle' ),
	'args'     => array(
		'public'        => false,
		'show_ui'       => true,
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-star-filled',
		'supports'      => array( 'title', 'thumbnail', 'revisions' ),
	)
) );


ThemePlate()->post_type( array(
	'name'     => 'package',
	'plural'   => __( 'Packages', 'club-menangle' ),
	'singular' => __( 'Package', 'club-menangle' ),
	'args'     => array(
		'has_archive'   => true,
		'rewrite'       => array(
			'slug'       => 'packages',
			'with-front' => false,
		),
		'labels'        => array(
			'archives' => __( 'Packages', 'club-menangle' ),
		),
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-portfolio',
		'supports'      => array( 'title', 'thumbnail', 'editor', 'revisions' ),
	)
) );
