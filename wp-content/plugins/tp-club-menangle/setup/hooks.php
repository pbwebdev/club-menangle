<?php

/**
 * Theme Hooks
 *
 * @package Club Menangle
 * @since 0.1.0
 */

require_once CLUB_MENANGLE_PLUGIN_PATH . 'includes/google-tracking-codes.php';

if ( ! function_exists( 'club_menangle_add_ga' ) ) {
	function club_menangle_add_ga() {
		$tid = club_menangle_options( 'google_analytics' );

		if ( $tid ) {
			themeplate_google_analytics_gtag( $tid );
		}
	}
	add_action( 'wp_head', 'club_menangle_add_ga', 5 );
}

if ( ! function_exists( 'club_menangle_add_gtm_head' ) ) {
	function club_menangle_add_gtm_head() {
		$tid = club_menangle_options( 'google_tagmanager' );

		if ( $tid ) {
			themeplate_google_tag_head( $tid );
		}
	}
	add_action( 'wp_head', 'club_menangle_add_gtm_head', 5 );
}

if ( ! function_exists( 'club_menangle_add_gtm_body' ) ) {
	function club_menangle_add_gtm_body() {
		$tid = club_menangle_options( 'google_tagmanager' );

		if ( $tid ) {
			themeplate_google_tag_body( $tid );
		}
	}
	add_action( 'wp_body_open', 'club_menangle_add_gtm_body', 5 );
}
