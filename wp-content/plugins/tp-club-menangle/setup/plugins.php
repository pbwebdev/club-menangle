<?php

/**
 * Require Plugins
 *
 * @package Club Menangle
 * @since 0.1.0
 */

// TGM Plugin Activation
require_once CLUB_MENANGLE_PLUGIN_PATH . 'includes/class-tgm-plugin-activation.php';

if ( ! function_exists( 'club_menangle_plugins' ) ) {
	function club_menangle_plugins() {
		$plugins = array(
			array(
				'name'             => 'ThemePlate',
				'slug'             => 'themeplate',
				'required'         => true,
				'source'           => 'https://github.com/kermage/ThemePlate/releases/download/v3.19.2/themeplate.zip',
				'force_activation' => true,
			),
			array(
				'name'             => 'Gravity Forms',
				'slug'             => 'gravityforms',
				'source'           => CLUB_MENANGLE_PLUGIN_PATH . 'includes/gravityforms.zip',
				'required'         => true,
				'force_activation' => true,
			),
			array(
				'name'             => 'FacetWP',
				'slug'             => 'facetwp',
				'source'           => CLUB_MENANGLE_PLUGIN_PATH . 'includes/facetwp.zip',
				'required'         => true,
				'force_activation' => true,
			),
			array(
				'name'             => 'WCAG 2.0 form fields for Gravity Forms',
				'slug'             => 'gravity-forms-wcag-20-form-fields',
				'required'         => true,
				'force_activation' => true,
			),
			array(
				'name'             => 'Activity Log',
				'slug'             => 'aryo-activity-log',
				'required'         => true,
				'force_activation' => true,
			),
			array(
				'name'   => 'Augment Types',
				'slug'   => 'augment-types',
			),
			array(
				'name' => 'Enable Media Replace',
				'slug' => 'enable-media-replace',
			),
			array(
				'name' => 'Wordfence Security',
				'slug' => 'wordfence',
			),
		);

		$config = array(
			'id'           => 'club_menangle-tgmpa',
			'menu'         => 'club_menangle-plugins',
			'parent_slug'  => 'cmau-options',
			'dismissable'  => false,
			'is_automatic' => true,
		);

		tgmpa( $plugins, $config );
	}
	add_action( 'tgmpa_register', 'club_menangle_plugins' );
}
