<?php

/**
 * Plugin Name: Club Menangle
 * Plugin URI:  https://clubmenangle.com.au
 * Author:      Gene Alyson Fortunado Torcende
 * Author URI:  mailto:gene@pbwebdev.com.au
 * Description: A ThemePlate project for Club Menangle
 * Version:     0.1.0
 * License:     GPL-2.0-only
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: club-menangle
 * Tags:        generator-themeplate, translation-ready, accessibility-ready
 */

/* generator-themeplate v1.13.0 */

// Accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/*
 * ==================================================
 * Global constants
 * ==================================================
 */
// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma
define( 'CLUB_MENANGLE_PLUGIN_FILE', __FILE__ );
define( 'CLUB_MENANGLE_PLUGIN_URL',  plugin_dir_url( __FILE__ ) );
define( 'CLUB_MENANGLE_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'CLUB_MENANGLE_REBUILD', (int) ( 'clubmenangle' === get_template() ) );
// phpcs:enable

function club_menangle_options( $key = '', $default = false ) {
	if ( class_exists( 'ThemePlate' ) && ThemePlate::is_initialized() && '' !== $key ) {
		$value = ThemePlate()->get_option( $key );

		if ( ! $value ) {
			$value = $default;
		}

		return $value;
	}

	$options = get_option( 'cmau-options', $default );
	$value   = $default;

	if ( empty( $key ) ) {
		$value = $options;
	} elseif ( is_array( $options ) && array_key_exists( $key, $options ) && false !== $options[ $key ] ) {
		$value = $options[ $key ];
	}

	return $value;
}

function club_menangle_values( $key, $type, $status = 'publish' ) {
	$posts = get_posts(
		array(
			'post_type'   => $type,
			'post_status' => $status,
			'meta_key'    => $key,
			'fields'      => 'ids',
			'nopaging'    => true,
		)
	);

	$values = array();

	foreach ( $posts as $post ) {
		$values[] = get_post_meta( $post, $key, true );
	}

	return array_filter( array_unique( $values ) );
}


/*
 * ==================================================
 * Setup Plugin
 * ==================================================
 */

if ( class_exists( 'ThemePlate' ) ) :
	ThemePlate( array(
		'title' => 'Club Menangle',
		'key'   => 'cmau',
	) );

	ThemePlate()->page( array(
		'id'     => 'event',
		'parent' => 'edit.php?post_type=event',
		'title'  => 'Event Settings',
		'menu'   => 'Settings',
	) );

	require_once 'setup/post-types.php';
	require_once 'setup/settings.php';
	require_once 'setup/settings-event.php';
	require_once 'setup/meta-boxes.php';
endif;

require_once 'setup/plugins.php';
require_once 'setup/hooks.php';
require_once 'setup/actions-filters.php';


/*
 * ==================================================
 * Extra custom functions
 * ==================================================
 */

