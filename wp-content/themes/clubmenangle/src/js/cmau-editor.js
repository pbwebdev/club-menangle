import $ from 'jquery';
import setupSlick from './_sliders';

const maybeHasCustom = ( node, timeout ) => {
	const passClasses = ( node ) => {
		const innerBlocks = node.querySelector( '[data-is-drop-zone]' );

		node.getAttribute( 'data-tp-blocks' ).split( ' ' ).forEach( className => {
			innerBlocks.classList.add( className );
		} );
	};

	const customBlock = node.querySelector( '[data-tp-blocks]' );

	if ( null !== customBlock ) {
		passClasses( customBlock );
	}
};

( ( wp ) => {
	wp.hooks.addAction( 'tpb-rendered', 'clubmenangle', ( blockID, blockRef ) => {
		console.log( blockID );
		maybeHasCustom( blockRef );
		setupSlick( $(blockRef).find('.slider') );
	} );
} )( wp );
