import $ from 'jquery';
import setupSlick from './_sliders';
import maybeMap from './_gmap';

jQuery(document).ready(async () => {
	// eslint-disable-next-line no-console
	console.log('Everything is ready. ThemePlate!');

	const initSlick = () => {
		if (typeof $.fn.slick === 'function') {
			$('.slider').each(function() {
				setupSlick($(this))
			});

			window.clearInterval(checkSlick);
		}
	}

	const checkSlick = window.setInterval(initSlick, 200);


	const $facetwpTemplate = $('#facetwp-template');
	let initialRefresh = true;

	if ($facetwpTemplate.length) {
		$(document).on('facetwp-refresh', function() {
			if (FWP.loaded && !initialRefresh) {
				FWP.enable_scroll = true;
			}
		});

		$(document).on('facetwp-loaded', function() {
			if (! FWP.loaded) {
				setTimeout(() => {
					FWP.refresh();
					initialRefresh = false
				});
			}

			if (FWP.enable_scroll === true) {
				$( 'html, body' ).animate( {
					scrollTop: $( '#facetwp-template' ).offset().top
				}, 500 );
			}
		});
	}

	maybeMap(document.querySelector('#cmau_map'))
});

const handleSizing = () => {
	const headerHeight = $('.site-header').outerHeight();
	const $pageBanner = $('.page-banner');

	$pageBanner
		.css('margin-top', -headerHeight)
		.css('padding-top', '')
		.css('padding-top', headerHeight + parseInt($pageBanner.css('padding-top')));
}

// window.addEventListener('load', handleSizing);
// window.addEventListener('resize', handleSizing);

document.addEventListener('alpine:init', () => {
	Alpine.data('openingHours', () => ({
		isOpen: false,
		line: 'CLOSED',

		init() {
			const $data = this.$el.querySelectorAll(':scope div > p');
			const dateNames = [
				['Sun', 'Sunday'],
				['Mon', 'Monday'],
				['Tue', 'Tuesday'],
				['Wed', 'Wednesday'],
				['Thu', 'Thursday'],
				['Fri', 'Friday'],
				['Sat', 'Saturday'],
			];
			const dateIndex = (wantedName) => dateNames.findIndex(name => {
				return name === wantedName || name.includes( wantedName );
			});
			const dateInRange = (wantedDate, subs) => {
				if (subs.some(sub => wantedDate.includes(sub))) {
					return true;
				}

				return dateIndex(subs[0]) <= dateIndex(wantedDate) && dateIndex(subs[1]) >= dateIndex(wantedDate);
			};

			Array.from($data).some($el => {
				const lines = $el.innerHTML.split(': ');

				if (1 === lines.length) {
					this.$refs.today.classList.add('hidden');
					this.open();

					return true;
				} else {
					const dateName = dateNames[new Date().getDay()];

					if (
						dateName.includes(lines[0]) ||
						dateInRange(dateName, lines[0].split('-'))
					) {
						this.line = lines[1];

						return true;
					}
				}

				return false;
			} )
		},

		open() {
			this.isOpen = true;
		},
	}));
});
