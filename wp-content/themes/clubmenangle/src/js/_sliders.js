
function navigation($this) {
	const $navigation = $this.next('.slider-navigation');
	const $nav = $navigation.length ? jQuery($navigation[0]) : $this;
	let dotsClass = `slick-dots z-1 ${$this.data( 'dots' ) ?? ''}`.replace(' true', '');

	function arrow(type) {
		return $navigation.length ? $nav.find(`.arrow-${type}`) : $this.siblings(`.arrow-${type}`);
	}

	dotsClass += $nav === $this ? ' absolute left-1/2 -translate-x-1/2' : '';

	return {
		dotsClass,
		appendArrows: $nav,
		appendDots: $nav,
		prevArrow: !!$this.data( 'arrows' ) ? arrow('previous') : '',
		nextArrow: !!$this.data( 'arrows' ) ? arrow('next') : '',
	};
}

export default function setupSlick($this) {
	const settings = {
		adaptiveHeight: true,
		focusOnSelect: true,
		slidesPerRow: 1,
		rows: 0,
	}

	const custom = navigation($this);

	$this.slick({
		...settings,
		...custom,
		slidesToShow: $this.data('show') ?? 1,
		slidesToScroll: $this.data('scroll') ?? 1,
		arrows: !!$this.data( 'arrows' ),
		dots: !!$this.data( 'dots' ),
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
				},
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: $this.data('show') ? Math.ceil($this.data('show') / 2) : 1,
				},
			},
		],
	})
}
