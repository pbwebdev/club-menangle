function initMap($this) {
	const marker = new google.maps.LatLng($this.dataset.lat, $this.dataset.lng);
	const settings = {
		center: marker,
		zoom: Number($this.dataset.zml),
	};

	new google.maps.Marker({
		map: new google.maps.Map($this, settings),
		position: marker,
	});
}

export default function mapScript($this) {
	if (null === $this) {
		return;
	}

	const script = document.createElement( 'script' );

	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?key=' + $this.dataset.api + '&callback=initMap';

	document.body.appendChild( script );

	window.initMap = () => initMap( $this )
}
