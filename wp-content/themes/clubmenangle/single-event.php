<?php

/**
 * The template for displaying single posts
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id     = get_the_ID();
$date     = get_post_meta( $p_id, 'cmau_event_date', true );
$time     = get_post_meta( $p_id, 'cmau_event_time', true );
$location = get_post_meta( $p_id, 'cmau_event_location', true );
$book     = get_post_meta( $p_id, 'cmau_event_book', true );
$tran_btn = get_post_meta( $p_id, 'cmau_event_transport', true );
$venue 	  = get_post_meta( $p_id, 'cmau_event_venue', true );
$video    = array(
	'url' => get_post_meta( $p_id, 'cmau_event_video', true ),
);
$images = get_post_meta( $p_id, 'cmau_event_gallery', true );

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
					<div class="4xl:px-24 full:px-32">
						<div class="4xl:px-16 full:px-0 py-8 2xl:py-16 4xl:py-24 full:py-32 grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16">
							<div class="flex max-xs:flex-col gap-4 md:gap-8">
								<div class="w-16 h-16 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
									<i class="text-3xl fas fa-calendar"></i>
								</div>

								<div class="flex flex-col gap-4">
									<h2 class="text-lg 2xl:text-xl 4xl:text-2xl uppercase text-teak">When</h2>

									<p class="2xl:text-lg 4xl:text-xl"><?php echo date( 'l jS F Y', strtotime( $date ) ); ?></p>
									<p class="2xl:text-lg 4xl:text-xl"><?php echo $time; ?></p>
								</div>
							</div>

							<div class="flex max-xs:flex-col gap-4 md:gap-8">
								<div class="w-16 h-16 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
									<i class="text-3xl fas fa-location-dot"></i>
								</div>

								<div class="flex flex-col gap-4">
									<h2 class="text-lg 2xl:text-xl 4xl:text-2xl uppercase text-teak">Where</h2>

									<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $location ) ) as $line ) : ?>
										<p class="2xl:text-lg 4xl:text-xl"><?php echo $line; ?></p>
									<?php endforeach; ?>

									<?php if ( $tran_btn ) : ?>
									<p class="2xl:text-lg 4xl:text-xl">
										<?php echo cmau_views()->render( 'global-button', [
											'small'  => true,
											'url'    => $tran_btn['url'],
											'text'   => $tran_btn['text'],
											'class'  => 'mt-4',
										] ); ?>
									</p>
									<?php endif; ?>
								</div>
							</div>

							<div class="flex max-xs:flex-col gap-4 md:gap-8">
								<div class="w-16 h-16 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
									<i class="text-3xl fas fa-store"></i>
								</div>

								<div class="flex flex-col gap-4">
									<h2 class="text-lg 2xl:text-xl 4xl:text-2xl uppercase text-teak">Venue</h2>

									<p class="2xl:text-lg 4xl:text-xl"><?php echo $venue; ?></p>

									<?php if ( $book ) : ?>
									<p class="2xl:text-lg 4xl:text-xl">
										<?php echo cmau_views()->render( 'global-button', [
											'small'  => true,
											'url'    => $book,
											'text'   => 'Book Now',
											'class'  => 'mt-4',
											'target' => '_blank',
										] ); ?>
									</p>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php get_template_part( 'template-parts/page', 'content' ); ?>
			</article><!-- #post-<?php the_ID(); ?> -->
		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/event', 'sections' ); ?>
		<?php get_template_part( 'template-parts/page', 'video', $video ); ?>
		<?php get_template_part( 'template-parts/event', 'packages' ); ?>
		<?php get_template_part( 'template-parts/page', 'images', compact( 'images' ) ); ?>

	</main>

	<?php get_template_part( 'template-parts/related', 'events' ); ?>

<?php

get_footer();
