<?php

/**
 * The main template file
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$cmau_event = get_option( 'cmau-event' );

$args = array(
	'post_type'      => 'event',
	'post_status'    => 'publish',
	'orderby'        => 'date',
	'posts_per_page' => $cmau_event['archive_featured'],
	'meta_query'     => array(
		array(
			'key'     => 'cmau_event_sticky',
			'value'   => '1',
			'compare' => '=',
		),
	),
);

$query = new WP_Query( $args );

get_header();

?>

	<main class="my-8 2xl:my-16 4xl:my-24 full:my-32">
		<div class="flex flex-wrap items-center justify-center gap-4 md:gap-8 px-8 2xl:px-16 4xl:px-24 full:px-32">
			<?php echo do_shortcode( '[facetwp facet="event_date"]' ); ?>
			<?php echo do_shortcode( '[facetwp facet="event_category"]' ); ?>
			<?php echo do_shortcode( '[facetwp facet="event_venue"]' ); ?>
		</div>

		<div class="my-8 2xl:my-16 4xl:my-24 full:my-32">
			<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => $cmau_event['archive_heading'],
					'small' => true,
				] ); ?>
			</div>

			<div id="featured_slider" class="slider py-8 2xl:py-16 4xl:py-24 full:py-32" data-arrows="true" data-dots="true">
				<?php while ( $query->have_posts() ) : ?>
					<?php
						$query->the_post();

						$date  = get_post_meta( get_the_ID(), 'cmau_event_date', true );
						$venue = get_post_meta( get_the_ID(), 'cmau_event_venue', true );
					?>

				<div class="relative w-full px-8 xl:px-12 2xl:px-16 4xl:px-24">
					<div class="grid md:grid-cols-2 gap-8 md:gap-16 items-center">
						<div class="relative pb-8 xl:pb-12 2xl:pb-16 4xl:pb-24">
							<?php the_post_thumbnail( 'tile-1_2', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

							<div class="absolute bg-pearlbush -z-10 -start-8 -end-8 xl:-start-12 xl:-end-12 2xl:-start-16 2xl:-end-16 4xl:-start-24 4xl:-end-24 top-32 2xl:top-64 bottom-0"></div>
						</div>

						<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
							<div class="flex items-center">
								<?php echo cmau_views()->render( 'item-lead', [
									'small' => true,
									'text'  => date( 'j M', strtotime( $date ) ),
									'class' => 'w-24 h-24 4xl:w-32 4xl:h-32 5xl:w-36 5xl:h-36 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white uppercase',
								] ); ?>

								<div class="flex-1 ps-4 md:ps-8 full:ps-16">
									<?php echo cmau_views()->render( 'item-title', [
										'title' => get_the_title(),
										'link'  => get_the_permalink(),
										'class' => 'mb-6',
									] ); ?>
									<?php echo cmau_views()->render( 'item-lead', ['text' => $venue] ); ?>
								</div>
							</div>

							<?php echo cmau_views()->render( 'item-excerpt', [
								'raw'   => true,
								'text'  => get_the_excerpt(),
								'class' => 'mt-12 4xl:mt-24',
							] ); ?>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>

			<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#featured_slider'] ); ?>

			<div class="pb-8 2xl:pb-16 4xl:pb-24 full:pb-32"></div>

			<div id="facetwp-template" class="grid md:grid-cols-2 gap-8 xl:gap-16 3xl:gap-24 5xl:gap-32 px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<?php while ( have_posts() ) : ?>
					<?php the_post(); ?>

					<article id="event-<?php the_ID(); ?>" <?php post_class( 'w-full' ); ?>>
						<?php get_template_part( 'template-parts/event', 'item' ); ?>
					</article><!-- #post-<?php the_ID(); ?> -->
				<?php endwhile; ?>
			</div>

			<?php echo do_shortcode( '[facetwp facet="event_pager"]' ); ?>
		</div>

	</main>

<?php

get_footer();
