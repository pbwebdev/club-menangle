<?php

/**
 * The template for displaying the footer
 *
 * @package Club Menangle
 * @since 0.1.0
 */

?>

		</div><!-- .site-content -->

		<footer class="site-footer">
			<?php get_template_part( 'template-parts/global', 'subscribe' ); ?>
			<?php get_sidebar( 'footer' ); ?>

			<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 bg-mineshaft py-4 md:flex items-center justify-between">
				<nav class="sitelinks py-2">
					<div class="hidden flex-wrap items-center justify-center gap-10 text-white 2xl:text-lg 4xl:text-xl"></div>
					<?php cmau_footer_menu( array( 'flex flex-wrap items-center justify-center gap-10 text-white 2xl:text-lg 4xl:text-xl', ) ); ?>
				</nav>

				<div class="copyright py-2 text-white text-center">
					<small class="text-xs 2xl:text-sm 4xl:text-base"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="underline hover:text-teak"><?php bloginfo( 'name' ); ?></a> &copy; <?php echo esc_html( gmdate( 'Y' ) ); ?> <span>|</span> Experience by <a href="https://pbwebdev.com" target="_blank" rel="noopener noreferrer" title="Custom WordPress Design & Development" class="underline hover:text-teak">PB Web Development</a></small>
				</div>
			</div>
		</footer><!-- .site-footer -->

		<?php wp_footer(); ?>

<!-- Hotjar Tracking Code for http://www.clubmenangle.com.au/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1831067,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

	</body>
</html>

<!-- A generator-themeplate project - https://github.com/kermage/generator-themeplate -->
