<?php

if ( empty( $title ) ) {
	return;
}

$link ??= false;

$classes = [
	'text-2xl 4xl:text-3xl 5xl:text-4xl full:text-5xl',
	$link ? 'text-teak hover:text-mineshaft uppercase' : '',
	$class ?? '',
];

?>

<h3 class="<?php echo implode( ' ', $classes ); ?>">
	<?php if ( $link ) : ?>
		<a href="<?= $link ?>">
	<?php endif; ?>

	<?= $title ?>

	<?php if ( $link ) : ?>
		</a>
	<?php endif; ?>
</h3>
