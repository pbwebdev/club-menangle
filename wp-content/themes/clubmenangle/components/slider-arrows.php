<?php

if ( empty( $control ) ) {
	return;
}

foreach ( ['previous', 'next'] as $type ) {
	$class = 'absolute top-1/3 -translate-y-1/3';
	echo cmau_views()->render( 'slider-arrow', compact( 'control', 'type', 'class' ) );
}
