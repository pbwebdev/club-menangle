<?php

if ( empty( $control ) || empty( $type ) ) {
	return;
}


$classes = [
	'w-6 h-6 xl:w-8 xl:h-8',
	'-rotate-45 border-teak hover:border-mineshaft',
	( $type === 'previous' ) ? '-left-6 border-t-4 border-l-4' : '-right-6 border-b-4 border-r-4',
	$class ?? '',
	'arrow-' . $type,
];

?>

<button
	aria-controls="<?= $control ?>"
	aria-label="<?= $type; ?>"
	class="<?php echo implode( ' ', $classes ); ?>"
>
</button>
