<?php

if ( empty( $title ) ) {
	return;
}

$link ??= false;
$small ??= false;
$level ??= 'h2';

$classes = [
	$small ? 'w-2/3 md:w-3/5' : 'w-4/5 md:w-2/3',
	$small ? 'text-3xl 4xl:text-4xl 5xl:text-5xl full:text-6xl' : 'text-4xl 4xl:text-5xl 5xl:text-6xl full:text-7xl',
	$link ? 'text-white' : 'text-teak',
	'uppercase',
	'pb-4 3xl:pb-6 5xl:pb-8',
	'border-b-4 4xl:border-b-6 full:border-b-8 border-teak',
	$class ?? '',
];

?>

<<?php echo $level; ?> class="<?php echo implode( ' ', $classes ); ?>">
	<?php if ( $link ) : ?>
		<a href="<?= $link ?>">
	<?php endif; ?>

	<?= $title ?>

	<?php if ( $link ) : ?>
		</a>
	<?php endif; ?>
</<?php echo $level; ?>>
