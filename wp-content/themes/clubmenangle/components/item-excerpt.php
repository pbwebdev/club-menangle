<?php

if ( empty( $text ) ) {
	return;
}

$raw ??= false;
$relaxed ??= true;
$class ??= '';

if ( ! $raw ) {
	$text = $this->escape( $text );
}

?>

<?php echo cmau_views()->render( 'partial/open-item-excerpt', compact( 'relaxed', 'class' ) ); ?>
	<p><?= $text; ?></p>
<?php echo cmau_views()->render( 'partial/close' ); ?>
