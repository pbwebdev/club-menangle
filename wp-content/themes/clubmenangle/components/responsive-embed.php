<?php

if ( empty( $url ) ) {
	return;
}

$classes = [
	'max-w-full h-auto',
	$class ?? '',
];

$output = str_replace( '<iframe ', '<iframe class="' . esc_attr( implode( ' ', $classes ) ) . '"', wp_oembed_get( $url ) );

?>

<div class="relative w-full">
	<?php echo $output; ?>

	<div class="absolute w-full h-full bg-pearlbush -z-10 -start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32"></div>
</div>
