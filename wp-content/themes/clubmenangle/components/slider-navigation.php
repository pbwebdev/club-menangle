<?php

if ( empty( $control ) ) {
	return;
}

$classes = [
	'slider-navigation',
	$class ?? '',
]

?>

<div class="<?php echo implode( ' ', $classes ); ?>">
	<?php echo cmau_views()->render( 'slider-arrow', compact( 'control' ) + ['type' => 'previous'] ); ?>
	<?php echo cmau_views()->render( 'slider-arrow', compact( 'control' ) + ['type' => 'next'] ); ?>
</div>
