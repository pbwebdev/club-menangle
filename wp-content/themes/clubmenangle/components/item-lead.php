<?php

if ( empty( $text ) ) {
	return;
}

$raw ??= false;
$small ??= false;

$classes = [
	'text-xl 4xl:text-2xl 5xl:text-3xl',
	$small ? '' : 'full:text-4xl',
	$class ?? '',
];

if ( ! $raw ) {
	$text = $this->escape( $text );
}

?>

<p class="<?php echo implode( ' ', $classes ); ?>"><?= $text; ?></p>
