<?php

if ( empty( $text ) && empty( $url ) ) {
	return;
}

$id ??= '';
$url ??= '#';
$target ??= '_self';
$accent ??= true;
$small ??= false;
$escape ??= true;
$attributes ??= '';

if ( ! $text ) {
	$text = $url;
}

$classes = [
	'inline-block text-center uppercase',
	$small ? '4xl:text-lg 5xl:text-xl full:text-2xl' : 'text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl',
	$small ? 'py-4 px-8 3xl:py-6 3xl:px-10 5xl:py-8 5xl:px-14' : 'py-4 px-10 3xl:py-7 3xl:px-12 5xl:py-10 5xl:px-16',
	$accent ? 'text-white bg-teak hover:bg-mineshaft' : 'bg-white hover:bg-mineshaft hover:text-white',
	$class ?? '',
];

?>

<a
	href="<?= $url ?>"
	target="<?= $target ?>"
	class="<?php echo implode( ' ', $classes ); ?>"
	<?php echo $attributes; ?>
>
	<?= $escape ? $this->escape( $text ) : $text; ?>
</a>
