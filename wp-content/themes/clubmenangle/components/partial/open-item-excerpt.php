<?php

$relaxed ??= true;

$classes = [
	'text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl',
	$relaxed ? '!leading-relaxed' : '!leading-loose',
	$class ?? '',
];

?>

<div class="<?php echo implode( ' ', $classes ); ?>">
