<?php

if ( empty( $text ) ) {
	return;
}

$url ??= '#';
$target ??= '_self';
$accent ??= true;
$small ??= false;

$classes = [
	'rounded-full font-lexend-deca',
	'flex flex-col justify-center items-center',
	'text-center uppercase',
	$small ? '4xl:text-lg 5xl:text-xl full:text-2xl' : 'text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl',
	'w-36 h-36 4xl:w-44 4xl:h-44 full:w-52 full:h-52',
	'p-8',
	$accent ? 'text-white bg-teak hover:bg-mineshaft' : 'bg-pearlbush hover:bg-mineshaft hover:text-white',
];

?>

<a
	href="<?= $url ?>"
	target="<?= $target ?>"
	class="<?php echo implode( ' ', $classes ); ?>"
>
	<?= $text ?> <i class="fas fa-chevron-right m-2 mb-0"></i>
</a>
