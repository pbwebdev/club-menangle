/*!
 *  Club Menangle 0.1.0
 *  Copyright (C) 2024 Gene Alyson Fortunado Torcende
 *  Licensed under GPL-2.0-only.
 */

(function ($) {
  'use strict';

  function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

  var $__default = /*#__PURE__*/_interopDefaultLegacy($);

  function _extends() {
    _extends = Object.assign ? Object.assign.bind() : function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
      return target;
    };
    return _extends.apply(this, arguments);
  }

  function navigation($this) {
    var _$this$data;
    var $navigation = $this.next('.slider-navigation');
    var $nav = $navigation.length ? jQuery($navigation[0]) : $this;
    var dotsClass = `slick-dots z-1 ${(_$this$data = $this.data('dots')) != null ? _$this$data : ''}`.replace(' true', '');
    function arrow(type) {
      return $navigation.length ? $nav.find(`.arrow-${type}`) : $this.siblings(`.arrow-${type}`);
    }
    dotsClass += $nav === $this ? ' absolute left-1/2 -translate-x-1/2' : '';
    return {
      dotsClass,
      appendArrows: $nav,
      appendDots: $nav,
      prevArrow: !!$this.data('arrows') ? arrow('previous') : '',
      nextArrow: !!$this.data('arrows') ? arrow('next') : ''
    };
  }
  function setupSlick($this) {
    var _$this$data2, _$this$data3;
    var settings = {
      adaptiveHeight: true,
      focusOnSelect: true,
      slidesPerRow: 1,
      rows: 0
    };
    var custom = navigation($this);
    $this.slick(_extends({}, settings, custom, {
      slidesToShow: (_$this$data2 = $this.data('show')) != null ? _$this$data2 : 1,
      slidesToScroll: (_$this$data3 = $this.data('scroll')) != null ? _$this$data3 : 1,
      arrows: !!$this.data('arrows'),
      dots: !!$this.data('dots'),
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      }, {
        breakpoint: 1024,
        settings: {
          slidesToShow: $this.data('show') ? Math.ceil($this.data('show') / 2) : 1
        }
      }]
    }));
  }

  var maybeHasCustom = function maybeHasCustom(node, timeout) {
    var passClasses = function passClasses(node) {
      var innerBlocks = node.querySelector('[data-is-drop-zone]');
      node.getAttribute('data-tp-blocks').split(' ').forEach(function (className) {
        innerBlocks.classList.add(className);
      });
    };
    var customBlock = node.querySelector('[data-tp-blocks]');
    if (null !== customBlock) {
      passClasses(customBlock);
    }
  };
  (function (wp) {
    wp.hooks.addAction('tpb-rendered', 'clubmenangle', function (blockID, blockRef) {
      console.log(blockID);
      maybeHasCustom(blockRef);
      setupSlick($__default["default"](blockRef).find('.slider'));
    });
  })(wp);

})(jQuery);

//# sourceMappingURL=cmau-editor.js.map
