<?php

/**
 * The template for displaying single posts
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$object  = get_queried_object();

get_header();

?>

	<main class="content">
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>
				<?php get_template_part( 'template-parts/page', 'content' ); ?>
			<?php endwhile; ?>
		</article><!-- #post-<?php the_ID(); ?> -->
	</main>

	<?php if ( 'post' === $object->post_type ) : ?>
	<?php get_template_part( 'template-parts/related', 'posts' ); ?>
	<?php endif; ?>

<?php

get_footer();
