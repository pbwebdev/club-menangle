/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./style.css',
		'./*.php',
		'./includes/*.php',
		'./setup/*.php',
		'./blocks/**/*.php',
		'./components/**/*.php',
		'./template-parts/**/*.php',
		'./page-templates/**/*.php',
		'./src/js/**/*.js',
	],
	theme: {
		container: {
			center: true,
			padding: '1.25rem',
		},
		screens: {
			xs: '360px',
			sm: '576px',
			md: '768px',
			lg: '1024px',
			xl: '1280px',
			'2xl': '1440px',
			'3xl': '1600px',
			'4xl': '1920px',
			'5xl': '2160px',
			full: '2560px',
		},
		extend: {
			colors: {
				teak: '#ae9866',
				mineshaft: '#383637',
				pearlbush: '#eee9df',
			},
			fontFamily: {
				'lexend-deca': ['"Lexend Deca"', 'sans-serif'],
				muli: ['"Muli"', 'sans-serif'],
			},
			height: {
				'banner': 'clamp(540px, 100vh, 540px)',
			},
			borderWidth: {
				3: '3px',
				6: '6px',
			},
			zIndex: {
				'-1': '-1',
				1: 1,
			},
			aspectRatio: {
				'2/1': '2 / 1',
				'5/6': '5 / 6',
				'16/10': '16 / 10',
			},
			typography: (theme) => ({
				DEFAULT: {
					css: {
						color: theme('colors.mineshaft'),
						'h1, h2, h3, h4': {
							fontWeight: 400,
						}
					},
				},
			}),
		},
	},
	variants: {
		extend: {
			// Custom
		},
	},
	plugins: [
		// Custom
		require('@tailwindcss/forms'),
		require('@tailwindcss/typography'),
		require('tailwind-scrollbar'),
	],
};
