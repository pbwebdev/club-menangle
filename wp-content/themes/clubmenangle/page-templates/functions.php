<?php

/**
 * Template Name: Legacy: Functions
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id  = get_the_ID();
$query = new WP_Query(
	array(
		'post_parent'    => $p_id,
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	)
);

$n = 0;

get_header();

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'template-parts/page', 'content' ); ?>
		<?php endwhile; ?>

		<?php if ( $query->have_posts() ) : ?>
			<?php while ( $query->have_posts() ) : ?>
				<?php
					$query->the_post();
					$n++;

					$zigzag = get_post_meta( get_the_ID(), 'cmau_zigzag_function_button', true );
				?>

				<?php echo cmau_views()->render( 'block::zigzag-item/markup', [
					'attributes' => array(
						'reverse' => ( 1 === ( $n + 1 ) % 2 ),
						'image'   => array(
							'id' => get_post_thumbnail_id(),
						),
						'heading' => get_the_title(),
						'link'    => array_merge( array(
							'url'  => get_the_permalink(),
							'text' => 'Find out more'
						), $zigzag['link'] ?? array() ),
					),
					'content' => apply_filters( 'the_content', get_the_excerpt() ),
				] ); ?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</article>
</main><!-- .content -->

<?php

get_footer();
