<?php

/**
 * Template Name: Legacy: Restaurants
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id  = get_the_ID();
$query = new WP_Query(
	array(
		'post_parent'    => $p_id,
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	)
);

$n = 0;

$sections = get_post_meta( $p_id, 'cmau_page_sections', false );

get_header();

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php if ( $query->have_posts() ) : ?>
			<?php while ( $query->have_posts() ) : ?>
				<?php
					$query->the_post();
					$n++;

					$gallery = get_post_meta( get_the_ID(), 'cmau_page_gallery', true );
				?>

				<?php get_template_part( 'template-parts/page', 'gallery', [
					'heading' => get_the_title(),
					'content' => get_the_excerpt(),
					'link'    => array(
						'url'  => get_the_permalink(),
						'text' => 'Find out more',
					),
					'images'  => $gallery['images'] ?? array(),
					'reverse' => ( 1 === ( $n + 1 ) % 2 ),
				] ); ?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>

		<?php foreach ( $sections as $index => $section ) : ?>
			<?php get_template_part( 'template-parts/page', 'gallery', $section + ['reverse' => ( 1 === ( $index + 1 ) % 2 )] ); ?>
		<?php endforeach; ?>
	</article>
</main><!-- .content -->

<?php

get_footer();
