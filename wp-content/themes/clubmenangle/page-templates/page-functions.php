<?php

/**
 * Template Name: Functions (No editor)
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id     = get_the_ID();
$image    = get_post_meta( $p_id, 'cmau_venue_image', true );
$heading  = get_post_meta( $p_id, 'cmau_venue_heading', true );
$content  = get_post_meta( $p_id, 'cmau_venue_content', true );
$tour     = get_post_meta( $p_id, 'cmau_venue_tour', true );
$video    = array(
	'url' => get_post_meta( $p_id, 'cmau_venue_video', true ),
	'placeholder' => $image,
);
$virtual  = get_post_meta( $p_id, 'cmau_venue_virtual', true );
$gallery  = array(
	'anchor'  => 'venue-gallery',
	'images'  => get_post_meta( $p_id, 'cmau_venue_gallery', true ),
	'reverse' => true,
);
$sections = array();

$query = new WP_Query(
	array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	)
);

if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

		$zigzag = get_post_meta( get_the_ID(), 'cmau_zigzag_function_button', true );
		$sections[] = [
			'reverse' => false,
			'image'   => get_post_thumbnail_id(),
			'heading' => get_the_title(),
			'link'    => array_merge( array(
				'url'  => get_the_permalink(),
				'text' => 'Find out more'
			), $zigzag['link'] ?? array() ),
			'content' => apply_filters( 'the_content', get_the_excerpt() ),
		];
	}
}

get_header();

?>

	<main class="content relative">
		<?php get_template_part( 'template-parts/venue', 'links', ['id' => $p_id] ); ?>

		<div class="xl:px-8 2xl:px-16 4xl:px-24 full:px-32">
			<div class="lg:px-8 2xl:px-16 4xl:px-24 full:px-32">
				<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32">
					<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 mt-8 2xl:mt-16 4xl:mt-24 full:mt-32">
						<?php echo cmau_views()->render( 'front-heading', [
							'title' => $heading,
							'small' => true,
							'class' => 'mb-8 2xl:mb-12 4xl:mb-24'
						] ); ?>

						<div class="pb-8 2xl:pb-16 text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl">
							<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $content ) ) as $line ) : ?>
								<p class="my-4 2xl:my-8 4xl:my-16"><?php echo $line; ?></p>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php get_template_part( 'template-parts/page', 'video', $video ); ?>

		<div class="pt-8 2xl:pt-16 4xl:pt-24 full:pt-32"></div>

		<?php get_template_part( 'template-parts/venue', 'spaces' ); ?>
		<?php get_template_part( 'template-parts/front', 'super', ['id' => get_option( 'page_on_front' )] ); ?>

		<div id="virtual-tour" class="xl:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
			<div class="lg:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32">
					<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
						<?php echo cmau_views()->render( 'front-heading', [
							'title' => 'Virtual Tour',
							'small' => true,
							'class' => 'text-right ml-auto mb-8 2xl:mb-12 4xl:mb-24'
						] ); ?>

						<div class="relative w-full">
							<?php echo wp_get_attachment_image( $tour['image'], 'tile-1_2', false, [
								'class' => 'aspect-16/10 object-cover object-center w-full brightness-75 group-hover:brightness-50'
							] ); ?>

							<div class="absolute w-full h-full bg-pearlbush -z-10 -start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32"></div>
							<div class="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
								<a href="<?php echo $tour['link'] ?? ''; ?>" target="_blank"><i class="fas fa-circle-play text-teak text-7xl xl:text-8xl 3xl:text-9xl"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="slider-section" class="slider mb-8 py-8 2xl:py-16 4xl:py-24 full:py-32" data-arrows="true" data-dots="true">
			<?php get_template_part( 'template-parts/page', 'sections', compact( 'sections' ) ); ?>
		</div>

		<?php echo cmau_views()->render( 'slider-navigation', ['control' => 'slider-section', 'class' => '-mt-16 2xl:-mt-24 4xl:-mt-32 full:-mt-40 mb-8 2xl:mb-16 4xl:mb-24 full:mb-32'] ); ?>
		<?php get_template_part( 'template-parts/venue', 'gallery', $gallery ); ?>
		<?php get_template_part( 'template-parts/venue', 'testimonials' ); ?>
	</main><!-- .content -->

<?php

get_footer();
