<?php

/**
 * Template Name: Directors Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'director',
		'posts_per_page' => -1,
	)
);

get_header();

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'template-parts/page', 'content' ); ?>
		<?php endwhile; ?>

		<?php if ( $query->have_posts() ) : ?>
			<div class="py-8 2xl:py-16 4xl:py-24 full:py-32 px-8 2xl:px-16 4xl:px-24 full:px-32 grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16">
				<?php while ( $query->have_posts() ) : ?>
					<?php
						$query->the_post();

						$d_id     = get_the_ID();
						$position = get_post_meta( $d_id, 'cmau_director_position', true );
					?>

					<div class="w-full">
						<?php the_post_thumbnail( 'tile-1_3', array( 'class' => 'aspect-5/6 object-cover object-center w-full brightness-75' ) ); ?>
						<?php echo cmau_views()->render( 'item-title', [
							'title' => get_the_title(),
							'class' => 'my-6',
						] ); ?>
						<?php echo cmau_views()->render( 'item-lead', [
							'text'  => $position,
							'class' => 'text-teak',
						] ); ?>

						<div class="mt-8 max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		<?php endif; ?>
	</article>

</main><!-- .content -->

<?php

get_footer();
