<?php

/**
 * Template Name: All Events (No editor)
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$args = array(
	'post_type'      => 'event',
	'post_status'    => 'publish',
	'posts_per_page' => -1,
	'meta_query'     => array(
		'date_clause' => array(
			'key' => 'cmau_event_date',
			'type' => 'DATE',
			'compare' => 'EXISTS',
		),
	),
	'orderby'        => array(
		'date_clause' => 'ASC',
		'meta_value' => 'DESC',
		'menu_order' => 'ASC',
		'post_date'  => 'DESC',
	),
);

$query = new WP_Query( $args );

get_header();

?>

	<main class="my-8 2xl:my-16 4xl:my-24 full:my-32">
		<div class="flex flex-wrap items-center justify-center gap-4 md:gap-8 px-8 2xl:px-16 4xl:px-24 full:px-32">
			<?php echo do_shortcode( '[facetwp facet="event_date"]' ); ?>
			<?php echo do_shortcode( '[facetwp facet="event_category"]' ); ?>
			<?php echo do_shortcode( '[facetwp facet="event_venue"]' ); ?>
		</div>

		<div class="my-8 2xl:my-16 4xl:my-24 full:my-32">
			<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => 'Tickets & Hospitality',
					'small' => true,
				] ); ?>
			</div>

			<div class="grid md:grid-cols-2 xl:grid-cols-3 gap-8 xl:gap-16 px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<?php while ( $query->have_posts() ) : ?>
					<?php $query->the_post(); ?>

					<article id="event-<?php the_ID(); ?>" <?php post_class( 'w-full' ); ?>>
						<?php get_template_part( 'template-parts/package', 'item' ); ?>
					</article><!-- #post-<?php the_ID(); ?> -->
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>

	</main>

<?php

get_footer();
