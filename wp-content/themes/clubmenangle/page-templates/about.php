<?php

/**
 * Template Name: About Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

$sections = get_post_meta( get_the_ID(), 'cmau_about_sections', false );

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'template-parts/page', 'content' ); ?>
		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/page', 'sections', compact( 'sections' ) ); ?>
	</article><!-- #post-<?php the_ID(); ?> -->
</main>

<?php

get_footer();
