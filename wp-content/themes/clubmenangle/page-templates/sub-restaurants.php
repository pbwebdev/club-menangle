<?php

/**
 * Template Name: Restaurants (No editor)
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id     = get_the_ID();
$info     = get_post_meta( $p_id, 'cmau_venue_info', false );
$images   = get_post_meta( $p_id, 'cmau_venue_images', true );
$heading  = get_post_meta( $p_id, 'cmau_venue_heading', true );
$content  = get_post_meta( $p_id, 'cmau_venue_content', true );
$button   = get_post_meta( $p_id, 'cmau_venue_button', true );
$events   = get_post_meta( $p_id, 'cmau_venue_events', true );
$sections = get_post_meta( get_the_ID(), 'cmau_page_sections', false );
$carousel = get_post_meta( $p_id, 'cmau_function_carousel', true );

if ( ! empty( $carousel ) ) {
	$carousel['anchor'] = 'functions';
}

get_header();

?>

	<main class="content relative">
		<?php get_template_part( 'template-parts/venue', 'links', ['id' => $p_id] ); ?>

		<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
			<?php if ( ! empty( $info ) ) : ?>
				<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32">
					<div class="lg:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32 grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16">
						<?php foreach ( $info as $item ) : ?>
							<div class="flex max-xs:flex-col gap-4 md:gap-8">
								<div class="w-16 h-16 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
									<i class="text-3xl <?php echo $item['icon']; ?>"></i>
								</div>

								<div class="grid gap-4">
									<h2 class="text-lg 2xl:text-xl 4xl:text-2xl uppercase"><?php echo $item['heading']; ?></h2>

									<?php if ( 'Opening Hours' === $item['heading'] ) : ?>
										<div x-data="openingHours" class="grid gap-4">
											<p class="2xl:text-lg 4xl:text-xl" x-ref="today">
												Today: <span x-text="line"></span>
											</p>
											<p class="2xl:text-lg 4xl:text-xl" x-show="!isOpen">
												<a href="#" class="inline-block font-bold pb-2 border-b-3 border-transparent hover:border-teak" x-on:click.prevent="open">
													Show more
												</a>
											</p>
											<div class="grid gap-4" x-show="isOpen" x-cloak="true">
									<?php endif; ?>

									<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $item['content'] ) ) as $line ) : ?>
										<p class="2xl:text-lg 4xl:text-xl"><?php echo $line; ?></p>
									<?php endforeach; ?>

									<?php if ( isset( $item['link']['url'], $item['link']['text'] ) ) : ?>
									<p class="2xl:text-lg 4xl:text-xl">
										<a href="<?php echo $item['link']['url']; ?>" target="<?php echo $item['link']['target']; ?>" class="inline-block font-bold pb-2 border-b-3 border-transparent hover:border-teak">
											<?php echo $item['link']['text']; ?>
										</a>
									</p>
									<?php endif; ?>

									<?php if ( 'Opening Hours' === $item['heading'] ) : ?>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php get_template_part( 'template-parts/page', 'images', compact( 'images' ) ); ?>

			<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32">
				<div class="lg:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
					<?php echo cmau_views()->render( 'front-heading', [
						'title' => $heading,
						'small' => true,
						'class' => 'mb-8 2xl:mb-12 4xl:mb-24'
					] ); ?>

					<div class="pb-8 2xl:pb-16 xl:flex items-center gap-4 xl:gap-8">
						<div class="flex-1 text-lg 2xl:text-xl 4xl:text-2xl full:text-4xl">
							<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $content ) ) as $line ) : ?>
								<p class="my-4 2xl:my-8 4xl:my-16"><?php echo $line; ?></p>
							<?php endforeach; ?>
						</div>

						<div class="shrink-0 px-4 py-8 sm:py-0">
							<?php echo cmau_views()->render( 'round-button', (array) $button + [
								'small' => true,
							] ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php get_template_part( 'template-parts/page', 'sections', compact( 'sections' ) ); ?>
		<?php get_template_part( 'template-parts/page', 'gallery', $carousel ); ?>
		<?php get_template_part( 'template-parts/venue', 'events', compact( 'events' ) ); ?>

	</main><!-- .content -->

<?php

get_footer();
