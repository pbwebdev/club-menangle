<?php

/**
 * Template Name: Legacy: Sub-Functions
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

$sections = get_post_meta( get_the_ID(), 'cmau_page_sections', false );
$gallery  = get_post_meta( get_the_ID(), 'cmau_page_gallery', true );
$gallery  = array_merge( array(
	'heading' => '',
	'images'  => array(),
	'reverse' => ( 1 === ( count( $sections ) + 1 ) % 2 ),
), (array) $gallery );

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'template-parts/page', 'content' ); ?>
		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/page', 'sections', compact( 'sections' ) ); ?>
		<?php get_template_part( 'template-parts/venue', 'gallery', $gallery ); ?>
		<?php get_template_part( 'template-parts/page', 'enquiry' ); ?>
	</article><!-- #post-<?php the_ID(); ?> -->
</main>

<?php

get_footer();
