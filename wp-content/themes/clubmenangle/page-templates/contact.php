<?php

/**
 * Template Name: Contact Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

$id   = get_the_ID();
$form = get_post_meta( $id, 'cmau_enquiry_form', true );

?>

<main class="content">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32' ); ?>>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'template-parts/page', 'content' ); ?>
		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/contact', 'details', compact( 'id' ) ); ?>
		<?php echo cmau_views()->render( 'block::zigzag-item/markup', [
			'attributes' => array(
				'reverse' => true,
				'image'   => array(
					'id' => get_post_meta( $id, 'cmau_enquiry_image', true ),
				),
				'heading' => get_post_meta( $id, 'cmau_enquiry_heading', true ),
				'link'    => array(
					'url'  => '',
					'text' => '',
				),
			),
			'content' => function_exists( 'gravity_form' ) && $form ? gravity_form( $form, false, false, false, '', true, 0, false ) : '',
		] ); ?>
	</article><!-- #post-<?php the_ID(); ?> -->
</main>

<?php

get_footer();
