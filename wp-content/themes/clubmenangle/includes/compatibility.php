<?php

/**
 * Compatibility
 *
 * @package Club Menangle
 * @since 0.1.0
 */

function cmau_upgrade_message() {
	$theme = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

	$requirement = sprintf(
		/* translators: 1. PHP version, 2. WordPress version */
		__( '<strong>%1$s</strong> requires at least PHP version <strong>%2$s</strong> and WordPress version <strong>%3$s</strong>.', 'club-menangle' ),
		$theme->get( 'Name' ),
		CLUB_MENANGLE_THEME_REQUIRES['PHP'],
		CLUB_MENANGLE_THEME_REQUIRES['WP']
	);

	$installed = sprintf(
		/* translators: 1. PHP version, 2. WP version */
		__( 'Site is running at PHP version <strong>%1$s</strong> and WordPress version <strong>%2$s</strong>.', 'club-menangle' ),
		PHP_VERSION,
		$GLOBALS['wp_version']
	);

	return sprintf(
		/* translators: 1. requirement message, 2. installed message */
		__( '<p>%1$s</p><p>%2$s</p><p>Please upgrade and try again.</p>', 'club-menangle' ),
		$requirement,
		$installed
	);
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	WP_CLI::error( cmau_upgrade_message() );
}

function cmau_notice() {
	wp_admin_notice( cmau_upgrade_message(), array( 'type' => 'error', 'paragraph_wrap' => false ) );
}

function cmau_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'cmau_notice' );
}
add_action( 'after_switch_theme', 'cmau_switch_theme' );

function cmau_customize() {
	wp_die( cmau_upgrade_message(), '', array( 'back_link' => true ) );
}
add_action( 'load-customize.php', 'cmau_customize' );

function cmau_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( cmau_upgrade_message() );
	}
}
add_action( 'template_redirect', 'cmau_preview' );
