<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf45f1254c29c05a0ed834bb7d3db31eb
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'ThemePlate\\Core\\' => 16,
            'ThemePlate\\Blocks\\' => 18,
        ),
        'L' => 
        array (
            'League\\Plates\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ThemePlate\\Core\\' => 
        array (
            0 => __DIR__ . '/..' . '/themeplate/core/src',
        ),
        'ThemePlate\\Blocks\\' => 
        array (
            0 => __DIR__ . '/..' . '/themeplate/blocks/src',
        ),
        'League\\Plates\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/plates/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'League\\Plates\\Engine' => __DIR__ . '/..' . '/league/plates/src/Engine.php',
        'League\\Plates\\Exception\\TemplateNotFound' => __DIR__ . '/..' . '/league/plates/src/Exception/TemplateNotFound.php',
        'League\\Plates\\Extension\\Asset' => __DIR__ . '/..' . '/league/plates/src/Extension/Asset.php',
        'League\\Plates\\Extension\\ExtensionInterface' => __DIR__ . '/..' . '/league/plates/src/Extension/ExtensionInterface.php',
        'League\\Plates\\Extension\\URI' => __DIR__ . '/..' . '/league/plates/src/Extension/URI.php',
        'League\\Plates\\Template\\Data' => __DIR__ . '/..' . '/league/plates/src/Template/Data.php',
        'League\\Plates\\Template\\Directory' => __DIR__ . '/..' . '/league/plates/src/Template/Directory.php',
        'League\\Plates\\Template\\FileExtension' => __DIR__ . '/..' . '/league/plates/src/Template/FileExtension.php',
        'League\\Plates\\Template\\Folder' => __DIR__ . '/..' . '/league/plates/src/Template/Folder.php',
        'League\\Plates\\Template\\Folders' => __DIR__ . '/..' . '/league/plates/src/Template/Folders.php',
        'League\\Plates\\Template\\Func' => __DIR__ . '/..' . '/league/plates/src/Template/Func.php',
        'League\\Plates\\Template\\Functions' => __DIR__ . '/..' . '/league/plates/src/Template/Functions.php',
        'League\\Plates\\Template\\Name' => __DIR__ . '/..' . '/league/plates/src/Template/Name.php',
        'League\\Plates\\Template\\ResolveTemplatePath' => __DIR__ . '/..' . '/league/plates/src/Template/ResolveTemplatePath.php',
        'League\\Plates\\Template\\ResolveTemplatePath\\NameAndFolderResolveTemplatePath' => __DIR__ . '/..' . '/league/plates/src/Template/ResolveTemplatePath/NameAndFolderResolveTemplatePath.php',
        'League\\Plates\\Template\\ResolveTemplatePath\\ThemeResolveTemplatePath' => __DIR__ . '/..' . '/league/plates/src/Template/ResolveTemplatePath/ThemeResolveTemplatePath.php',
        'League\\Plates\\Template\\Template' => __DIR__ . '/..' . '/league/plates/src/Template/Template.php',
        'League\\Plates\\Template\\Theme' => __DIR__ . '/..' . '/league/plates/src/Template/Theme.php',
        'ThemePlate\\Blocks\\AssetsHelper' => __DIR__ . '/..' . '/themeplate/blocks/src/AssetsHelper.php',
        'ThemePlate\\Blocks\\BlockType' => __DIR__ . '/..' . '/themeplate/blocks/src/BlockType.php',
        'ThemePlate\\Blocks\\CustomBlocks' => __DIR__ . '/..' . '/themeplate/blocks/src/CustomBlocks.php',
        'ThemePlate\\Blocks\\FieldsHelper' => __DIR__ . '/..' . '/themeplate/blocks/src/FieldsHelper.php',
        'ThemePlate\\Core\\Config' => __DIR__ . '/..' . '/themeplate/core/src/Config.php',
        'ThemePlate\\Core\\Field' => __DIR__ . '/..' . '/themeplate/core/src/Field.php',
        'ThemePlate\\Core\\Field\\CheckboxField' => __DIR__ . '/..' . '/themeplate/core/src/Field/CheckboxField.php',
        'ThemePlate\\Core\\Field\\ColorField' => __DIR__ . '/..' . '/themeplate/core/src/Field/ColorField.php',
        'ThemePlate\\Core\\Field\\DateField' => __DIR__ . '/..' . '/themeplate/core/src/Field/DateField.php',
        'ThemePlate\\Core\\Field\\EditorField' => __DIR__ . '/..' . '/themeplate/core/src/Field/EditorField.php',
        'ThemePlate\\Core\\Field\\FileField' => __DIR__ . '/..' . '/themeplate/core/src/Field/FileField.php',
        'ThemePlate\\Core\\Field\\GroupField' => __DIR__ . '/..' . '/themeplate/core/src/Field/GroupField.php',
        'ThemePlate\\Core\\Field\\HtmlField' => __DIR__ . '/..' . '/themeplate/core/src/Field/HtmlField.php',
        'ThemePlate\\Core\\Field\\InputField' => __DIR__ . '/..' . '/themeplate/core/src/Field/InputField.php',
        'ThemePlate\\Core\\Field\\LinkField' => __DIR__ . '/..' . '/themeplate/core/src/Field/LinkField.php',
        'ThemePlate\\Core\\Field\\NumberField' => __DIR__ . '/..' . '/themeplate/core/src/Field/NumberField.php',
        'ThemePlate\\Core\\Field\\RadioField' => __DIR__ . '/..' . '/themeplate/core/src/Field/RadioField.php',
        'ThemePlate\\Core\\Field\\SelectField' => __DIR__ . '/..' . '/themeplate/core/src/Field/SelectField.php',
        'ThemePlate\\Core\\Field\\TextareaField' => __DIR__ . '/..' . '/themeplate/core/src/Field/TextareaField.php',
        'ThemePlate\\Core\\Field\\TypeField' => __DIR__ . '/..' . '/themeplate/core/src/Field/TypeField.php',
        'ThemePlate\\Core\\Fields' => __DIR__ . '/..' . '/themeplate/core/src/Fields.php',
        'ThemePlate\\Core\\Form' => __DIR__ . '/..' . '/themeplate/core/src/Form.php',
        'ThemePlate\\Core\\Handler' => __DIR__ . '/..' . '/themeplate/core/src/Handler.php',
        'ThemePlate\\Core\\Helper\\AssetsHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/AssetsHelper.php',
        'ThemePlate\\Core\\Helper\\BoxHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/BoxHelper.php',
        'ThemePlate\\Core\\Helper\\FieldsHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/FieldsHelper.php',
        'ThemePlate\\Core\\Helper\\FormHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/FormHelper.php',
        'ThemePlate\\Core\\Helper\\MainHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/MainHelper.php',
        'ThemePlate\\Core\\Helper\\MetaHelper' => __DIR__ . '/..' . '/themeplate/core/src/Helper/MetaHelper.php',
        'ThemePlate\\Core\\Repository' => __DIR__ . '/..' . '/themeplate/core/src/Repository.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf45f1254c29c05a0ed834bb7d3db31eb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf45f1254c29c05a0ed834bb7d3db31eb::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitf45f1254c29c05a0ed834bb7d3db31eb::$classMap;

        }, null, ClassLoader::class);
    }
}
