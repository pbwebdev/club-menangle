import { resolve } from 'path';

const configFile = resolve( process.cwd(), `tailwind.config.js` );
const defaultConfig = require( configFile );

/** @type {import('tailwindcss').Config} */
module.exports = {
	...defaultConfig,
	important: '.wp-block-themeplate',
	content: [
		'./**/*.php',
	],
};
