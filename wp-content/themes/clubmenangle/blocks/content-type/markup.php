<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

if ( empty( $attributes['anchor'] ) ) {
	$attributes['anchor'] = uniqid( 'section-' );
}

$attributes['count'] = 'event' === $attributes['type'] ? '2' : '3';

$ptype = get_post_type_object( $attributes['type'] );
$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => $attributes['type'],
		'posts_per_page' => $attributes['count'],
		'orderby'        => 'rand',
	)
);

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<section id="<?php echo $attributes['anchor']; ?>" class="py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32 not-prose">
	<?php echo cmau_views()->render( 'front-heading', [ 'title' => $attributes['heading'] ] ); ?>

	<div class="py-8 xl:py-16 grid md:grid-cols-2 gap-8 4xl:gap-16<?php echo '3' === $attributes['count'] ? ' xl:grid-cols-3' : ''; ?>">
		<?php while ( $query->have_posts() ) : ?>
			<?php $query->the_post(); ?>

			<div class="w-full">
				<?php get_template_part( 'template-parts/' . $attributes['type'], 'item' ); ?>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="text-center">
		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_post_type_archive_link( 'event' ),
			'text' => 'See ' . $ptype->labels->all_items,
		] ); ?>
	</div>
</section>
