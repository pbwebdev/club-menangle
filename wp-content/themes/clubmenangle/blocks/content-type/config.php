<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'type'    => array(
		'title'   => __( 'Post Type', 'club-menangle' ),
		'type'    => 'select',
		'options' => array(
			'post'  => 'News',
			'event' => 'Events',
		),
		'default' => 'post',
	),
	// 'count'  => array(
	// 	'title'   => __( 'Post Count', 'club-menangle' ),
	// 	'type'    => 'select',
	// 	'options' => array(
	// 		2 => 'Two',
	// 		3 => 'Three',
	// 	),
	// 	'default' => 2,
	// ),
);

return array(
	'inner_blocks'  => false,
	'custom_fields' => $fields,
);
