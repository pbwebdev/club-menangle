<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

$grid = 'grid gap-4 md:gap-8 2xl:gap-16 md:grid-cols-2';
$class = 'my-8 2xl:my-16 4xl:my-24 full:my-32';

$classes = array(
	'alignfull my-8 2xl:my-16 4xl:my-24 full:my-32',
	'grid gap-4 md:gap-8 2xl:gap-16 md:grid-cols-2',
);

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<section class="<?php echo implode( cmau_is_block_editor() ? '" data-tp-blocks="' : ' ', $classes ); ?>">
	<?php echo $content; ?>
</section>
