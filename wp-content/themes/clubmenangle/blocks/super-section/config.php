<?php

/**
 * @package ThemePlate
 */

return array(
	'allowed_blocks' => array(
		'cmau/super-item',
	),
	'template_blocks' => array(
		array( 'cmau/super-item', '' ),
		array( 'cmau/super-item', array( 'reverse' => 'true' ) ),
	),
	'template_lock'   => true,
);
