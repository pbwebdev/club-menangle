<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var WP_Block $block      Block instance.
 */

$attributes['reverse'] = '2' === $attributes['align'];

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div class="not-prose mb-8 2xl:mb-12 4xl:mb-24">
	<?php echo cmau_views()->render( 'front-heading', [
		'title' => $attributes['text'],
		'level' => $attributes['level'],
		'small' => true,
		'class' => '!w-full ' . ( $attributes['reverse'] ? 'text-right ml-auto' : '' ),
	] ); ?>
</div>
