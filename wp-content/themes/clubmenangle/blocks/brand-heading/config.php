<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'text'  => array(
		'title'   => __( 'Text', 'club-menangle' ),
		'type'    => 'text',
		'default' => __('Heading', 'club-menangle' ),
	),
	'align' => array(
		'title'   => __( 'Align', 'club-menangle' ),
		'type'    => 'radio',
		'options' => array(
			__( 'Left', 'club-menangle' ),
			__( 'Right', 'club-menangle' ),
		),
		'default' => 1,
	),
	'level' => array(
		'title' => __( 'Level', 'club-menangle' ),
		'type'  => 'select',
		'options' => array(
			'h1' => __( 'H1', 'club-menangle' ),
			'h2' => __( 'H2', 'club-menangle' ),
			'h3' => __( 'H3', 'club-menangle' ),
			'h4' => __( 'H4', 'club-menangle' ),
			'h5' => __( 'H5', 'club-menangle' ),
			'h6' => __( 'H6', 'club-menangle' ),
		),
		'default' => 'h2',
	),
);

return array(
	'inner_blocks'  => false,
	'custom_fields' => $fields,
);
