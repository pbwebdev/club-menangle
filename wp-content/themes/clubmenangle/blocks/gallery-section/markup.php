<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

if ( empty( $attributes['link']['text'] ) && ! empty( $attributes['link']['title'] ) ) {
	$attributes['link']['text'] = $attributes['link']['title'];
}

$attributes['setting']['reverse'] = cmau_is_true( $attributes['setting']['reverse'] );
$attributes['setting']['images']  = array_filter( $attributes['setting']['images'] );

if ( empty( $attributes['anchor'] ) ) {
	$attributes['anchor'] = uniqid( 'section-' );
}

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div id="<?php echo $attributes['anchor']; ?>" class="alignfull py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="not-prose px-8 2xl:px-16 4xl:px-24 full:px-32">
		<?php if ( $attributes['heading'] ) : ?>
			<?php echo cmau_views()->render( 'front-heading', [
				'title' => $attributes['heading'],
				'small' => true,
				'class' => $attributes['setting']['reverse'] ? 'text-right ml-auto' : '',
			] ); ?>
		<?php endif; ?>
	</div>

	<div class="py-8 xl:py-16 xl:grid xl:grid-cols-7 items-center">
		<div class="relative px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32 xl:col-span-3 h-full <?php echo $attributes['setting']['reverse'] ? 'order-1' : 'order-2'; ?>">
			<div class="2xl:py-16 4xl:py-24 full:py-32 max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl prose-invert text-white">
				<?php echo $content; ?>

				<?php if ( isset( $attributes['link']['url'], $attributes['link']['text'] ) ) : ?>
					<div class="mt-16 not-prose">
						<?php echo cmau_views()->render( 'global-button', $attributes['link'] + [
								'small' => true,
								'class' => 'hover:text-teak hover:bg-white',
							] ); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="absolute inset-0 bg-mineshaft -z-1 <?php echo $attributes['setting']['reverse'] ? 'xl:-end-16 2xl:-end-32 4xl:-end-64' : 'xl:-start-16 2xl:-start-32 4xl:-start-64'; ?>"></div>
		</div>

		<div class="relative xl:col-span-4 xl:py-8 2xl:py-16 4xl:py-24 full:py-32 <?php echo $attributes['setting']['reverse'] ? 'order-2' : 'order-1'; ?>">
			<div id="<?php echo $attributes['anchor']; ?>_slider" class="slider" data-arrows="true" data-dots="true">
				<?php foreach ( $attributes['setting']['images'] as $image ) : ?>
					<div class="not-prose relative">
						<?php if ( isset( $image['id'] ) ) : ?>
						<?php echo wp_get_attachment_image( $image['id'], 'tile-3_4', false, [
							'class' => 'aspect-16/10 object-cover object-center w-full brightness-75 group-hover:brightness-50'
						] ); ?>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#' . $attributes['anchor'] . '_slider', 'class' => 'absolute left-1/2 -translate-x-1/2 mt-8 2xl:mt-16'] ); ?>
		</div>
	</div>
</div>
