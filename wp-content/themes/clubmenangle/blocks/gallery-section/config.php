<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'link'    => array(
		'title' => __( 'Link', 'club-menangle' ),
		'type'  => 'link',
	),
	'setting' => array(
		'title'  => __( 'Settings', 'club-menangle' ),
		'type'   => 'group',
		'fields' => array(
			'reverse' => array(
				'title' => __( 'Reversed?', 'club-menangle' ),
				'type'  => 'checkbox',
			),
			'images'  => array(
				'title'    => __( 'Images', 'club-menangle' ),
				'type'     => 'file',
				'multiple' => true,
			),
		),
	),
);

return array(
	'custom_fields' => $fields,
);
