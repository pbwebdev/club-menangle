<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'count' => array(
		'title'   => __( 'Show Number', 'club-menangle' ),
		'type'    => 'number',
		'default' => '6',
	),
	'reverse' => array(
		'title' => __( 'Reversed?', 'club-menangle' ),
		'type'  => 'checkbox',
	),
);

return array(
	'inner_blocks'  => false,
	'custom_fields' => $fields,
);
