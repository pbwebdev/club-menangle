<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var WP_Block $block      Block instance.
 */

$attributes['reverse'] = cmau_is_true( $attributes['reverse'] );

$args = array(
	'post_status'    => 'publish',
	'post_type'      => 'sponsor',
	'posts_per_page' => -1,
);

$query = new WP_Query( $args );
$count = $attributes['count'] ?? 6;

if ( $query->post_count < $count ) {
	$count = $query->post_count;
}

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div class="alignfull px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="not-prose mb-8 2xl:mb-12 4xl:mb-24">
		<?php if ( $attributes['heading'] ) : ?>
		<?php echo cmau_views()->render( 'front-heading', [
			'title' => $attributes['heading'],
			'small' => true,
			'class' => $attributes['reverse'] ? 'text-right ml-auto' : '',
		] ); ?>
		<?php endif; ?>
	</div>

	<div class="relative">
		<div class="slider" id="sponsor_slider" data-show="<?php echo $count; ?>" data-arrows="true">
			<?php while ( $query->have_posts() ) : ?>
				<?php
					$query->the_post();

					$link = get_post_meta( get_the_ID(), 'cmau_sponsor_link', true );
					switch ( get_post_meta( get_the_ID(), 'cmau_sponsor_logo', true ) ) {
						default:
						case 1:
							$size = 'sponsor-boxed';
							break;

						case 2:
							$size = 'sponsor-wide';
							break;
					}
				?>

				<div class="not-prose w-full px-4 4xl:px-8">
					<?php if ( $link ) : ?>
						<a href="<?php echo $link; ?>" target="_blank" rel="noopener noreferrer">
					<?php endif; ?>
					<?php the_post_thumbnail( $size, ['class' => 'mx-auto'] ); ?>
					<?php if ( $link ) : ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>

		<?php echo cmau_views()->render( 'slider-arrows', [ 'control' => '#sponsor_slider' ] ); ?>
	</div>
</div>
