<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

global $cmau_options;

if ( empty( $attributes['uber']['text'] ) && ! empty( $attributes['uber']['title'] ) ) {
	$attributes['uber']['text'] = $attributes['uber']['title'];
}

if ( empty( $attributes['anchor'] ) ) {
	$attributes['anchor'] = uniqid( 'section-' );
}

$items = [
	[
		'text' => $attributes['phone'] . ( $attributes['time'] ? '<br>' . $attributes['time'] : '' ),
		'icon' => 'fas fa-phone-alt',
		'empty' => empty( $attributes['phone'] ),
	],
	[
		'text' => $attributes['address'],
		'icon' => 'fas fa-map-marker',
		'empty' => empty( $attributes['address'] ),
	],
	[
		'text' => isset( $attributes['uber']['url'] ) ? '<a href="' . $attributes['uber']['url'] . '">' . $attributes['uber']['text'] . '</a>' : '',
		'icon' => 'fab fa-uber',
		'empty' => empty( array_filter( $attributes['uber'] ) ),
	],
	[
		'text' => $attributes['postal'],
		'icon' => 'fas fa-fax',
		'empty' => empty( $attributes['postal'] ),
	],
	[
		'text' => $attributes['email'],
		'icon' => 'far fa-envelope',
		'empty' => empty( $attributes['email'] ),
	],
];

$api = $cmau_options['global_map'];

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div id="<?php echo $attributes['anchor']; ?>" class="alignfull py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="not-prose px-8 2xl:px-16 4xl:px-24 full:px-32">
		<?php if ( $attributes['heading'] ) : ?>
			<?php echo cmau_views()->render( 'front-heading', [
				'title' => $attributes['heading'],
				'small' => true,
			] ); ?>
		<?php endif; ?>
	</div>

	<div class="py-8 xl:py-16 grid xl:grid-cols-7">
		<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 xl:col-span-4">
			<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl pb-8 xl:py-16 xl:pe-8 2xl:pe-16 4xl:pe-24 full:pe-32">
				<ul class="grid gap-4 md:gap-8 pl-0 list-none not-prose">
					<?php foreach( $items as $item ) : ?>
						<?php if ( $item['empty'] ) continue; ?>

						<li class="flex gap-4 items-center">
							<span class="w-16 h-16 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
								<i class="text-3xl <?php echo $item['icon']; ?> fa-fw"></i>
							</span>
							<?php echo $item['text']; ?>
						</li>
					<?php endforeach; ?>
			</div>
		</div>

		<div class="not-prose relative w-full h-full xl:col-span-3 ps-8 xl:ps-0">
			<?php if ( $api && $attributes['lat'] && $attributes['lng'] && $attributes['zml'] ) : ?>
				<div id="cmau_map" class="holder" data-api="<?php echo $api; ?>" data-lat="<?php echo $attributes['lat']; ?>" data-lng="<?php echo $attributes['lng']; ?>" data-zml="<?php echo $attributes['zml']; ?>" style="padding-top: 100%;"></div>
			<?php endif; ?>

			<div class="absolute w-full h-full bg-pearlbush -z-10 -start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32"></div>
		</div>
	</div>
</div>
