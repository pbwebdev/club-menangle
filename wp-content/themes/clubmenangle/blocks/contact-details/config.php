<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'phone'   => array(
		'title' => __( 'Phone', 'club-menangle' ),
		'type'  => 'text',
	),
	'time'    => array(
		'title' => __( 'Time', 'club-menangle' ),
		'type'  => 'text',
	),
	'address' => array(
		'title' => __( 'Address', 'club-menangle' ),
		'type'  => 'text',
	),
	'uber'    => array(
		'title' => __( 'Uber Mobile', 'club-menangle' ),
		'type'  => 'link',
	),
	'postal'  => array(
		'title' => __( 'Postal', 'club-menangle' ),
		'type'  => 'text',
	),
	'email'   => array(
		'title' => __( 'Email', 'club-menangle' ),
		'type'  => 'text',
	),
	'lat' => array(
		'title' => __( 'Latitude', 'club-menangle' ),
		'type'  => 'text',
	),
	'lng' => array(
		'title' => __( 'Longitude', 'club-menangle' ),
		'type'  => 'text',
	),
	'zml' => array(
		'title' => __( 'Zoom', 'club-menangle' ),
		'type'  => 'number',
	),
);

return array(
	'inner_blocks'  => false,
	'custom_fields' => $fields,
);
