<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'image'   => array(
		'title' => __( 'Image', 'club-menangle' ),
		'type'  => 'file',
	),
	'url'     => array(
		'title' => __( 'URL', 'club-menangle' ),
		'type'  => 'url',
	),
	'reverse' => array(
		'title' => __( 'Reversed?', 'club-menangle' ),
		'type'  => 'checkbox',
	),
);

return array(
	'custom_fields' => $fields,
);
