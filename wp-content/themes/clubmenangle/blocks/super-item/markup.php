<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

$attributes['reverse'] = cmau_is_true( $attributes['reverse'] );

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div class="py-8 2xl:py-16 4xl:py-24 full:py-32 <?php echo $attributes['reverse'] ? 'pe-8 2xl:pe-16 4xl:pe-24 full:pe-32' : 'ps-8 2xl:ps-16 4xl:ps-24 full:ps-32'; ?>">
	<div class="not-prose relative">
		<?php if ( isset( $attributes['image']['id'] ) ) : ?>
		<?php echo wp_get_attachment_image( $attributes['image']['id'], 'tile-1_2', false, [
			'class' => 'aspect-16/10 object-cover object-center w-full brightness-75'
		] ); ?>
		<?php endif; ?>

		<div class="absolute bottom-8 start-8 end-8 2xl:bottom-16 2xl:start-16 2xl:end-16 4xl:bottom-24 full:bottom-32 4xl:start-24 full:start-32 4xl:end-24 full:end-32 max-w-xs">
			<?php if ( $attributes['heading'] ) : ?>
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => $attributes['heading'],
					'link'  => $attributes['url'] ?: '#',
					'small' => true,
				] ); ?>
			<?php endif; ?>
		</div>

		<div class="absolute w-full h-full bg-pearlbush -z-10 <?php echo $attributes['reverse'] ? '-end-8 2xl:-end-16 4xl:-end-24 full:-end-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32' : '-start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -top-8 2xl:-top-16 4xl:-top-24 full:-top-32'; ?>"></div>
	</div>

	<div class="pt-8 2xl:pt-16 4xl:pt-24 full:pt-32 px-8 2xl:px-16 4xl:px-24 full:px-32 <?php echo $attributes['reverse'] ? 'mt-6' : 'mt-0 md:mt-6'; ?> xl:mt-12">
		<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl prose-headings:leading-relaxed">
			<?php echo $content; ?>
		</div>
	</div>
</div>
