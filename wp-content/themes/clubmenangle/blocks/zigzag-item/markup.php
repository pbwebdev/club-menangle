<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

if ( empty( $attributes['link']['text'] ) && ! empty( $attributes['link']['title'] ) ) {
	$attributes['link']['text'] = $attributes['link']['title'];
}

$attributes['reverse'] = cmau_is_true( $attributes['reverse'] );

if ( empty( $attributes['anchor'] ) ) {
	$attributes['anchor'] = uniqid( 'section-' );
}

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div id="<?php echo $attributes['anchor']; ?>" class="alignfull py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="not-prose px-8 2xl:px-16 4xl:px-24 full:px-32<?php echo $attributes['reverse'] ? ' pb-8 2xl:pb-16 4xl:pb-24 full:pb-32' : ''; ?>">
		<?php if ( $attributes['heading'] ) : ?>
			<?php echo cmau_views()->render( 'front-heading', [
				'title' => $attributes['heading'],
				'small' => true,
				'class' => $attributes['reverse'] ? 'text-right ml-auto' : '',
			] ); ?>
		<?php endif; ?>
	</div>

	<div class="py-8 xl:py-16 grid xl:grid-cols-7">
		<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 xl:col-span-4 <?php echo $attributes['reverse'] ? 'pt-8 xl:pt-16 order-2' : 'order-1'; ?>">
			<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl <?php echo $attributes['reverse'] ? 'pb-8 xl:pt-16 xl:ps-8 2xl:ps-16 4xl:ps-24 full:ps-32' : 'pb-8 xl:py-16 xl:pe-8 2xl:pe-16 4xl:pe-24 full:pe-32'; ?>">
				<?php echo $content; ?>

				<?php if ( isset( $attributes['link']['url'], $attributes['link']['text'] ) ) : ?>
					<div class="mt-16 not-prose">
						<?php echo cmau_views()->render( 'global-button', $attributes['link'] + [
								'small' => true,
							] ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="not-prose relative w-full h-full xl:col-span-3 <?php echo $attributes['reverse'] ? 'pe-8 xl:pe-0 order-1' : 'ps-8 xl:ps-0 order-2'; ?>">
			<?php if ( isset( $attributes['image']['id'] ) ) : ?>
			<?php echo wp_get_attachment_image( $attributes['image']['id'], 'tile-1_2', false, [
				'class' => 'aspect-16/10 object-cover object-center w-full h-full brightness-75'
			] ); ?>
			<?php endif; ?>

			<div class="absolute w-full h-full bg-pearlbush -z-10 <?php echo $attributes['reverse'] ? 'end-0 2xl:-end-16 4xl:-end-24 full:-end-32 -top-8 2xl:-top-16 4xl:-top-24 full:-top-32' : '-start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32'; ?>"></div>
		</div>
	</div>
</div>
