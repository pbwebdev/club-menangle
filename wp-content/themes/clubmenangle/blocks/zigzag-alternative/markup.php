<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var string   $content    Block inner content.
 * @var WP_Block $block      Block instance.
 */

if ( empty( $attributes['link']['text'] ) && ! empty( $attributes['link']['title'] ) ) {
	$attributes['link']['text'] = $attributes['link']['title'];
}

$attributes['reverse'] = cmau_is_true( $attributes['reverse'] );

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div class="alignfull my-8 2xl:my-16 4xl:my-24 full:my-32 grid xl:grid-cols-7">
	<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 xl:py-8 2xl:py-16 4xl:py-24 full:py-32 my-8 2xl:my-12 4xl:my-24 xl:col-span-4 <?php echo $attributes['reverse'] ? 'order-1' : 'order-2'; ?>">
		<?php if ( $attributes['heading'] ) : ?>
			<?php echo cmau_views()->render( 'item-title', [
				'title' => $attributes['heading'],
				'class' => 'not-prose text-teak uppercase mb-8 2xl:mb-12 4xl:mb-24',
			] ); ?>
		<?php endif; ?>

		<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl">
			<?php echo $content; ?>

			<?php if ( isset( $attributes['link']['url'], $attributes['link']['text'] ) ) : ?>
				<div class="my-4 not-prose">
					<?php echo cmau_views()->render( 'global-button', $attributes['link'] + [
						'small' => true,
					] ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="py-8 2xl:py-16 4xl:py-24 full:py-32 xl:col-span-3 h-full <?php echo $attributes['reverse'] ? 'order-2' : 'order-1'; ?>">
		<div class="not-prose relative w-full h-full">
			<?php if ( isset( $attributes['image']['id'] ) ) : ?>
			<?php echo wp_get_attachment_image( $attributes['image']['id'], 'tile-1_2', false, [
				'class' => 'object-cover object-center w-full h-full brightness-75'
			] ); ?>
			<?php endif; ?>

			<div class="absolute w-full -z-10 -top-8 2xl:-top-16 4xl:-top-24 full:-top-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32 <?php echo $attributes['reverse'] ? 'bg-mineshaft start-8 2xl:start-16 4xl:start-24 full:start-32' : 'bg-pearlbush end-8 2xl:end-16 4xl:end-24 full:end-32'; ?>"></div>
		</div>
	</div>
</div>
