<?php

/**
 * @package ThemePlate
 */

$fields = array(
	'heading' => array(
		'title' => __( 'Heading', 'club-menangle' ),
		'type'  => 'text',
	),
	'image'   => array(
		'title' => __( 'Image', 'club-menangle' ),
		'type'  => 'file',
	),
	'link'    => array(
		'title' => __( 'Link', 'club-menangle' ),
		'type'  => 'link',
	),
	'reverse' => array(
		'title' => __( 'Reversed?', 'club-menangle' ),
		'type'  => 'checkbox',
	),
);

return array(
	'custom_fields' => $fields,
);
