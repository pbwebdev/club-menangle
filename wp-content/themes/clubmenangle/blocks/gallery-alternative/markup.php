<?php

/**
 * @package ThemePlate
 */

/**
 * @var array    $attributes Block attributes.
 * @var WP_Block $block      Block instance.
 */

$attributes['setting']['reverse'] = cmau_is_true( $attributes['setting']['reverse'] );
$attributes['setting']['images']  = array_filter( $attributes['setting']['images'] );

if ( empty( $attributes['anchor'] ) ) {
	$attributes['anchor'] = uniqid( 'section-' );
}

// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
?>

<div id="<?php echo $attributes['anchor']; ?>" class="alignfull mb-8 px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="not-prose mb-8 2xl:mb-12 4xl:mb-24">
		<?php if ( $attributes['heading'] ) : ?>
		<?php echo cmau_views()->render( 'front-heading', [
			'title' => $attributes['heading'],
			'small' => true,
			'class' => $attributes['setting']['reverse'] ? 'text-right ml-auto' : '',
		] ); ?>
		<?php endif; ?>
	</div>

	<div class="relative w-full mb-8 py-8 2xl:py-16 4xl:py-24 full:py-32">
		<div id="<?php echo $attributes['anchor']; ?>_slider" class="slider" data-arrows="true" data-dots="true">
			<?php foreach ( $attributes['setting']['images'] as $image ) : ?>
				<div class="not-prose relative">
					<?php if ( isset( $image['id'] ) ) : ?>
					<?php echo wp_get_attachment_image( $image['id'], 'tile-1_1', false, [
						'class' => 'aspect-2/1 object-cover object-center w-full brightness-75 group-hover:brightness-50'
					] ); ?>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>

		<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#' . $attributes['anchor'] . '_slider', 'class' => 'absolute left-1/2 -translate-x-1/2 pt-8 mt-8 2xl:mt-16 4xl:mt-24 full:mt-32'] ); ?>

		<div class="absolute w-1/2 h-full bg-mineshaft -z-10 top-0 <?php echo $attributes['setting']['reverse'] ? '-left-8 2xl:-start-16 4xl:-start-24 full:-start-32' : 'right-0 -end-8 2xl:-end-16 4xl:-end-24 full:-end-32'; ?>"></div>
	</div>
</div>
