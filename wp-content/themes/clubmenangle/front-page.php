<?php

/**
 * The template for displaying pages
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/front', 'super' ); ?>
		<?php get_template_part( 'template-parts/front', 'events' ); ?>
		<?php get_template_part( 'template-parts/front', 'spaces' ); ?>
		<?php get_template_part( 'template-parts/front', 'posts' ); ?>
		<?php get_template_part( 'template-parts/front', 'about' ); ?>

	</main><!-- .content -->

<?php

get_footer();

