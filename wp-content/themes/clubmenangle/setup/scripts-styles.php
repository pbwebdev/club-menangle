<?php

/**
 * Enqueue scripts and styles
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'cmau_scripts_styles_registration' ) ) {
	function cmau_scripts_styles_registration() {
		$suffix = ( SCRIPT_DEBUG || CLUB_MENANGLE_THEME_DEBUG ) ? '' : '.min';

		if ( ! is_admin() ) {
			// Deregister the jquery version bundled with WordPress
			wp_deregister_script( 'jquery' );
			wp_deregister_script( 'jquery-migrate' );
		}

		// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header
		wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery' . $suffix . '.js', array(), '3.7.1' );
		wp_register_script( 'jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.4.1/jquery-migrate' . $suffix . '.js', array(), '3.4.1' );
		wp_add_inline_script( 'jquery', 'jQuery.noConflict();' );

		// Font Awesome
		wp_register_script( 'cmau-fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all' . $suffix . '.js', array(), '6.4.2', array( 'strategy' => 'defer', 'in_footer' => false ) );
		wp_add_inline_script( 'cmau-fontawesome', 'FontAwesomeConfig = { searchPseudoElements: true };' );
		// AlpineJS
		wp_register_script( 'cmau-alpinejs', 'https://cdnjs.cloudflare.com/ajax/libs/alpinejs/3.13.9/cdn' . $suffix . '.js', array(), '3.13.9', array( 'in_footer' => true, 'strategy' => 'defer' ) );
		// SlickJS
		wp_register_script( 'cmau-slickjs', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick' . $suffix . '.js', array(), '1.8.1', array( 'in_footer' => true ) );
		wp_register_style( 'cmau-slickcss', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick' . $suffix . '.css', array(), '1.8.1' );
	}
	add_action( 'init', 'cmau_scripts_styles_registration', 5 );
}

if ( ! function_exists( 'cmau_scripts_styles_early' ) ) {
	function cmau_scripts_styles_early() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'cmau-fontawesome' );
	}
	add_action( 'wp_enqueue_scripts', 'cmau_scripts_styles_early', 5 );
}

if ( ! function_exists( 'cmau_scripts_styles_late' ) ) {
	function cmau_scripts_styles_late() {
		$suffix = ( SCRIPT_DEBUG || CLUB_MENANGLE_THEME_DEBUG ) ? '' : '.min';
		$theme  = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		wp_enqueue_script( 'cmau-alpinejs' );
		wp_enqueue_script( 'cmau-slickjs' );
		wp_enqueue_style( 'cmau-slickcss' );
		wp_deregister_script( 'cardanopress-alpinejs' );

		// Site scripts and styles
		wp_enqueue_style( 'cmau-style', CLUB_MENANGLE_THEME_URL . 'assets/css/club-menangle' . $suffix . '.css', array(), $theme->get( 'Version' ) );
		wp_enqueue_script( 'cmau-script', CLUB_MENANGLE_THEME_URL . 'assets/js/club-menangle' . $suffix . '.js', array(), $theme->get( 'Version' ), true );

		$cmau_options = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'cmau-script', 'cmau_options', apply_filters( 'cmau_localize_script', $cmau_options ) );
	}
	add_action( 'wp_enqueue_scripts', 'cmau_scripts_styles_late', 20 );
}

if ( ! function_exists( 'cmau_editor_scripts_styles' ) ) {
	function cmau_editor_scripts_styles() {
		$suffix = ( SCRIPT_DEBUG || CLUB_MENANGLE_THEME_DEBUG ) ? '' : '.min';
		$theme  = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		wp_enqueue_script( 'cmau-slickjs' );
		wp_enqueue_style( 'cmau-slickcss' );
		wp_enqueue_script( 'cmau-fontawesome' );
		wp_enqueue_script( 'cmau-editor', CLUB_MENANGLE_THEME_URL . 'assets/js/cmau-editor' . $suffix . '.js', array( 'wp-hooks' ), $theme->get( 'Version' ) );
		wp_enqueue_style( 'cmau-editor', CLUB_MENANGLE_THEME_URL . 'assets/css/cmau-editor' . $suffix . '.css', array(), $theme->get( 'Version' ) );
	}
	add_action( 'enqueue_block_editor_assets', 'cmau_editor_scripts_styles' );
}
