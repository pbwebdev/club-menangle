<?php

/**
 * Register navigation menus
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'cmau_navigations' ) ) {
	function cmau_navigations() {
		require_once CLUB_MENANGLE_THEME_PATH . 'includes/class-clean-navbar.php';

		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'club-menangle' ),
			'mobile' => __( 'Mobile Menu', 'club-menangle' ),
			'widget-menu-1'  => __( 'Widget Menu 1', 'club-menangle' ),
			'widget-menu-2'  => __( 'Widget Menu 2', 'club-menangle' ),
			'footer'  => __( 'Footer Menu', 'club-menangle' ),
		) );
	}
	add_action( 'after_setup_theme', 'cmau_navigations' );
}

// Primary Menu
if ( ! function_exists( 'cmau_primary_menu' ) ) {
	function cmau_primary_menu( $classes = array() ) {
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'menu_class'     => implode( ' ', $classes ),
			'walker'         => new Clean_NavBar(),
			'depth'          => 0,
		) );
	}
}

// Mobile Menu
if ( ! function_exists( 'cmau_mobile_menu' ) ) {
	function cmau_mobile_menu( $classes = array() ) {
		wp_nav_menu( array(
			'theme_location' => 'mobile',
			'menu_class'     => implode( ' ', $classes ),
			'depth'          => 2,
		) );
	}
}

// Widget Menu
if ( ! function_exists( 'cmau_widget_menu_1' ) ) {
	function cmau_widget_menu_1( $classes = array() ) {
		wp_nav_menu( array(
			'theme_location' => 'widget-menu-1',
			'menu_class'     => implode( ' ', $classes ),
			'depth'          => 2,
		) );
	}
}

if ( ! function_exists( 'cmau_widget_menu_2' ) ) {
	function cmau_widget_menu_2( $classes = array() ) {
		wp_nav_menu( array(
			'theme_location' => 'widget-menu-2',
			'menu_class'     => implode( ' ', $classes ),
			'depth'          => 2,
		) );
	}
}


// Footer Menu
if ( ! function_exists( 'cmau_footer_menu' ) ) {
	function cmau_footer_menu( $classes = array() ) {
		wp_nav_menu( array(
			'theme_location' => 'footer',
			'menu_class'     => implode( ' ', $classes ),
			'depth'          => 1,
		) );
	}
}

add_filter( 'nav_menu_link_attributes', function( $atts, $menu_item, $args, $depth ) {
	if ( in_array( $args->theme_location, array( 'widget-menu-1', 'widget-menu-2', 'footer' ) ) ) {
		$atts['class'] = 'py-2 border-b-4 border-transparent hover:border-teak';

		if ( isset( $menu_item->current ) && $menu_item->current ) {
			$atts['class'] .= ' !border-teak';
		}

		if ( in_array( $args->theme_location, array( 'widget-menu-1', 'widget-menu-2' ) ) ) {
			if ( $depth == 0 ) {
				$atts['class'] .= ' text-xl 2xl:text-2xl 4xl:text-3xl';
			} else {
				$atts['class'] .= ' py-2 2xl:text-lg 4xl:text-xl';
			}
		}
	}

	return $atts;
}, 10, 4 );

add_filter( 'wp_nav_menu_args', function( $args ) {
	add_filter( 'nav_menu_submenu_css_class', function( $classes, $args ) {
		if ( in_array( $args->theme_location, array( 'widget-menu-1', 'widget-menu-2' ) ) ) {
			$classes = array( 'flex', 'flex-wrap', 'flex-col', 'gap-2', '4xl:gap-4', 'mt-8' );
		}

		if ( 'mobile' === $args->theme_location ) {
			$classes = array( 'mt-4', 'flex', 'flex-col', 'gap-3' );
		}

		return $classes;
	}, PHP_INT_MAX, 2 );

	add_filter( 'nav_menu_css_class', function( $classes, $menu_item, $args ) {
		if ( in_array( $args->theme_location, array( 'widget-menu-1', 'widget-menu-2' ) ) ) {
			$classes = array( 'py-2' );
		}

		return $classes;
	}, PHP_INT_MAX, 3 );

	add_filter( 'nav_menu_item_attributes', function( $atts, $menu_item, $args ) {
		if ( 'primary' !== $args->theme_location ) {
			return $atts;
		}

		if ( $args->walker->has_children ) {
			$atts['x-data'] = "{open: false}";
			$atts['x-on:mouseleave'] = "open = false";
		}

		return $atts;
	}, PHP_INT_MAX, 3 );

	add_filter( 'nav_menu_link_attributes', function( $atts, $menu_item, $args, $depth ) {
		if ( 'mobile' === $args->theme_location ) {
			$classes = explode( ' ', $atts['class'] ?? '' );

			if ( ! $depth ) {
				$classes[] = 'text-xl';
			}

			if ( isset( $menu_item->current ) && $menu_item->current ) {
				$classes[] = 'border-b-4';
				$classes[] = 'border-teak';
			}

			$atts['class'] = implode( ' ', array_unique( array_filter( $classes ) ) );
		}

		return $atts;
	}, PHP_INT_MAX, 4 );

	add_filter( 'nav_menu_submenu_attributes', function( $atts, $args ) {
		if ( 'primary' !== $args->theme_location ) {
			return $atts;
		}
		$atts['x-cloak'] = true;
		$atts['x-show'] = "open";
		$atts['x-transition:enter'] = "transition origin-top ease-out duration-300";
		$atts['x-transition:enter-start'] = "opacity-0 scale-y-90";
		$atts['x-transition:enter-end'] = "opacity-100 scale-y-100";
		$atts['x-transition:leave'] = "transition origin-top ease-in duration-150";
		$atts['x-transition:leave-start'] = "opacity-100 scale-y-100";
		$atts['x-transition:leave-end'] = "opacity-0 scale-y-90";

		return $atts;
	}, PHP_INT_MAX, 2 );

	return $args;
}, 11 );
