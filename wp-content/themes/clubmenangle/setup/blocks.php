<?php

/**
 * Setup custom blocks
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'cmau_blocks_init' ) ) {
	function cmau_blocks_init() {
		register_block_style( 'core/heading', array(
			'name'  => 'cmau-section',
			'label' => __( 'CMAU: Section', 'themeslug' ),
		) );
	}
	add_action( 'init', 'cmau_blocks_init' );
	( new ThemePlate\Blocks\CustomBlocks( CLUB_MENANGLE_THEME_PATH . 'blocks' ) )->init();
}

// Add custom block category.
if ( ! function_exists( 'cmau_block_categories' ) ) {
	function cmau_block_categories( $categories ) {
		$theme = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		if ( ! in_array( CLUB_MENANGLE_THEME_BASE, array_column( $categories, 'slug' ) ) ) {
			array_unshift(
				$categories,
				array(
					'title' => $theme->get( 'Name' ),
					'slug'  => CLUB_MENANGLE_THEME_BASE,
				)
			);
		}

		return $categories;
	}
	add_filter( 'block_categories_all', 'cmau_block_categories' );
}
