<?php

/**
 * Register widget areas
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'cmau_widgets_init' ) ) {
	function cmau_widgets_init() {
		register_sidebar( array(
			'id'            => 'sidebar',
			'name'          => __( 'Sidebar Area', 'club-menangle' ),
			'description'   => __( 'Add widgets here to appear in sidebar.', 'club-menangle' ),
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
			'before_widget' => '<section class="widget %2$s">',
			'after_widget'  => '</section>',
		) );
		register_sidebars( 3, array(
			'id'            => 'footer',
			/* translators: count */
			'name'          => __( 'Footer Area %d', 'club-menangle' ),
			'description'   => __( 'Add widgets here to appear in footer.', 'club-menangle' ),
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
			'before_widget' => '<section class="widget %2$s p-4 w-full md:w-1/3">',
			'after_widget'  => '</section>',
		) );
	}
	add_action( 'widgets_init', 'cmau_widgets_init' );
}
