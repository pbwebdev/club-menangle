<?php

/**
 * Setup FacetWP
 *
 * @package Club Menangle
 * @since 0.1.0
 */

add_filter( 'facetwp_shortcode_html', function( $output ) {
	$output = str_replace( 'facetwp-facet', '!mb-0 max-w-full max-sm:w-full facetwp-facet', $output );
	$output = str_replace( 'facetwp-type-radio', 'facetwp-type-radio flex flex-col items-start', $output );

	return $output;
} );

add_filter( 'facetwp_facet_html', function( $output, $params ) {
	if ( 'dropdown' === $params['facet']['type'] ) {
		$class  = array(
			'facetwp-dropdown',
			'border border-mineshaft max-w-full max-sm:w-full',
			'py-3 px-8 3xl:py-6 3xl:px-12 5xl:py-9 5xl:px-20 !pr-28',
			'text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl uppercase',
			'focus:ring-0 focus:ring-offset-0 focus:border-teak focus:text-teak',
			'hover:border-teak hover:text-teak',
		);

		return str_replace(
			'facetwp-dropdown',
			implode( ' ', $class ),
			$output
		);
	}

	if ( 'radio' === $params['facet']['type'] ) {
		$class  = array(
			'facetwp-radio',
			'pb-2 border-b-4 border-transparent hover:border-teak',
			'mt-4 2xl:mt-8 text-xl 2xl:text-2xl 4xl:text-3xl',
			'inline-block !bg-none !mb-0 !pl-0'
		);

		return str_replace(
			'facetwp-radio',
			implode( ' ', $class ),
			$output
		);
	}

	if ( 'pager' === $params['facet']['type'] ) {
		return str_replace(
			'facetwp-type-pager',
			'facetwp-type-pager text-center',
			$output
		);
	}

	return $output;
}, 10, 2 );

add_filter( 'facetwp_index_row', function( $params ) {
	if ( in_array( $params['facet_name'], ['event_date', 'news_date'], true ) ) {
		$date = strtotime( $params['facet_value'] );
		$params['facet_value'] = date( 'Y-m', $date );
		$params['facet_display_value'] = date( 'M Y', $date );
	}

	return $params;
} );

add_filter( 'facetwp_query_args', function( $args, $class ) {
	if ( 'event' !== ( $args['post_type'] ?? false ) ) {
		return $args;
	}

	if ( $args['facetwp'] ?? false ) {
		$args['posts_per_page'] = -1;
		$args['meta_key'] = 'cmau_event_date';
		$args['meta_type'] = 'DATE';
		$args['meta_compare'] = 'EXISTS';

		if ( isset( $args['at-archive'] ) ) {
			$args['orderby'] = array(
				'meta_value' => 'DESC',
				'menu_order' => 'ASC',
				'post_date'  => 'DESC',
			);
		} else {
			$args['orderby'] = array(
				'meta_value' => 'ASC',
				'menu_order' => 'DESC',
				'post_date'  => 'ASC',
			);
		}
	}

	return $args;
}, 10, 2 );
