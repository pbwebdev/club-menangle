<?php

/**
 * The template for displaying the header
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

$links = array(
	'book' => 'far fa-calendar-check',
	'menu' => 'fas fa-bars',
);

?>

<!DOCTYPE html>
<html class="no-js text-sm 2xl:text-xs 4xl:text-[10px]" <?php language_attributes(); ?>>
	<head>
		<script type="text/javascript">document.documentElement.className = document.documentElement.className.replace( 'no-js', 'js' );</script>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<?php wp_head(); ?>
	</head>

	<body <?php body_class( 'max-w-[1440px] mx-auto overflow-x-hidden font-muli text-mineshaft' ); ?> x-data="{ overlaySection: false, subscribeSection: false }">
		<?php wp_body_open(); ?>

		<a class="screen-reader-text" href="#site-content"><?php esc_html_e( 'Skip to content', 'club-menangle' ); ?></a>

		<header class="site-header relative bg-white z-10">
			<div class="flex sm:items-center justify-between 4xl:text-lg 5xl:text-xl full:text-2xl uppercase text-center">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="flex items-center bg-teak">
					<?php
					echo wp_get_attachment_image( $cmau_options['global_logo'], 'full', false, array(
						'alt'         => get_bloginfo( 'name' ),
						'class'       => 'max-w-[120px] xs:max-w-[150px] sm:max-w-[200px] md:max-w-none',
						'data-unlazy' => 'true',
					));
					?>
				</a>

				<div class="hidden xl:flex px-8">
					<nav class="sitenav">
						<div class="hidden text-center items-center gap-8 4xl:gap-10"></div>
						<?php cmau_primary_menu( array( 'flex items-center gap-8 4xl:gap-10' ) ); ?>
					</nav>
				</div>

				<div class="flex pr-4 md:pr-8 4xl:pr-12 sm:gap-4 xl:gap-6 4xl:gap-10">
					<?php foreach ( $links as $type => $icon ) : ?>
						<?php $text = $cmau_options[ 'global_' . $type ]; ?>
						<button
							class="
								flex max-sm:flex-col gap-3 xl:gap-4 4xl:gap-6 items-center justify-center
								bg-transparent hover:bg-mineshaft hover:text-white
								sm:border sm:border-mineshaft hover:border-transparent
								p-4 2xl:px-6 4xl:px-10
								<?php echo 'menu' === $type ? 'xl:hidden' : ''; ?>
							"
							aria-expanded="false"
							aria-controls="overlay-section"
							aria-label="Toggle <?php echo $text; ?>"
							x-on:click="overlaySection = '<?php echo $type; ?>'"
						>
							<i class="<?php echo $icon; ?> fa-xl"></i>
							<span class="hidden xs:block max-sm:text-sm !leading-none"><?php echo $text; ?></span>
						</button>
					<?php endforeach; ?>
				</div>
			</div>
		</header><!-- .site-header -->

		<?php get_template_part( 'template-parts/global', 'overlay' ); ?>

		<div id="site-content" class="site-content">
			<?php get_template_part( 'template-parts/global', 'banner' ); ?>
