<?php

/**
 * The main template file
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

?>

<main class="content">
	<?php if ( is_404() ) : ?>
		<div class="-my-8 md:-my-12 xl:-my-16 3xl:-my-24 5xl:-my-32"></div>
	<?php else : ?>
		<div class="my-8 2xl:my-16 4xl:my-24 full:my-32">
			<?php if ( is_home() ) : ?>
				<div class="flex flex-wrap items-center justify-center gap-4 md:gap-8 px-8 2xl:px-16 4xl:px-24 full:px-32">
					<?php echo do_shortcode( '[facetwp facet="news_date"]' ); ?>
					<?php echo do_shortcode( '[facetwp facet="news_category"]' ); ?>
				</div>
			<?php endif; ?>

			<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => is_home() ? 'News' : '',
					'small' => true,
					'class' => is_home() ? 'my-8 2xl:my-16 4xl:my-24 full:my-32' : '',
				] ); ?>
			</div>

			<div class="grid md:grid-cols-2 gap-8 md:gap-16 3xl:gap-24 5xl:gap-32 px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<?php while ( have_posts() ) : ?>
					<?php the_post(); ?>

					<article id="news-<?php the_ID(); ?>" <?php post_class( 'w-full' ); ?>>
						<?php get_template_part( 'template-parts/post', 'item' ); ?>
					</article><!-- #post-<?php the_ID(); ?> -->
				<?php endwhile; ?>
			</div>

			<?php the_posts_pagination( array( 'class' => 'text-center' ) ); ?>
		</div>
	<?php endif; ?>
</main>

<?php

get_footer();
