<?php

/**
* The template for displaying events section
*
* @package Club Menangle
* @since 0.1.0
*/

$p_id  = get_the_ID();
$date  = get_post_meta( $p_id, 'cmau_event_date', true );
$venue = get_post_meta( $p_id, 'cmau_event_venue', true );

?>

<div class="relative">
	<?php the_post_thumbnail( 'tile-1_2', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

	<div class="absolute left-8 top-8 xl:left-12 xl:top-12">
		<?php echo cmau_views()->render( 'item-lead', [
			'raw'   => true,
			'small' => true,
			'text'  => get_the_term_list( $p_id, 'event-category', '', '&nbsp;' ),
			'class' => 'text-white flex gap-2',
		] ); ?>
	</div>
</div>

<div class="flex items-center py-8 4xl:py-12">
	<?php echo cmau_views()->render( 'item-lead', [
		'small' => true,
		'text'  => date( 'j M', strtotime( $date ) ),
		'class' => 'w-24 h-24 4xl:w-32 4xl:h-32 5xl:w-36 5xl:h-36 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white uppercase',
	] ); ?>

	<div class="flex-1 ps-4 md:ps-8 full:ps-16">
		<?php echo cmau_views()->render( 'item-title', [
			'title' => get_the_title(),
			'link'  => get_the_permalink(),
			'class' => 'mb-6',
		] ); ?>
		<?php echo cmau_views()->render( 'item-lead', ['text' => $venue] ); ?>
	</div>
</div>
