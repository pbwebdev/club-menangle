<?php

/**
 * The template for displaying gallery section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args ) || empty( $args['images'] ) ) {
	return;
}

$attributes = array(
	'anchor'  => $args['anchor'] ?? '',
	'heading' => $args['heading'] ?? 'Venue Gallery',
	'setting' => array(
		'reverse' => $args['reverse'] ?? false,
		'images'  => array_map(
			function( $image ) {
				return array(
					'id' => $image,
				);
			},
			$args['images'] ?? array()
		),
	),
);

echo cmau_views()->render( 'block::gallery-alternative/markup', compact( 'attributes' ) );
