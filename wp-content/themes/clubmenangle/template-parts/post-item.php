<?php

/**
* The template for displaying news item
*
* @package Club Menangle
* @since 0.1.0
*/

?>

<div class="relative">
	<?php the_post_thumbnail( 'tile-1_3', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

	<div class="absolute right-8 top-8 xl:right-12 xl:top-12">
		<?php echo cmau_views()->render( 'item-lead', [
			'raw'   => true,
			'small' => true,
			'text'  => get_the_term_list( get_the_ID(), 'category', '', '&nbsp;' ),
			'class' => 'text-white flex gap-2',
		] ); ?>
	</div>
</div>

<?php echo cmau_views()->render( 'item-title', [
	'title' => get_the_title(),
	'link'  => get_the_permalink(),
	'class' => 'py-8 4xl:py-12',
] ); ?>

<?php echo cmau_views()->render( 'partial/open-item-excerpt' ); ?>
	<p class="mb-6 4xl:mb-8"><?php echo get_the_excerpt(); ?></p>
	<p><a href="<?php the_permalink(); ?>" class="text-teak hover:text-mineshaft">Read more</a></p>
<?php echo cmau_views()->render( 'partial/close' ); ?>
