<?php

/**
 * The template for displaying spaces section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$spaces = get_post_meta( get_the_ID(), 'cmau_venue_spaces', false );

if ( empty( $spaces ) ) {
	return;
}

?>

<div id="venue-spaces" class="py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
		<?php echo cmau_views()->render( 'front-heading', [
			'title' => 'Venue Spaces',
			'small' => true,
		] ); ?>
	</div>

	<div id="venue-spaces_slider" class="slider" data-arrows="true" data-dots="true">
		<?php foreach ( $spaces as $index => $space ) : ?>
			<?php
			$space = array_merge( array(
				'heading' => '',
				'content' => '',
				'image'   => '',
				'button'  => array(),
			), $space );

			$space['button'] = array_merge( array(
				'url'  => '',
				'text' => '',
			), $space['button'] );
			?>

			<div class="relative w-full">
				<div class="py-8 xl:py-16 xl:grid xl:grid-cols-7 items-center">
					<div class="xl:py-8 2xl:py-16 4xl:py-24 full:py-32 xl:col-span-3">
						<?php echo wp_get_attachment_image( $space['image'], 'tile-1_2', false, [
							'class' => 'aspect-square object-cover object-center w-full brightness-75 group-hover:brightness-50'
						] ); ?>
					</div>

					<div class="relative md:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32 xl:col-span-4 h-full flex items-center">
						<div class="py-8 2xl:py-16 4xl:py-24 full:py-32 px-8 2xl:px-16 4xl:px-24 full:px-32 text-white">
							<?php echo cmau_views()->render( 'item-lead', [
								'text' => $space['heading'],
								'class' => 'mb-8 2xl:mb-12 4xl:mb-24',
							] ); ?>

							<?php if ( ! empty( $space['info'] ) ) : ?>
								<div class="mb-8 2xl:mb-12 4xl:mb-24 grid sm:grid-cols-2 gap-4 4xl:gap-8">
									<?php foreach ( $space['info'] as $info ) : ?>
										<div class="flex items-center gap-4">
											<div class="w-16 h-16 4xl:w-24 4xl:h-24 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white">
												<i class="text-2xl 4xl:text-3xl <?php echo $info['icon']; ?>"></i>
											</div>

											<div class="my-4 2xl:text-lg 4xl:text-xl">
												<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $info['lines'] ) ) as $line ) : ?>
													<p class="my-1 2xl:my-2"><?php echo $line; ?></p>
												<?php endforeach; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>

							<?php echo cmau_views()->render( 'partial/open-item-excerpt' ); ?>
								<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $space['content'] ) ) as $line ) : ?>
									<p class="mb-12 4xl:mb-16 5xl:mb-24"><?php echo $line; ?></p>
								<?php endforeach; ?>
							<?php echo cmau_views()->render( 'partial/close' ); ?>

							<?php if ( isset( $space['button']['url'], $space['button']['text'] ) ) : ?>
								<div class="mt-16">
									<?php echo cmau_views()->render( 'global-button', $space['button'] + [
											'small' => true,
											'class' => 'hover:text-teak hover:bg-white',
											'attributes' => 'id="venue-spaces_slide-' . $index . '"'
										] ); ?>
								</div>
							<?php endif; ?>
						</div>

						<div class="absolute inset-0 bg-mineshaft -z-1"></div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

	<?php echo cmau_views()->render( 'slider-navigation', ['control' => 'venue-spaces_slider'] ); ?>
</div>
