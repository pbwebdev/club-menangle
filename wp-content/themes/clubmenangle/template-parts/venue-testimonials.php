<?php

/**
 * The template for displaying testimonials section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$testimonials = get_post_meta( get_the_ID(), 'cmau_venue_testimonials', false );

if ( empty( $testimonials ) ) {
	return;
}

?>

<div id="testimonials" class="py-8 2xl:py-16 4xl:py-24 full:py-32">
	<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
		<?php echo cmau_views()->render( 'front-heading', [
			'title' => 'Testimonials',
			'small' => true,
			'class' => 'mb-8 2xl:mb-12 4xl:mb-24'
		] ); ?>
	</div>

	<div class="relative w-full pb-8 2xl:pb-16 4xl:pb-24 full:pb-32">
		<div id="testimonial_slider" class="slider" data-arrows="true" data-dots="true">
			<?php foreach ( $testimonials as $testimonial ) : ?>
				<div class="relative py-8 2xl:py-16 4xl:py-24 full:py-32 xl:px-8 2xl:px-16 4xl:px-24 full:px-32">
					<div class="py-8 2xl:py-16 4xl:py-24 full:py-32 md:px-8 2xl:px-16 4xl:px-24 full:px-32 text-white text-center z-1">
						<?php echo cmau_views()->render( 'item-lead', [
							'text' => $testimonial['heading'],
							'class' => 'mb-8 2xl:mb-12 4xl:mb-24',
						] ); ?>

						<?php echo cmau_views()->render( 'partial/open-item-excerpt', ['class' => 'px-8 2xl:px-16 4xl:px-24 full:px-32'] ); ?>
						<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $testimonial['content'] ) ) as $line ) : ?>
							<p class="mb-12 4xl:mb-16 5xl:mb-24"><?php echo $line; ?></p>
						<?php endforeach; ?>
						<?php echo cmau_views()->render( 'partial/close' ); ?>

						<p class="text-lg italic"><?php echo $testimonial['person']; ?></p>
						<div class="mb-8 pb-8 2xl:mb-16"></div>
					</div>

					<div class="absolute inset-0 -z-1">
						<?php echo wp_get_attachment_image( $testimonial['image'], 'tile-1_1', false, array( 'class' => 'aspect-16/10 w-full h-full object-cover object-center brightness-50' ) ); ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#testimonial_slider', 'class' => '-mt-16 2xl:-mt-24 4xl:-mt-32 full:-mt-40 mb-8 2xl:mb-16 4xl:mb-24 full:mb-32'] ); ?>

		<div class="absolute w-1/2 h-full bg-pearlbush -z-10 bottom-0 right-0"></div>
	</div>
</div>
