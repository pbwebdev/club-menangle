<?php

/**
 * The template for displaying video section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args ) || ( empty( $args['url'] ) && empty( $args['placeholder'] ) ) ) {
	return;
}

?>

<div class="xl:px-8 2xl:px-16 4xl:px-24 full:px-32">
	<div class="lg:px-8 2xl:px-16 4xl:px-24 full:px-32">
		<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
			<div class="px-8 2xl:px-16 4xl:px-24 full:px-32">
				<?php if ( empty( $args['url'] ) ) : ?>
					<div class="relative w-full">
						<?php echo wp_get_attachment_image( $args['placeholder'], 'tile-1_2', false, [
							'class' => 'aspect-16/10 object-cover object-center w-full brightness-75 group-hover:brightness-50'
							] ); ?>

						<div class="absolute w-full h-full bg-pearlbush -z-10 -start-8 2xl:-start-16 4xl:-start-24 full:-start-32 -bottom-8 2xl:-bottom-16 4xl:-bottom-24 full:-bottom-32"></div>
						<div class="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
							<a href="#"><i class="fas fa-circle-play text-teak text-7xl xl:text-8xl 3xl:text-9xl"></i></a>
						</div>
					</div>
				<?php else : ?>
					<?php echo cmau_views()->render( 'responsive-embed', [
						'url' => $args['url'],
						'class' => 'aspect-16/10'
					] ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
