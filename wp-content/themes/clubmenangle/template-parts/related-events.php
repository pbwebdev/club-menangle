<?php

/**
* The template for displaying news section
*
* @package Club Menangle
* @since 0.1.0
*/

$event = get_post_type_object( 'event' );
$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'event',
		'posts_per_page' => 2,
		'post__not_in'   => array( get_the_ID() ),
		'meta_query'     => array(
			'date_clause' => array(
				'key' => 'cmau_event_date',
				'type' => 'DATE',
				'compare' => 'EXISTS',
			),
		),
		'orderby'        => array(
			'date_clause' => 'ASC',
			'meta_value' => 'DESC',
			'menu_order' => 'ASC',
			'post_date'  => 'DESC',
		),
		'tax_query'      => array( array(
			'taxonomy' => 'event-category',
			'field'    => 'term_id',
			'terms'    => wp_get_object_terms( get_the_ID(), 'event-category', array( 'fields' => 'ids' ) ),
		) ),
	)
);

if ( ! $query->have_posts() ) {
	return;
}

?>

<section class="py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<?php echo cmau_views()->render( 'front-heading', [ 'title' => 'Related Events' ] ); ?>

	<div class="py-8 xl:py-16 grid md:grid-cols-2 gap-8 4xl:gap-16">
		<?php while ( $query->have_posts() ) : ?>
			<?php $query->the_post(); ?>

			<div class="w-full">
				<?php get_template_part( 'template-parts/event', 'item' ); ?>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="text-center">
		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_post_type_archive_link( 'event' ),
			'text' => 'See ' . $event->labels->all_items,
		] ); ?>
	</div>
</section>
