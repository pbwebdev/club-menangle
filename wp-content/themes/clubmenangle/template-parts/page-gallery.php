<?php

/**
 * The template for displaying gallery section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args ) ) {
	return;
}

$attributes = array_merge( array(
	'reverse' => true,
	'anchor'  => '',
	'heading' => '',
	'content' => '',
	'link'    => array(),
	'images'  => array(),
), $args );

$attributes['link'] = array_merge( array(
	'url'  => '',
	'text' => '',
), $attributes['link'] );

$attributes['setting'] = array(
	'reverse' => $attributes['reverse'],
	'images'  => array_map(
		function( $image ) {
			return array(
				'id' => $image,
			);
		},
		$attributes['images']
	)
);

$content = apply_filters( 'the_content', $attributes['content'] );

unset( $attributes['reverse'] );
unset( $attributes['content'] );
unset( $attributes['images'] );
echo cmau_views()->render( 'block::gallery-section/markup', compact( 'attributes', 'content' ) );
