<?php

/**
* The template for displaying news section
*
* @package Club Menangle
* @since 0.1.0
*/

$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'post',
		'posts_per_page' => 10,
		'orderby'        => array(
			'menu_order' => 'ASC',
			'post_date'  => 'DESC',
		),
	)
);

if ( ! $query->have_posts() ) {
	return;
}

$news = get_post_meta( get_the_ID(), 'cmau_featured_news', true );

if ( empty( $news ) ) {
	$news = array(
		'title' => 'Featured News',
		'button' => 'See all news',
	);
}

$slider = $query->found_posts > 3;

?>

<section class="my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<?php echo cmau_views()->render( 'front-heading', [ 'title' => $news['title'] ] ); ?>

	<?php if ( $slider ) : ?>
		<div class="relative">
	<?php endif; ?>

	<div class="py-8 xl:py-16 <?php echo $slider ? 'slider" id="featured_slider" data-show="3" data-arrows="true' : 'grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16 items-center' ?>">
		<?php while ( $query->have_posts() ) : ?>
			<?php $query->the_post(); ?>

			<div class="w-full px-4 4xl:px-8">
				<?php get_template_part( 'template-parts/post', 'item' ); ?>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<?php if ( $slider ) : ?>
			<?php echo cmau_views()->render( 'slider-arrows', [ 'control' => '#featured_slider' ] ); ?>
		</div>
	<?php endif; ?>

	<div class="py-16 max-2xl:pt-8 text-center">
		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_post_type_archive_link( 'post' ),
			'text' => $news['button'],
		] ); ?>
	</div>
</section>
