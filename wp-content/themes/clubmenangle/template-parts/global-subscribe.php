<?php

/**
 * The template for displaying the subscribe section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

if ( ! $cmau_options['subscribe_heading'] && ! $cmau_options['subscribe_content'] && ! $cmau_options['subscribe_page'] ) {
	return;
}

?>

<section class="my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32 bg-teak">
	<div class="lg:flex items-center justify-between text-center -mx-4 py-6 lg:py-0">
		<?php if ( $cmau_options['subscribe_heading'] ) : ?>
			<h2 class="shrink-0 px-4 my-4 2xl:my-8 text-3xl 4xl:text-4xl 5xl:text-5xl full:text-6xl text-white uppercase"><?php echo $cmau_options['subscribe_heading']; ?></h2>
		<?php endif; ?>

		<?php if ( $cmau_options['subscribe_content'] ) : ?>
			<p class="text-lg 4xl:text-xl 5xl:text-2xl full:text-3xl text-center text-white px-4 my-4 2xl:my-8"><?php echo $cmau_options['subscribe_content']; ?></p>
		<?php endif; ?>

		<div class="shrink-0 px-4 my-8 2xl:my-16">
			<?php echo cmau_views()->render( 'global-button', $cmau_options['subscribe_link'] + [
				'accent' => false,
				'class'  => 'text-teak',
				'attributes' => '#' === $cmau_options['subscribe_link']['url'] ? 'x-on:click.prevent="subscribeSection = true"' : '',
			] ); ?>
		</div>
	</div>
</section>

<?php if ( '#' === $cmau_options['subscribe_link']['url'] ) : ?>
	<div
		id="subscribe-section"
		class="fixed top-0 left-1/2 -translate-x-1/2 z-10 max-w-[1440px] mx-auto"
		x-cloak
		x-show="subscribeSection !== false"
		x-transition:enter="transition ease-out duration-150"
		x-transition:enter-start="transform opacity-0 scale-75"
		x-transition:enter-end="transform opacity-100 scale-100"
		x-transition:leave="transition ease-in duration-75"
		x-transition:leave-start="transform opacity-100 scale-100"
		x-transition:leave-end="transform opacity-0 scale-75"
	>
		<div
			class="relative bg-white shadow-2xl overflow-y-auto max-h-full"
			x-on:click.outside="subscribeSection = false"
		>
			<button
				class="absolute right-0 2xl:right-4 top-0 2xl:top-4 text-teak hover:text-mineshaft p-4"
				aria-expanded="true"
				aria-controls="subscribe-section"
				aria-label="Close subscribe section"
				x-on:click="subscribeSection = false"
			>
				<i class="fas fa-close fa-2xl"></i>
			</button>

			<div class="max-xs:px-4 px-8 2xl:px-16 4xl:px-24 full:px-32 max-xs:py-4 py-8 2xl:py-16 4xl:py-24 full:py-32">
				<!-- BEGIN TalkBox form embed code https://talkbox.impactapp.com.au -->
				<!-- you can set data-width/data-height yourself for more control over iframe size (ie. data-height="1200") -->
				<div class="sbx-embedded-form" data-sbx-id="lLhbLV5NkG1g1m29a5V-lg==" data-width="auto" data-height="auto"></div>
				<script type="text/javascript" src="https://d3kivyesuae41d.cloudfront.net/forms/lLhbLV5NkG1g1m29a5V-lg==/form.js" async="async" defer="defer"></script>
				<!-- END TalkBox form embed code https://talkbox.impactapp.com.au -->
			</div>
		</div>
	</div>
<?php endif; ?>
