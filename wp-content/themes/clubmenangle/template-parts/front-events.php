<?php

/**
* The template for displaying events section
*
* @package Club Menangle
* @since 0.1.0
*/

$count = 0;
$events = get_post_meta( get_the_ID(), 'cmau_featured_events', true );

if ( empty( $events ) ) {
	$events = array(
		'title' => 'What\'s On',
		'button' => 'See all events',
	);
}

if ( empty( $events['items'] ) ) {
	$query = new WP_Query(
		array(
			'post_status'    => 'publish',
			'post_type'      => 'event',
			'fields'         => 'ids',
			'posts_per_page' => 3,
			'meta_query'     => array(
				'date_clause' => array(
					'key' => 'cmau_event_date',
					'type' => 'DATE',
					'compare' => 'EXISTS',
				),
			),
			'orderby'        => array(
				'date_clause' => 'ASC',
				'meta_value' => 'DESC',
				'menu_order' => 'ASC',
				'post_date'  => 'DESC',
			),
		)
	);

	if ( ! $query->have_posts() ) {
		return;
	}

	$events['items'] = $query->posts;
}

?>

<section class="my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<?php echo cmau_views()->render( 'front-heading', [ 'title' => $events['title'] ] ); ?>

	<div class="grid md:grid-rows-2 md:grid-cols-2 gap-8 md:gap-16 items-center py-8 2xl:py-16 4xl:py-24 full:py-32">
		<?php foreach ( $events['items'] as $e_id ) : ?>
			<?php
			$count++;

			$date  = get_post_meta( $e_id, 'cmau_event_date', true );
			$venue = get_post_meta( $e_id, 'cmau_event_venue', true );

			if ( ! $date ) {
				$date = get_the_date( '', $e_id );
			}
			?>

			<div class="w-full xl:ps-8 2xl:ps-16 4xl:ps-24 full:ps-32<?php echo 2 === $count ? ' row-span-2' : '' ?>">
				<?php echo get_the_post_thumbnail( $e_id, 'tile-1_2', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

				<div class="relative py-4 md:py-8 xl:px-8 xl:py-12 full:px-12 full:py-16">
					<div class="flex items-center">
						<?php echo cmau_views()->render( 'item-lead', [
							'small' => true,
							'text'  => date( 'j M', strtotime( $date ) ),
							'class' => 'w-24 h-24 4xl:w-32 4xl:h-32 5xl:w-36 5xl:h-36 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white uppercase',
						] ); ?>

						<div class="flex-1 ps-4 md:ps-8 full:ps-16">
							<?php echo cmau_views()->render( 'item-title', [
								'title' => get_the_title( $e_id ),
								'link'  => get_the_permalink( $e_id ),
								'class' => 'mb-6',
							] ); ?>
							<?php echo cmau_views()->render( 'item-lead', ['text' => $venue] ); ?>
						</div>
					</div>

					<?php if ( 2 === $count ) : ?>
						<div class="absolute bg-pearlbush -z-10 -start-8 -end-8 xl:-start-12 xl:-end-12 2xl:-start-16 2xl:-end-16 4xl:-start-24 4xl:-end-24 full:-end-32 -top-32 2xl:-top-64 bottom-0"></div>

						<?php echo cmau_views()->render( 'item-excerpt', [
							'raw'   => true,
							'text'  => get_the_excerpt( $e_id ),
							'class' => 'mt-12 4xl:mt-24',
						] ); ?>
					<?php endif; ?>
				</div>

				<?php if ( 2 === $count ) : ?>
					<div class="hidden xl:block my-16 text-center">
						<?php echo cmau_views()->render( 'global-button', [
							'url'  => get_post_type_archive_link( 'event' ),
							'text' => $events['button'],
						] ); ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="xl:hidden py-16 md:pt-0 text-center">
		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_post_type_archive_link( 'event' ),
			'text' => $events['button'],
		] ); ?>
	</div>
</section>
