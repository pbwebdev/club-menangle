<?php

/**
 * The template for displaying the about section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$pid = get_post_meta( get_the_ID(), 'cmau_featured_about', true );

if ( ! $pid ) {
	return;
}

$about = get_post( $pid );

if ( ! $about ) {
	return;
}

$super   = get_post_meta( $pid, 'cmau_banner_super', true );
$heading = get_post_meta( $pid, 'cmau_banner_heading', true );
$content = get_post_meta( $pid, 'cmau_banner_content', true );

if ( ! $heading ) {
	$heading = get_the_title( $pid );
}

if ( ! $content ) {
	$content = $about->post_excerpt;
}

?>

<section class="my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<?php echo cmau_views()->render( 'front-heading', [
		'title' => implode( ' ', [ $super ?? '', $heading ] ),
		'small' => true,
	] ); ?>

	<div class="py-8 2xl:py-16 sm:flex flex-wrap items-center justify-between -mx-4">
		<?php echo cmau_views()->render( 'item-excerpt', [
			'text'    => $content,
			'class'   => 'flex-1 px-4 my-8 md:my-0',
			'relaxed' => false,
		] ); ?>

		<div class="shrink-0 px-4 py-8 sm:py-0">
			<?php echo cmau_views()->render( 'round-button', [
				'text' => 'Read more',
				'url' => get_the_permalink( $pid ),
				'accent' => false,
			] ); ?>
		</div>
	</div>
</section>
