<?php

/**
 * The template for displaying extra sections
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args['sections'] ) ) {
	return;
}

foreach ( $args['sections'] as $index => $attributes ) {
	$attributes = array_merge( array(
		'anchor'  => 'section-' . ( $index + 1 ),
		'reverse' => ( 1 !== ( $index + 1 ) % 2 ),
		'image'   => '',
		'heading' => '',
		'content' => '',
		'link'    => array(),
	), $attributes );

	$attributes['link'] = array_merge( array(
		'url'  => '',
		'text' => '',
	), $attributes['link'] );

	$attributes['image'] = array(
		'id' => $attributes['image'],
	);

	$content = apply_filters( 'the_content', $attributes['content'] );

	unset( $attributes['content'] );
	echo cmau_views()->render( 'block::zigzag-item/markup', compact( 'attributes', 'content' ) );
}
