<?php

/**
 * The template for displaying the contact details
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args['id'] ) ) {
	return;
}

$phone   = get_post_meta( $args['id'], 'cmau_contact_phone', true );
$time    = get_post_meta( $args['id'], 'cmau_contact_time', true );
$address = get_post_meta( $args['id'], 'cmau_contact_address', true );
$uber    = get_post_meta( $args['id'], 'cmau_contact_uber', true );
$postal  = get_post_meta( $args['id'], 'cmau_contact_postal', true );
$email   = get_post_meta( $args['id'], 'cmau_contact_email', true );
$lat     = get_post_meta( $args['id'], 'cmau_map_lat', true );
$lng     = get_post_meta( $args['id'], 'cmau_map_lng', true );
$zml     = get_post_meta( $args['id'], 'cmau_map_zml', true );

$uber = array_merge( array(
	'url' => '',
	'text' => '',
), (array) $uber );

if ( empty( $uber['url'] ) && $uber['deeplink'] ) {
	$uber['url'] = $uber['deeplink'];
}

if ( empty( $uber['text'] ) && $uber['linktext'] ) {
	$uber['text'] = $uber['linktext'];
}

$attributes = compact( 'phone', 'time', 'address', 'uber', 'postal', 'email', 'lat', 'lng', 'zml' );

$attributes['heading'] = get_the_title();

echo cmau_views()->render( 'block::contact-details/markup', compact( 'attributes' ) );
