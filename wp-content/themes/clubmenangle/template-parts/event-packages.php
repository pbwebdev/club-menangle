<?php

/**
 * The template for displaying packages section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$packages = get_post_meta( get_the_ID(), 'cmau_event_packages', true );

$packages = array_merge( array(
	'heading' => '',
	'items'   => array(),
), (array) $packages );

if ( ! $packages['heading'] && empty( $packages['items'] ) ) {
	return;
}

?>

<div id="packages" class="px-8 2xl:px-16 4xl:px-24 full:px-32 pt-8 2xl:pt-16 4xl:pt-24 full:pt-32">
	<?php echo cmau_views()->render( 'front-heading', [
		'title' => $packages['heading'],
		'small' => true,
	] ); ?>

	<div class="py-8 xl:py-16 grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16">
		<?php foreach ( $packages['items'] as $package ) : ?>
			<div class="relative w-full">
				<?php echo get_the_post_thumbnail( $package, 'tile-1_3', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

				<?php echo cmau_views()->render( 'item-title', [
					'title' => get_the_title( $package ),
					'class' => 'py-8 4xl:py-12',
				] ); ?>

				<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl">
					<?php echo apply_filters( 'the_content', get_the_content( null, false, $package ) ); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
