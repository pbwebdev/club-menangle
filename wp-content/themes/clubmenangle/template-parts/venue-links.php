<?php

/**
 * The template for displaying the venue links
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$args['id'] ??= get_the_ID();

$links = get_post_meta( $args['id'], 'cmau_venue_links', false );

if ( empty( $links ) ) {
	return;
}

?>

<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 bg-teak text-white py-8 2xl:py-12">
	<ul class="md:flex flex-wrap items-center justify-center gap-8 4xl:gap-12 full:gap-16 text-xl 4xl:text-2xl 5xl:text-3xl full:text-4xl uppercase">
		<?php foreach ( $links as $link ) : ?>
			<li class="px-2">
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="inline-block py-2 border-b-3 border-transparent hover:border-white">
					<?php echo $link['text']; ?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
