<?php

/**
 * The template for displaying the enquiry section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$page_id = get_the_ID();
$contact = get_post_meta( $page_id, 'cmau_page_contact', true );

if ( empty( $contact ) ) {
	return;
}

$attributes = array_merge( array(
	'heading' => '',
	'content' => '',
), $contact );

$content = apply_filters( 'the_content', $attributes['content'] );

unset( $attributes['content'] );

if ( empty( $content ) ) {
	return;
}

?>

<div class="alignfull py-8 2xl:py-16 4xl:py-24 full:py-32 text-center">
	<div class="not-prose px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32">
		<?php if ( $attributes['heading'] ) : ?>
			<?php echo cmau_views()->render( 'front-heading', [
				'title' => $attributes['heading'],
				'small' => true,
				'class' => 'mx-auto',
			] ); ?>
		<?php endif; ?>
	</div>

	<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 xl:col-span-4">
		<div class="max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl">
			<?php echo $content; ?>
		</div>
	</div>
</div>
