<?php

/**
 * The template for displaying content section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$content = get_the_content();

if ( empty( $content ) ) {
	return;
}

$content = apply_filters( 'the_content', $content );
$content = str_replace( ']]>', ']]&gt;', $content );
$classes = 'is-layout-constrained px-8 2xl:px-0 max-w-none prose prose-lg 2xl:prose-xl 4xl:prose-2xl';

if ( is_single() ) {
	$classes .= ' prose-blockquote:text-teak prose-blockquote:border-l-teak prose-blockquote:not-italic prose-blockquote:leading-tight';
}

?>

<div id="editor-<?php the_ID(); ?>" class="<?php echo $classes; ?>">
	<?php if ( 'event' === get_post_type() ) : ?>
		<div class="alignwide mb-8 2xl:mb-12 4xl:mb-24">
			<?php echo cmau_views()->render( 'front-heading', [
				'title' => 'About the event',
				'small' => true,
			] ); ?>
		</div>
	<?php endif; ?>

	<?php echo $content; ?>

	<?php if ( 'event' === get_post_type() ) : ?>
		<?php $url = get_post_meta( get_the_ID(), 'cmau_event_book', true ); ?>

		<?php if ( $url ) : ?>
		<p class="alignwide not-prose 2xl:text-lg 4xl:text-xl">
			<?php echo cmau_views()->render( 'global-button', [
				'small'  => true,
				'url'    => $url,
				'text'   => 'Book Now',
				'class'  => 'mt-4',
				'target' => '_blank',
			] ); ?>
		</p>
		<?php endif; ?>
	<?php endif; ?>
</div>
