<?php

/**
 * The template for displaying the banner slides
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args['slides'] ) || empty( $args['p_id'] ) ) {
	return;
}

$classes = array(
	'heading' => [
		'my-4 xl:my-8 max-xs:text-4xl text-5xl 4xl:text-6xl 5xl:text-7xl full:text-8xl uppercase',
		is_post_type_archive( 'event' ) || is_singular( 'event' ) ? 'pb-4 3xl:pb-6 5xl:pb-8 border-b-3 4xl:border-b-6 border-teak' : '',
	],
	'content' => [
		'text-xl 4xl:text-2xl 5xl:text-3xl full:text-4xl',
		'post' === get_post_type( $args['p_id'] ) ? 'pb-4 3xl:pb-6 5xl:pb-8 border-b-4 border-teak' : '',
	]
);

?>

<div id="banner_slider" class="page-banner slider" data-arrows="true" data-dots="true">
	<?php foreach ( $args['slides'] as $slide ) : ?>
		<div class="px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32 h-banner relative">
			<div class="flex flex-wrap items-end md:px-8 2xl:px-16 4xl:px-24 full:px-32 w-full h-full">
				<div class="md:w-2/3 xl:w-1/2 text-white">
					<?php if ( $slide['heading'] ) : ?>
						<h1 class="<?php echo implode( ' ', $classes['heading'] ); ?>"><?php echo $slide['heading']; ?></h1>
					<?php endif; ?>

					<?php if ( $slide['content'] ) : ?>
						<p class="<?php echo implode( ' ', $classes['content'] ); ?>"><?php echo $slide['content']; ?></p>
					<?php endif; ?>

					<div class="my-16">
						<?php if ( isset( $slide['button']['url'], $slide['button']['text'] ) ) : ?>
							<?php echo cmau_views()->render( 'global-button', $slide['button'] + [
								'accent' => false,
								'class'  => 'text-teak',
							] ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<?php if ( $slide['image'] ) : ?>
				<div class="absolute inset-0 overflow-hidden -z-1">
					<?php echo wp_get_attachment_image( $slide['image'], 'full', false, array( 'class' => 'w-full h-full object-cover object-center brightness-50' ) ); ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
</div>

<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#banner_slider', 'class' => 'absolute left-1/2 -translate-x-1/2 -mt-16 2xl:-mt-24 4xl:-mt-32 full:-mt-40'] ); ?>
