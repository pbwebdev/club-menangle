<?php

/**
* The template for displaying super section
*
* @package Club Menangle
* @since 0.1.0
*/

$args['id'] ??= get_the_ID();

$supers = get_post_meta( $args['id'], 'cmau_featured_supers', false );

if ( empty( $supers ) ) {
	return;
}

$inner_content = '';

foreach ( $supers as $index => $attributes ) {
	$attributes = array_merge( array(
		'reverse' => ( 1 !== ( $index + 1 ) % 2 ),
		'heading' => '',
		'content' => '',
		'link'    => '',
		'image'   => '',
	), $attributes );

	$attributes['image'] = array(
		'id' => $attributes['image'],
	);
	$attributes['url'] = $attributes['link'];

	if ( is_array( $attributes['url'] ) ) {
		$attributes['url'] = $attributes['url']['url'] ?? '#'; // temp
	}

	$content = apply_filters( 'the_content', $attributes['content'] );

	unset( $attributes['link'] );
	unset( $attributes['content'] );
	ob_start();
	echo cmau_views()->render( 'block::super-item/markup', compact( 'attributes', 'content' ) );

	$inner_content .= ob_get_clean();
}

$content = $inner_content;

echo cmau_views()->render( 'block::super-section/markup', compact( 'content' ) );
