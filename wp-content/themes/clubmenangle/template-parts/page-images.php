<?php

/**
 * The template for displaying video section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( empty( $args ) || empty( $args['images'] ) ) {
	return;
}

?>

<div id="venue_slider" class="slider py-32 md:px-8 2xl:px-16 4xl:px-24 full:px-32" data-dots="true" data-arrows="true">
	<?php foreach ( $args['images'] as $image ) : ?>
		<div class="relative px-8 2xl:px-16 4xl:px-24 full:px-32 pb-8 xl:pb-12 2xl:pb-16 4xl:pb-24">
			<?php echo wp_get_attachment_image( $image, 'tile-1_1', false, [
				'class' => 'aspect-2/1 object-cover object-center w-full brightness-75 group-hover:brightness-50'
			] ); ?>

			<div class="absolute bg-pearlbush -z-10 -start-8 -end-8 xl:-start-12 xl:-end-12 2xl:-start-16 2xl:-end-16 4xl:-start-24 4xl:-end-24 top-32 2xl:top-64 bottom-0"></div>
		</div>
	<?php endforeach; ?>
</div>
<?php echo cmau_views()->render( 'slider-navigation', ['control' => '#venue_slider', 'class' => '-mt-24 mb-8 2xl:mb-16'] ); ?>
