<?php

/**
* The template for displaying events section
*
* @package Club Menangle
* @since 0.1.0
*/

$args['type'] ??= 'event';
$args['p_id'] ??= get_the_ID();

$date     = get_post_meta( $args['p_id'], 'cmau_' . $args['type'] . '_date', true );
$location = get_post_meta( $args['p_id'], 'cmau_' . $args['type'] . '_location', true ) ?: '&mdash; &mdash; &mdash;';
$price    = get_post_meta( $args['p_id'], 'cmau_' . $args['type'] . '_price', true ) ?: '&mdash; &mdash; &mdash;';
$book     = get_post_meta( $args['p_id'], 'cmau_' . $args['type'] . '_book', true ) ?: '#';

if ( ! $date ) {
	$date = get_the_date( '', $args['p_id'] );
}

?>

<div class="w-full">
	<?php echo get_the_post_thumbnail( $args['p_id'], 'tile-1_3', array( 'class' => 'aspect-16/10 object-cover object-center w-full brightness-75' ) ); ?>

	<?php echo cmau_views()->render( 'item-title', [
		'title' => get_the_title( $args['p_id'] ),
		'class' => 'my-6',
	] ); ?>

	<div class="grid gap-4">
		<div class="flex max-xs:flex-col gap-4 md:gap-8">
			<span class="text-3xl fa-fw fas fa-calendar"></span>
			<p class="2xl:text-lg 4xl:text-xl"><?php echo date( 'l jS F Y', strtotime( $date ) ); ?></p>
		</div>

		<div class="flex max-xs:flex-col gap-4 md:gap-8">
			<span class="text-3xl fa-fw fas fa-location-dot"></span>

			<p class="2xl:text-lg 4xl:text-xl"><?php echo implode( ' ', array_filter( preg_split('/\r\n|[\r\n]/', $location ) ) ); ?></p>
		</div>

		<div class="flex max-xs:flex-col gap-4 md:gap-8">
			<span class="text-3xl fa-fw fas fa-money-bill"></span>

			<pre class="2xl:text-lg 4xl:text-xl font-muli"><?php echo $price; ?></pre>
		</div>

		<div class="my-1 2xl:my-2"></div>

		<?php echo cmau_views()->render( 'global-button', [
			'url'  => $book,
			'text' => 'Get tickets',
			'small' => true,
			'accent' => false,
			'class' => 'border border-mineshaft',
		] ); ?>

		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_the_permalink( $args['p_id'] ),
			'text' => 'More Info',
			'small' => true,
			'accent' => false,
			'class' => 'border border-mineshaft',
		] ); ?>
	</div>
</div>
