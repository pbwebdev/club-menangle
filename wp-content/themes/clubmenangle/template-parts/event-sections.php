<?php

/**
 * The template for displaying event sections
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$args['id'] ??= get_the_ID();
$args['sections'] ??= get_post_meta( $args['id'], 'cmau_event_sections', false );

if ( empty( $args['sections'] ) ) {
	return;
}

foreach ( $args['sections'] as $index => $attributes ) {
	$attributes = array_merge( array(
		'reverse' => ( 1 !== ( $index + 1 ) % 2 ),
		'image'   => '',
		'heading' => '',
		'content' => '',
		'link'    => array(
			'url'  => get_post_meta( $args['id'], 'cmau_event_book', true ),
			'text' => 'Book Now',
		),
	), $attributes );

	$attributes['image'] = array(
		'id' => $attributes['image'],
	);

	$content = apply_filters( 'the_content', $attributes['content'] );

	unset( $attributes['content'] );
	echo cmau_views()->render( 'block::zigzag-alternative/markup', compact( 'attributes', 'content' ) );
}

