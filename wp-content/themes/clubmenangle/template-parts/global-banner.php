<?php

/**
 * The template for displaying the banner
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$images = $heading = $content = $button = $p_id = null;

if ( is_post_type_archive() ) {
	$object  = get_queried_object();
	$heading = $object->labels->archives;

	if ( 'event' === $object->name ) {
		$setting = get_option( 'cmau-event' );
		$images  = $setting['archive_images'];
	} else {
		$images = get_post_thumbnail_id( get_the_ID() );
	}
} else {
	$p_id = get_the_ID();

	if ( is_home() || is_404() ) {
		$p_id = get_option( 'page_for_posts' );
	}

	$slides = get_post_meta( $p_id, 'cmau_banner_slides', false );
	$slides = array_filter( $slides );

	if ( ! empty( $slides ) ) {
		return get_template_part( 'template-parts/global', 'banner-slides', compact( 'slides', 'p_id' ) );
	}

	$heading = get_post_meta( $p_id, 'cmau_banner_heading', true );
	$content = get_post_meta( $p_id, 'cmau_banner_content', true );
	$button  = get_post_meta( $p_id, 'cmau_banner_button', true );
	$images  = get_post_meta( $p_id, 'cmau_banner_images', true );

	if ( ! $heading ) {
		$heading = single_post_title( '', false );
	}

	if ( ! $images && has_post_thumbnail( $p_id ) ) {
		$images = get_post_thumbnail_id( $p_id );
	}

	if ( 'post' === get_post_type( $p_id ) ) {
		$content = 'Posted on ' . get_the_date( 'j M Y');
	} elseif ( 'event' === get_post_type( $p_id ) ) {
		$date = get_post_meta( $p_id, 'cmau_event_date', true );
		$time = get_post_meta( $p_id, 'cmau_event_time', true );

		if ( $date ) {
			$date = date( 'jS M Y', strtotime( $date ) ) . ' ' . $time;
		}
	}
}

$images = array_filter( (array) $images );
$button = array_merge( array(
	'url'  => '',
	'text' => '',
), (array) $button );

$classes = array(
	'heading' => [
		'my-4 xl:my-8 max-xs:text-4xl text-5xl 4xl:text-6xl 5xl:text-7xl full:text-8xl uppercase',
		is_post_type_archive( 'event' ) || is_singular( 'event' ) ? 'pb-4 3xl:pb-6 5xl:pb-8 border-b-3 4xl:border-b-6 border-teak' : '',
	],
	'content' => [
		'text-xl 4xl:text-2xl 5xl:text-3xl full:text-4xl',
		'post' === get_post_type( $p_id ) ? 'pb-4 3xl:pb-6 5xl:pb-8 border-b-4 border-teak' : '',
	]
);

?>

<div class="page-banner px-8 2xl:px-16 4xl:px-24 full:px-32 py-8 2xl:py-16 4xl:py-24 full:py-32 flex flex-wrap items-end justify-center relative<?php echo count( $images ) ? ' h-banner' : ''; ?>">
	<div class="md:px-8 2xl:px-16 4xl:px-24 full:px-32 w-full z-1">
		<div class="md:w-2/3 xl:w-1/2 text-white">
			<?php if ( $heading ) : ?>
				<h1 class="<?php echo implode( ' ', $classes['heading'] ); ?>"><?php echo $heading; ?></h1>
			<?php endif; ?>

			<?php if ( $content ) : ?>
				<p class="<?php echo implode( ' ', $classes['content'] ); ?>"><?php echo $content; ?></p>
			<?php endif; ?>

			<?php if ( isset( $button['url'], $button['text'] ) ) : ?>
				<div class="my-16">
					<?php echo cmau_views()->render( 'global-button', $button + [
						'accent' => false,
						'class'  => 'text-teak',
					] ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="absolute inset-0 overflow-hidden<?php echo count( $images ) > 1 ? ' slider" data-dots="flex-col top-1/2 -translate-y-1/2 !left-full -ml-8 md:-ml-16 xl:-ml-32' : ' bg-gray-500' ?>">
		<?php foreach ( $images as $image ) : ?>
			<?php echo wp_get_attachment_image( $image, 'full', false, array( 'class' => 'w-full h-full object-cover object-center brightness-50' ) ); ?>
		<?php endforeach; ?>
	</div>
</div>
