<?php

/**
* The template for displaying spaces section
*
* @package Club Menangle
* @since 0.1.0
*/

$spaces = get_post_meta( get_the_ID(), 'cmau_featured_spaces', true );

if ( empty( $spaces ) ) {
	return;
}

?>

<section class="my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32 grid gap-8 md:grid-cols-2" x-data="{ activeTab: 0 }">
	<div class="px-8 xl:px-16 3xl:px-24 5xl:px-32 relative flex items-center">
		<ul class="w-full text-xl 2xl:text-2xl 4xl:text-3xl full:text-4xl uppercase text-white">
			<?php foreach ( $spaces['items'] as $index => $item ) : ?>
				<li class="my-4 xl:my-10 3xl:my-16 5xl:my-32">
					<a
						href="#"
						class="flex items-center justify-between group gap-8"
						x-on:click.prevent="activeTab = <?php echo $index; ?>"
						>
						<span
							class="py-4 xl:py-6 3xl:py-8 border-b-3 4xl:border-b-6 group-hover:border-teak"
							:class="activeTab === <?php echo $index; ?> ? 'border-teak' : 'border-transparent'"
						>
							<?php echo $item['title']; ?>
						</span>
						<img src="<?php echo CLUB_MENANGLE_THEME_URL; ?>assets/images/arrow.svg" class="h-8 xl:h-12 3xl:h-16 5xl:h-20">
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<?php echo wp_get_attachment_image( $spaces['image'], 'tile-3_4', false, array( 'class' => 'w-full h-full object-cover object-center brightness-75 absolute inset-0 -z-1' ) ); ?>
	</div>

	<div class="md:px-4 xl:px-8 2xl:px-16 relative flex items-center">
		<?php foreach ( $spaces['items'] as $index => $item ) : ?>
			<div class="my-8 2xl:my-16 4xl:my-24 full:my-32 relative" x-cloak x-show="activeTab === <?php echo $index; ?>">
				<h2 class="mb-12 4xl:mb-24 text-xl 2xl:text-2xl 4xl:text-3xl full:text-4xl pb-4 3xl:pb-6 5xl:pb-8 border-b-3 4xl:border-b-6 border-teak"><?php echo $item['heading']; ?></h2>

				<?php echo cmau_views()->render( 'partial/open-item-excerpt' ); ?>
					<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $item['content'] ) ) as $line ) : ?>
						<p class="mb-12 4xl:mb-16 5xl:mb-24"><?php echo $line; ?></p>
					<?php endforeach; ?>
				<?php echo cmau_views()->render( 'partial/close' ); ?>
			</div>
		<?php endforeach; ?>

		<div class="absolute left-0 right-0 -bottom-12 md:-bottom-16 xl:-bottom-24 3xl:-bottom-24 5xl:-bottom-32 text-center">
			<?php echo cmau_views()->render( 'global-button', $spaces['button'] ); ?>
		</div>

		<div class="absolute bg-pearlbush -z-10
			-start-8 -end-8 -top-16 -bottom-4
			md:-start-20 md:-top-8 md:-bottom-8
			xl:-start-24 xl:-end-12 xl:-top-12 xl:-bottom-12
			2xl:-end-16 4xl:-end-24 full:-end-32 4xl:-start-24 full:-start-32 4xl:-top-16 4xl:-bottom-16"></div>
	</div>
</section>

<div class="py-8 2xl:py-16 4xl:py-24 full:py-32"></div>
