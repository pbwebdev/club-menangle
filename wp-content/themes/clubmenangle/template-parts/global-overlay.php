<?php

/**
 * The template for displaying the overlay section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

if ( empty( $cmau_options['global_events'] ) ) {
	$query = new WP_Query(
		array(
			'post_status'    => 'publish',
			'post_type'      => 'event',
			'fields'         => 'ids',
			'posts_per_page' => 3,
			'meta_query'     => array(
				'date_clause' => array(
					'key' => 'cmau_event_date',
					'type' => 'DATE',
					'compare' => 'EXISTS',
				),
			),
			'orderby'        => array(
				'date_clause' => 'ASC',
				'meta_value' => 'DESC',
				'menu_order' => 'ASC',
				'post_date'  => 'DESC',
			),
		)
	);

	$cmau_options['global_events'] = $query->posts;
}

?>

<div id="overlay-section" class="fixed inset-0 z-10 max-xs:px-4 max-xs:py-4 px-8 py-8 max-w-[1440px] mx-auto"
	 x-cloak
	 x-show="overlaySection !== false"
	 x-transition:enter="transition origin-top ease-out duration-300"
	 x-transition:enter-start="opacity-0 scale-y-90"
	 x-transition:enter-end="opacity-100 scale-y-100"
	 x-transition:leave="transition origin-top ease-in duration-150"
	 x-transition:leave-start="opacity-100 scale-y-100"
	 x-transition:leave-end="opacity-0 scale-y-90"
>
	<div class="relative bg-white shadow-2xl overflow-y-auto max-h-full" x-on:click.outside="overlaySection = false">
		<button
			class="absolute right-0 2xl:right-4 top-0 2xl:top-4 text-teak hover:text-mineshaft p-4"
			aria-expanded="true"
			aria-controls="overlay-section"
			aria-label="Close overlay section"
			x-on:click="overlaySection = false"
		>
			<i class="fas fa-close fa-2xl"></i>
		</button>

		<div class="grid md:grid-cols-7" x-show="overlaySection === 'book'">
			<div class="max-xs:px-4 px-8 2xl:px-16 4xl:px-24 full:px-32 max-xs:py-4 py-8 2xl:py-16 4xl:py-24 full:py-32 col-span-3">
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => 'Reservations',
					'small' => true,
					'class' => 'max-xs:text-2xl'
				] ); ?>

				<p class="mt-4 2xl:mt-8 4xl:mt-12">Choose a restaurant:</p>

				<ul class="text-xl 2xl:text-2xl 4xl:text-3xl !leading-loose">
					<?php foreach( $cmau_options['global_reservations'] as $reservation ) : ?>
					<li class="mt-4 2xl:mt-8 4xl:mt-12">
						<a
							href="<?php echo $reservation['url']; ?>"
							target="<?php echo $reservation['target']; ?>"
							class="pb-2 border-b-4 border-transparent hover:border-teak"><?php echo $reservation['text']; ?></a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>

			<div class="max-xs:px-4 px-8 2xl:px-16 4xl:px-24 full:px-32 max-xs:py-4 py-8 2xl:py-16 4xl:py-24 full:py-32 col-span-4 md:!ps-0">
				<?php echo cmau_views()->render( 'front-heading', [
					'title' => 'What\'s On',
					'small' => true,
					'class' => 'max-xs:text-2xl'
				] ); ?>

				<div class="mt-8 2xl:mt-12 4xl:mt-16 full:mt-24 grid gap-8 3xl:gap-12">
					<?php foreach ( $cmau_options['global_events'] as $p_id ) : ?>
						<?php
						$date  = get_post_meta( $p_id, 'cmau_event_date', true );
						$book  = get_post_meta( $p_id, 'cmau_event_book', true ) ?: '#';
						$venue = get_post_meta( $p_id, 'cmau_event_venue', true );
						?>

						<div class="flex flex-wrap max-md:flex-col gap-8 items-center">
							<?php echo cmau_views()->render( 'item-lead', [
								'small' => true,
								'text'  => date( 'j M', strtotime( $date ) ),
								'class' => 'w-24 h-24 4xl:w-32 4xl:h-32 5xl:w-36 5xl:h-36 bg-teak rounded-full flex justify-center items-center p-8 text-center text-white uppercase',
							] ); ?>

							<div class="flex-1">
								<?php echo cmau_views()->render( 'item-title', [
									'title' => get_the_title( $p_id ),
									'link'  => get_the_permalink( $p_id ),
									'class' => 'max-xs:text-xl mb-2',
								] ); ?>
								<?php echo cmau_views()->render( 'item-lead', ['text' => $venue] ); ?>
							</div>

							<?php echo cmau_views()->render( 'global-button', [
								'url'  => get_the_permalink( $p_id ),
								'text' => 'Book now',
								'small' => true,
								'accent' => false,
								'class' => 'border border-mineshaft',
							] ); ?>
						</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>

				<div class="mt-12 3xl:mt-18 5xl:mt-24 flex flex-wrap items-center justify-center gap-8">
					<?php echo cmau_views()->render( 'global-button', [
						'url'  => get_the_permalink( $cmau_options['global_tickets'] ),
						'text' => 'See all events',
						'small' => true,
					] ); ?>
				</div>
			</div>
		</div>

		<div class="py-8 px-8" x-show="overlaySection === 'menu'">
			<?php cmau_mobile_menu( array( 'flex flex-col items-center gap-8 text-center' ) ); ?>
		</div>
	</div>
</div>
