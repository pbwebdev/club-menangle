<?php

/**
* The template for displaying news section
*
* @package Club Menangle
* @since 0.1.0
*/

$news = get_post_type_object( 'post' );
$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'post',
		'posts_per_page' => 3,
		'post__not_in'   => array( get_the_ID() ),
		'category__in'   => wp_get_post_categories( get_the_ID(), array( 'fields' => 'ids' ) ),
		'orderby'        => 'rand',
	)
);

if ( ! $query->have_posts() ) {
	return;
}

?>

<section class="py-8 my-8 md:my-12 xl:my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<?php echo cmau_views()->render( 'front-heading', [ 'title' => 'Related News' ] ); ?>

	<div class="py-8 xl:py-16 grid md:grid-cols-2 xl:grid-cols-3 gap-8 4xl:gap-16">
		<?php while ( $query->have_posts() ) : ?>
			<?php $query->the_post(); ?>

			<div class="w-full">
				<?php get_template_part( 'template-parts/post', 'item' ); ?>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="text-center">
		<?php echo cmau_views()->render( 'global-button', [
			'url'  => get_post_type_archive_link( 'post' ),
			'text' => 'See ' . $news->labels->all_items,
		] ); ?>
	</div>
</section>
