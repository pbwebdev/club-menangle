<?php

/**
 * Club Menangle functions and definitions
 *
 * @package Club Menangle
 * @since 0.1.0
 */

/* generator-themeplate v1.17.0 */

// Accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * ==================================================
 * Global constants
 * ==================================================
 */
// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma
define( 'CLUB_MENANGLE_THEME_BASE',  basename( __DIR__ ) );
define( 'CLUB_MENANGLE_THEME_URL',   get_theme_root_uri( CLUB_MENANGLE_THEME_BASE ) . '/' . CLUB_MENANGLE_THEME_BASE . '/' );
define( 'CLUB_MENANGLE_THEME_PATH',  get_theme_root( CLUB_MENANGLE_THEME_BASE ) . '/' . CLUB_MENANGLE_THEME_BASE . '/' );
define( 'CLUB_MENANGLE_THEME_DEBUG', false );

define( 'CLUB_MENANGLE_THEME_REQUIRES', array(
	'PHP' => '7.4',
	'WP'  => '6.3',
) );
// phpcs:enable

// Check if PHP and WordPress installed versions met the requirements
if ( version_compare( PHP_VERSION, CLUB_MENANGLE_THEME_REQUIRES['PHP'], '<' ) || version_compare( $GLOBALS['wp_version'], CLUB_MENANGLE_THEME_REQUIRES['WP'], '<' ) ) {
	require_once CLUB_MENANGLE_THEME_PATH . 'includes/compatibility.php';
	return;
}

// Better move this folder (club-menangle) to the plugins directory, then remove these lines after
if ( file_exists( CLUB_MENANGLE_THEME_PATH . 'clubmenangle/clubmenangle.php' ) ) {
	function cmau_stock_notice() {
		wp_admin_notice(
			sprintf(
				'Better move <strong>%s</strong> to <strong>%s</strong>.',
				wp_normalize_path( CLUB_MENANGLE_PLUGIN_PATH ),
				wp_normalize_path( WP_PLUGIN_DIR )
			),
			array( 'type' => 'info' )
		);
	}

	add_action( 'admin_notices', 'cmau_stock_notice' );
	require_once 'clubmenangle/clubmenangle.php';
}


/*
 * ==================================================
 * Setup Theme
 * ==================================================
 */

require_once 'dependencies/vendor/autoload.php';
require_once 'setup/features.php';
require_once 'setup/navigations.php';
require_once 'setup/widgets.php';
require_once 'setup/blocks.php';
require_once 'setup/facetwp.php';
require_once 'setup/scripts-styles.php';
require_once 'setup/actions-filters.php';


/*
 * ==================================================
 * Extra custom functions
 * ==================================================
 */

$cmau_options = array();

if ( function_exists('club_menangle_options') ) {
	$cmau_options = club_menangle_options();
}

function cmau_views() {
	static $engine;

	if ( null === $engine ) {
		$engine = new League\Plates\Engine( CLUB_MENANGLE_THEME_PATH . 'components' );

		$engine->addFolder( 'block', CLUB_MENANGLE_THEME_PATH . 'blocks' );
	}

	return $engine;
}

function cmau_is_block_editor() {
	return defined( 'REST_REQUEST' ) && REST_REQUEST;
}

function cmau_is_true( $value ) {
	return in_array( $value, array( 'true', '1', 1, true ), true );
}

add_action( 'load-post.php', function () {
	if (
		empty( $_GET['post'] ) ||
		(
			! in_array(
				get_page_template_slug( $_GET['post'] ),
				array(
					'page-templates/all-events.php',
					'page-templates/restaurants.php',
					'page-templates/page-functions.php',
					'page-templates/sub-restaurants.php',
				),
				true
			) &&
			! in_array(
				$_GET['post'],
				array(
					get_option( 'page_on_front' ),
					get_option( 'page_for_posts' )
				),
				true
			)
		)
	) {
		return;
	}

	remove_post_type_support( 'page', 'editor' );

	if ( $_GET['post'] === get_option( 'page_on_front' ) ) {
		remove_meta_box( 'pageparentdiv', 'page', 'side' );
	} elseif ( $_GET['post'] === get_option( 'page_for_posts' ) ) {
		return;
	}

	add_action( 'edit_form_after_title', function() {
		wp_admin_notice(
			__( 'You are currently editing a page with a template that does not support editor content.' ),
			array(
				'type'               => 'warning',
				'additional_classes' => array( 'inline' ),
			)
		);
	} );
} );

add_shortcode( 'cm_download', function( $atts ) {
	$args = shortcode_atts( array (
		'id' => '',
		'url' => '',
		'newtab' => false,
		'text' => ''
	), $atts );

	if ( ! $args['url'] ) {
		if ( empty( $args['id'] ) ) {
			return;
		}

		$links = get_post_meta( get_the_ID(), 'cmau_download_links', false );

		if ( empty( $links ) ) {
			return;
		}

		$index = array_search(
			strtolower( $atts['id'] ),
			array_map( 'strtolower', array_column( $links, 'text' ) )
		);

		if ( false === $index ) {
			return;
		}

		if ( empty( $links[ $index ]['url'] ) ) {
			return;
		}

		$args['url'] = $links[ $index ]['url'];
		$args['text'] = $links[ $index ]['text'];
	}

	$svg = '<svg class="inline-block mr-2 -mt-1" xmlns="http://www.w3.org/2000/svg" width="21" height="19"><path fill="currentColor" d="M18.562 18.25c.47 0 .868-.164 1.196-.492.328-.328.492-.727.492-1.196v-4.5c0-.468-.164-.867-.492-1.195a1.627 1.627 0 00-1.196-.492h-3.234l1.617-1.617c.352-.352.522-.756.51-1.213a1.704 1.704 0 00-.492-1.178c-.317-.328-.72-.492-1.213-.492H13.5V1.938c0-.47-.164-.868-.492-1.196A1.627 1.627 0 0011.812.25H8.437c-.468 0-.867.164-1.195.492a1.627 1.627 0 00-.492 1.196v3.937H4.5a1.57 1.57 0 00-1.178.492c-.316.328-.48.72-.492 1.178a1.66 1.66 0 00.475 1.213l1.617 1.617H1.687c-.468 0-.867.164-1.195.492A1.627 1.627 0 000 12.063v4.5c0 .468.164.867.492 1.195.328.328.727.492 1.195.492h16.875zm-8.437-5.062L4.5 7.563h3.937V1.938h3.375v5.625h3.938l-5.625 5.625zm8.437 3.374H1.687v-4.5H6.61l2.32 2.32c.329.329.727.493 1.196.493s.867-.164 1.195-.492l2.32-2.32h4.922v4.5zm-2.25-1.406a.814.814 0 00.598-.246.814.814 0 00.246-.597.814.814 0 00-.246-.598.814.814 0 00-.598-.246.814.814 0 00-.597.246.814.814 0 00-.246.598c0 .234.082.433.246.597a.814.814 0 00.597.246z"/></svg>';

	return cmau_views()->render( 'global-button', [
		'small'  => true,
		'escape' => false,
		'url'    => $args['url'],
		'target' => '1' === $args['newtab'] ? '_blank' : '_self',
		'text'   => $svg . $args['text'],
		'class'  => 'not-prose mt-4',
	] );
} );

add_action( 'admin_footer', function() {
	echo '<input type="hidden" id="cm-rebuild" value="' . CLUB_MENANGLE_REBUILD . '" />';
}, PHP_INT_MAX );


function cmau_term_links( $links ) {
	return array_map(
		function( $link ) {
			return str_replace( 'rel="tag"', 'rel="tag" class="pb-2 border-b-3 border-teak"', $link );
		},
		$links
	);
}

add_filter( 'term_links-category', 'cmau_term_links' );
add_filter( 'term_links-event-category', 'cmau_term_links' );
