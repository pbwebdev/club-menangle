<?php

/**
 * The template containing the footer areas
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

?>

<section class="my-16 3xl:my-24 5xl:my-32 px-8 2xl:px-16 4xl:px-24 full:px-32">
	<div class="grid sm:grid-cols-2 xl:grid-cols-4 gap-10">
		<div class="max-xl:mb-6">
			<div class="hidden flex-wrap flex-col gap-8 4xl:gap-12"></div>
			<?php cmau_widget_menu_1( array( 'flex flex-wrap flex-col gap-8 4xl:gap-12', ) ); ?>
		</div>

		<div class="max-xl:mb-6">
			<div class="hidden flex-wrap flex-col gap-8 4xl:gap-12"></div>
			<?php cmau_widget_menu_2( array( 'flex flex-wrap flex-col gap-8 4xl:gap-12', ) ); ?>
		</div>

		<div class="flex flex-col justify-between gap-8 4xl:gap-12">
			<?php foreach ( $cmau_options['contact_items'] as $item ) : ?>
				<div class="grid gap-4">
					<h4 class="text-lg 2xl:text-xl 4xl:text-2xl uppercase"><?php echo $item['heading']; ?></h4>
					<?php foreach ( array_filter( preg_split('/\r\n|[\r\n]/', $item['content'] ) ) as $line ) : ?>
						<p class="py-2 2xl:text-lg 4xl:text-xl"><?php echo $line; ?></p>
					<?php endforeach; ?>

					<?php if ( ! empty( $item['social'] ) ) : ?>
						<ul class="flex flex-wrap gap-4 mt-4 text-4xl">
							<?php foreach ( $item['social'] as $social ) : ?>
								<li>
									<a
										href="<?php echo $social['link']; ?>"
										target="_blank"
										rel="noopener noreferrer"
										aria-label="<?php echo ucfirst( $social['provider'] ); ?>"
										class="text-teak hover:text-mineshaft"
									>
										<span class="fab fa-<?php echo $social['provider']; ?>"></span>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>

		<div class="max-xl:mb-6">
			<h4 class="mb-8 4xl:mb-12 text-xl 2xl:text-2xl 4xl:text-3xl uppercase"><?php echo $cmau_options['opening_heading']; ?></h4>

			<?php foreach ( $cmau_options['opening_venue'] as $venue ) : ?>
				<div class="mt-8 4xl:mt-12">
					<h4 class="mb-8 4xl:mb-12 text-lg 2xl:text-xl 4xl:text-2xl uppercase"><?php echo $venue['heading']; ?></h4>

					<table class="table-auto text-left">
						<?php foreach ( $venue['hours'] as $item ) : ?>
							<tr class="mt-4 4xl:mt-8 2xl:text-lg 4xl:text-xl">
								<th class="font-normal"><?php echo $item['day']; ?>:</th>
								<td class="px-2"><?php echo $item['time']; ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
