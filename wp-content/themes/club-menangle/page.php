<?php

/**
 * The template for displaying pages
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

$flipbook = get_post_meta( get_the_ID(), 'cmau_flipbook_flipbook_code', false )[0];

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<div class="section full-pad container">
				<?php the_post(); ?>
				<?php the_content(); ?>

				<?php
					if($flipbook) {
						echo do_shortcode($flipbook['code']['text']);
					}
				?>

			</div>
		<?php endwhile; ?>

	</main><!-- .content -->

	<div class="quarter-pad"></div>

<?php

get_footer();
