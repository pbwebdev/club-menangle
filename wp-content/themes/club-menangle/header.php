<?php

/**
 * The template for displaying the header
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

?>

<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<script type="text/javascript">document.documentElement.className = document.documentElement.className.replace( 'no-js', 'js' );</script>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<?php wp_body_open(); ?>

		<?php get_template_part( 'template-parts/global', 'loader' ); ?>

		<a class="screen-reader-text" href="#site-content"><?php esc_html_e( 'Skip to content', 'club-menangle' ); ?></a>

		<div class="site-wrapper">
			<header class="site-header full-offset nav-down">
				<div class="branding half-offset">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php
						echo wp_get_attachment_image( $cmau_options['global_logo'], 'full', false, array(
							'alt'         => get_bloginfo( 'name' ),
							'data-unlazy' => 'true',
						) );
						?>
					</a>
				</div><!-- .branding -->

				<div class="navbar offset-width">
					<div class="main">
						<a href="#" class="main-toggle" aria-expanded="false">
							<span class="bars">
								<i class="bar"></i>
								<i class="bar"></i>
								<i class="bar"></i>
							</span>
							<span class="text"><?php echo $cmau_options['global_menu']; ?></span>
						</a>
					</div>

					<div class="book">
						<a href="#" class="book-toggle" aria-expanded="false"><?php echo $cmau_options['global_book']; ?></a>
					</div>
				</div><!-- .navbar -->

				<div class="extra">
					<a href="<?php echo esc_url( $cmau_options['global_link']['url'] ); ?>" target="_blank" rel="noopener noreferrer">
						<?php echo $cmau_options['global_link']['text']; ?> <span class="fas fa-external-link-alt"></span>
					</a>
				</div>
			</header><!-- .site-header -->

			<div class="site-menu">
				<div class="offset-width"></div>
				<div class="wrapper">
					<nav class="sitenav">
						<?php club_menangle_primary_menu(); ?>
					</nav>

					<?php echo wp_get_attachment_image( $cmau_options['global_nav'], 'full', false, array( 'alt' => get_bloginfo( 'name' ) ) ); ?>
				</div>
			</div>

			<div class="site-book">
				<div class="offset-width"></div>
				<div class="wrapper">
					<?php get_template_part( 'template-parts/global', 'book' ); ?>
				</div>
			</div>

			<div id="site-content" class="site-content full-offset">
				<?php get_template_part( 'template-parts/global', 'banner' ); ?>
