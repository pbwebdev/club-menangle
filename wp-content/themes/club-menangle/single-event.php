<?php

/**
 * The template for displaying single event
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$events = new WP_Query( array(
	'post_type'      => 'event',
	'posts_per_page' => -1,
	'meta_key'       => 'cmau_event_date',
	'orderby'        => array(
		'meta_value' => 'ASC',
		'menu_order' => 'DESC',
		'post_date'  => 'ASC',
	),
) );

$events = $events->get_posts();

$p_id = get_the_ID();
$prev = false;
$next = false;
$ebid = get_post_meta( get_the_ID(), 'cmau_eventbrite_id', true );

if ( $ebid ) {
	wp_enqueue_script( 'cmau-ebwidget', 'https://www.eventbrite.com.au/static/widgets/eb_widgets.js', array(), CLUB_MENANGLE_VERSION, true );
	wp_add_inline_script( 'cmau-ebwidget', 'window.EBWidgets.createWidget({
		widgetType: "checkout",
		eventId: "' . $ebid . '",
		iframeContainerId: "eventbrite-widget-container-' . $ebid . '",
	});' );
}

$count = count( $events ) - 1;
$index = array_search( $p_id, array_column( $events, 'ID' ) );

if ( $index ) {
	$prev = $events[ $index - 1 ];
}

if ( $index < $count ) {
	$next = $events[ $index + 1 ];
}

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php
				the_post();

				$date     = get_post_meta( $p_id, 'cmau_event_date', true );
				$time     = get_post_meta( $p_id, 'cmau_event_time', true );
				$location = get_post_meta( $p_id, 'cmau_event_location', true );
				$uber     = get_post_meta( $p_id, 'cmau_event_uber', true );
				$price    = get_post_meta( $p_id, 'cmau_event_price', true );
				$book     = get_post_meta( $p_id, 'cmau_event_book', true );
				$images   = get_post_meta( $p_id, 'cmau_event_images', true );
				$venue 	  = get_post_meta( $p_id, 'cmau_event_venue', true );
				$packages = get_post_meta( $p_id, 'cmau_event_packages', true );
				$sections = get_post_meta( $p_id, 'cmau_event_sections', false );

				$uber = array_merge( array(
					'deeplink' => '',
					'linktext' => '',
				), (array) $uber );

				$packages = array_merge( array(
					'heading' => '',
					'items'   => array(),
				), (array) $packages );
			?>

			<article id="event-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="section cm-1 full-pad">
					<div class="text full-pad">
						<div class="full-pad">
							<h2><?php the_title(); ?></h2>

							<?php if ( $date || $location || $price ) : ?>
								<ul>
									<?php if ( $venue ) : ?>
										<li><span><i class="far fas fa-globe fa-fw"></i></span><?php echo $venue; ?></li>
									<?php endif; ?>
									<?php if ( $date ) : ?>
										<li><span><i class="far fa-calendar-alt fa-fw"></i></span><?php echo date( 'l jS F Y', strtotime( $date ) ); ?> <?php echo $time; ?></li>
									<?php endif; ?>
									<?php if ( $location ) : ?>
										<li><span><i class="fas fa-map-marker fa-fw"></i></span><?php echo $location; ?></li>
										<?php if ( $uber['deeplink'] && $uber['linktext'] ) : ?>
											<li class="uber-link"><span><i class="fab fa-uber fa-fw"></i></span><a href="<?php echo $uber['deeplink']; ?>"><?php echo $uber['linktext']; ?></a></li>
										<?php endif; ?>
									<?php endif; ?>
									<?php if ( $price ) : ?>
										<li><span><i class="fas fa-money-bill fa-fw"></i></span><pre><?php echo $price; ?></pre></li>
									<?php endif; ?>
								</ul>
							<?php endif; ?>

							<?php if ( $book ) : ?>
								<a href="<?php echo $book; ?>" class="btn btn-primary circle">Book</a>
							<?php endif; ?>
						</div>
					</div>

					<div class="image half-pad">
						<div class="holder">
							<?php
								if ( isset( $images['info'] ) ) {
									echo wp_get_attachment_image( $images['info'], 'tile_1-2' );
								} else {
									the_post_thumbnail( 'tile_1-2' );
								}
							?>
						</div>
					</div>
				</div>

				<div class="section cm-5 full-pad">
					<div class="image half-pad">
						<div class="holder">
							<?php
								if ( isset( $images['about'] ) ) {
									echo wp_get_attachment_image( $images['about'], 'tile_1-2' );
								} else {
									the_post_thumbnail( 'tile_1-2' );
								}
							?>
						</div>
					</div>

					<div class="text full-pad">
						<div class="full-pad">
							<h2>About the event</h2>

							<?php the_content(); ?>

							<?php if ( $book ) : ?>
								<a href="<?php echo $book; ?>" class="btn btn-primary circle">Book</a>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<?php foreach ( $sections as $index => $section ) : ?>
					<?php
						$section = array_merge( array(
							'info'  => '',
							'image' => '',
						), (array) $section );
					?>

					<?php if ( $section['info'] ) : ?>
						<?php if ( 0 === $index % 2 ) : ?>
							<div class="section cm-1 full-pad">
								<div class="text full-pad">
									<div class="inner full-pad">
										<?php echo apply_filters( 'the_content', $section['info'] ); ?>
									</div>
								</div>

								<div class="image half-pad">
									<div class="holder">
										<?php
											if ( isset( $section['image'] ) ) {
												echo wp_get_attachment_image( $section['image'], 'tile_1-2' );
											} else {
												the_post_thumbnail( 'tile_1-2' );
											}
										?>
									</div>
								</div>
							</div>
						<?php else : ?>
							<div class="section cm-5 full-pad">
								<div class="image half-pad">
									<div class="holder">
										<?php
											if ( isset( $section['image'] ) ) {
												echo wp_get_attachment_image( $section['image'], 'tile_1-2' );
											} else {
												the_post_thumbnail( 'tile_1-2' );
											}
										?>
									</div>
								</div>

								<div class="text full-pad">
									<div class="inner full-pad">
										<?php echo apply_filters( 'the_content', $section['info'] ); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				<?php endforeach; ?>

				<?php if ( $ebid ) : ?>
					<?php
						$eb_heading = get_post_meta( $p_id, 'cmau_eventbrite_heading', true );
						$eb_content = get_post_meta( $p_id, 'cmau_eventbrite_content', true );
					?>
					<div class="half-pad"></div>

					<div class="section cm-2 full-pad">
						<div class="image">
							<div id="eventbrite-widget-container-<?php echo $ebid; ?>" class="holder">
							</div>
						</div>

						<div class="text full-pad">
							<div class="full-pad">
								<div class="inner full-pad">
									<h2><?php echo $eb_heading; ?></h2>

									<?php echo apply_filters( 'the_content', $eb_content ); ?>

									<?php if ( $book ) : ?>
										<a href="<?php echo $book; ?>" class="btn btn-link">Find out more</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</article>
		<?php endwhile; ?>

		<?php if ( $packages['heading'] || $packages['items'] ) : ?>
			<div class="section full-pad">
				<?php if ( $packages['heading'] ) : ?>
					<h2 class="text-center"><?php echo $packages['heading']; ?></h2>
				<?php endif; ?>

				<?php if ( $packages['items'] ) : ?>
					<div class="row">

						<?php foreach ( $packages['items'] as $package ) : ?>
							<div class="col-md-4">
								<div class="card">
									<?php echo get_the_post_thumbnail( $package, 'tile_1-2' ); ?>

									<div class="card-body">
										<h3><?php echo get_the_title( $package ); ?></h3>

										<?php echo apply_filters( 'the_content', get_the_content( null, false, $package ) ); ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="section more-events full-pad">
			<div class="prev-next">
				<?php if ( $prev ) : ?>
					<?php
						$prev->event_date = get_post_meta( $prev->ID, 'cmau_event_date', true );
						$prev->overlay    = get_post_meta( $prev->ID, 'cmau_featured_overlay', true );

						if ( ! $prev->event_date ) {
							$prev->event_date = get_the_date( '', $prev->ID );
						}
					?>

					<div class="prev">
						<p class="label">Previous event</p>

						<article id="event-<?php echo $prev->ID; ?>" class="event-item">
							<a href="<?php echo get_the_permalink( $prev->ID ); ?>">
								<div class="image">
									<?php echo get_the_post_thumbnail( $prev->ID, 'tile-1_4' ); ?>

									<?php if ( $prev->overlay ) : ?>
										<pre class="overlay quarter-pad"><?php echo $prev->overlay; ?></pre>
									<?php endif; ?>
								</div>

								<div class="info quarter-pad">
									<p><?php echo date( 'j M', strtotime( $prev->event_date ) ); ?></p>
									<h3><?php echo $prev->post_title; ?></h3>
								</div>
							</a>
						</article>
					</div>
				<?php endif; ?>

				<?php if ( $next ) : ?>
					<?php
						$next->event_date = get_post_meta( $next->ID, 'cmau_event_date', true );
						$next->overlay    = get_post_meta( $next->ID, 'cmau_featured_overlay', true );

						if ( ! $next->event_date ) {
							$next->event_date = get_the_date( '', $next->ID );
						}
					?>

					<div class="next">
						<p class="label">Next event</p>

						<article id="event-<?php echo $next->ID; ?>" class="event-item">
							<a href="<?php echo get_the_permalink( $next->ID ); ?>">
								<div class="image">
									<?php echo get_the_post_thumbnail( $next->ID, 'tile-1_4' ); ?>

									<?php if ( $next->overlay ) : ?>
										<pre class="overlay quarter-pad"><?php echo $next->overlay; ?></pre>
									<?php endif; ?>
								</div>

								<div class="info quarter-pad">
									<p><?php echo date( 'j M', strtotime( $next->event_date ) ); ?></p>
									<h3><?php echo $next->post_title; ?></h3>
								</div>
							</a>
						</article>
					</div>
				<?php endif; ?>
			</div>

			<div class="sizer"></div>

			<div class="all quarter-pad">
				<a href="<?php echo get_post_type_archive_link( 'event' ); ?>" class="btn btn-secondary circle">All Events</a>
				<?php echo wp_get_attachment_image( $cmau_options['global_nav'], 'full', false, array( 'alt' => get_bloginfo( 'name' ) ) ); ?>
			</div>
		</div>

		<?php get_template_part( 'template-parts/event', 'enquiry' ); ?>

	</main><!-- .content -->

<?php

get_footer();
