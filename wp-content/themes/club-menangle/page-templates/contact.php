<?php

/**
 * Template Name: Contact Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

$api = $cmau_options['global_map'];

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php
				the_post();

				$pid     = get_the_ID();
				$content = get_the_content();
				$phone   = get_post_meta( $id, 'cmau_contact_phone', true );
				$time    = get_post_meta( $id, 'cmau_contact_time', true );
				$address = get_post_meta( $id, 'cmau_contact_address', true );
				$uber    = get_post_meta( $id, 'cmau_contact_uber', true );
				$postal  = get_post_meta( $id, 'cmau_contact_postal', true );
				$email   = get_post_meta( $id, 'cmau_contact_email', true );
				$image   = get_post_meta( $id, 'cmau_enquiry_image', true );
				$heading = get_post_meta( $id, 'cmau_enquiry_heading', true );
				$form    = get_post_meta( $id, 'cmau_enquiry_form', true );
				$lat     = get_post_meta( $id, 'cmau_map_lat', true );
				$lng     = get_post_meta( $id, 'cmau_map_lng', true );
				$zml     = get_post_meta( $id, 'cmau_map_zml', true );

				$uber = array_merge( array(
					'deeplink' => '',
					'linktext' => '',
				), (array) $uber );
			?>

			<?php if ( $content ) : ?>
				<div class="section full-pad container">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>

			<div class="section cm-1 full-pad">
				<div class="text full-pad">
					<div class="full-pad">
						<div class="inner full-pad">
							<h2><?php the_title(); ?></h2>

							<ul>
								<?php if ( $phone ) : ?>
									<?php if ( $time ) : ?>
										<?php $phone .= '<br>' . $time; ?>
									<?php endif; ?>
									<li><span><i class="fas fa-phone-alt fa-fw"></i></span><?php echo $phone; ?></li>
								<?php endif; ?>
								<?php if ( $address ) : ?>
									<li><span><i class="fas fa-map-marker fa-fw"></i></span><?php echo $address; ?></li>
									<?php if ( $uber['deeplink'] && $uber['linktext'] ) : ?>
										<li class="uber-link"><span><i class="fab fa-uber fa-fw"></i></span><a href="<?php echo $uber['deeplink']; ?>"><?php echo $uber['linktext']; ?></a></li>
									<?php endif; ?>
								<?php endif; ?>
								<?php if ( $postal ) : ?>
									<li><span><i class="fas fa-fax fa-fw"></i></span><?php echo $postal; ?></li>
								<?php endif; ?>
								<?php if ( $email ) : ?>
									<li><span><i class="far fa-envelope fa-fw"></i></span><?php echo $email; ?></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>

				<div class="image half-pad">
					<div id="cmau_map" class="holder" data-api="<?php echo $api; ?>" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-zml="<?php echo $zml; ?>" style="padding-top: 100%;"></div>
				</div>
			</div>

			<div class="enquiry section cm-2 full-pad">
				<div class="image">
					<div class="holder">
						<?php echo wp_get_attachment_image( $image, 'full', false, array( 'alt' => 'Enquiry ' . get_bloginfo( 'name' ) ) ); ?>
					</div>
				</div>

				<div class="text full-pad">
					<div class="form special full-pad">
						<?php if ( $heading ) : ?>
							<h3><?php echo $heading; ?></h3>
						<?php endif; ?>

						<?php if ( function_exists( 'gravity_form' ) && $form ) : ?>
							<p>&nbsp;</p>
							<?php gravity_form( $form, false, false, false, '', true ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>

	</main><!-- .content -->

	<div class="half-pad"></div>

<?php

get_footer();
