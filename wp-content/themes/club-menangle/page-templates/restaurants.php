<?php

/**
 * Template Name: Restaurants Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$p_id  = get_the_ID();
$query = new WP_Query(
	array(
		'post_parent'    => $p_id,
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	)
);

$n = 0;

$sections = get_post_meta( $p_id, 'cmau_page_sections', false );

get_header();

?>

	<main class="content">

		<?php
			while ( have_posts() ) {
				the_post();

				$content = get_the_content();

				if ( $content ) {
					echo '<div class="section full-pad container">';
					echo apply_filters( 'the_content', $content );
					echo '</div>';
				}
			}
		?>

		<?php while ( $query->have_posts() ) : ?>
			<?php
				$query->the_post();
				$n++;

				$gallery = get_post_meta( get_the_ID(), 'cmau_page_gallery', true );
				$gallery = array_merge( array(
					'images' => array(),
				), (array) $gallery );
			?>

			<?php if ( 1 === $n % 2 ) : ?>
				<div class="section cm-8 full-pad">
					<div class="gallery full-pad">
						<div class="wrapper full-pad">
							<div class="slider">
								<?php foreach ( $gallery['images'] as $image ) : ?>
									<div class="image">
										<?php echo wp_get_attachment_image( $image, 'tile-1_2' ); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>

					<div class="info full-pad">
						<div class="quarter-pad">
							<h2><?php the_title(); ?></h2>

							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn btn-link">Find out more</a>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="section cm-9 full-pad">
					<div class="info full-pad">
						<div class="quarter-pad">
							<h2><?php the_title(); ?></h2>

							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn btn-link">Find out more</a>
						</div>
					</div>

					<div class="gallery full-pad">
						<div class="wrapper full-pad">
							<div class="slider">
								<?php foreach ( $gallery['images'] as $image ) : ?>
									<div class="image">
										<?php echo wp_get_attachment_image( $image, 'tile-1_2' ); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>

		<?php endwhile; ?>
		<?php wp_reset_query(); ?>

		<?php foreach ( $sections as $index => $section ) : ?>
			<?php
				$section = array_merge( array(
					'image'   => '',
					'heading' => '',
					'content' => '',
					'link'    => array(),
				), $section );

				$section['link'] = array_merge( array(
					'url'  => '',
					'text' => '',
				), $section['link'] );
			?>

			<?php if ( 1 === ( $index + 1 ) % 2 ) : ?>
				<div class="section cm-8 full-pad">
					<div class="gallery full-pad">
						<div class="wrapper full-pad">
							<div class="slider">
								<?php foreach ( $section['images'] as $image ) : ?>
									<div class="image">
										<?php echo wp_get_attachment_image( $image, 'tile-1_2' ); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>

					<div class="info full-pad">
						<div class="quarter-pad">
							<h2><?php echo $section['heading']; ?></h2>
							<?php echo apply_filters( 'the_content', $section['content'] ); ?>
							<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text']; ?></a>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="section cm-9 full-pad">
					<div class="info full-pad">
						<div class="quarter-pad">
							<h2><?php echo $section['heading']; ?></h2>
							<?php echo apply_filters( 'the_content', $section['content'] ); ?>
							<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text']; ?></a>
						</div>
					</div>

					<div class="gallery full-pad">
						<div class="wrapper full-pad">
							<div class="slider">
								<?php foreach ( $section['images'] as $image ) : ?>
									<div class="image">
										<?php echo wp_get_attachment_image( $image, 'tile-1_2' ); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>

		<?php endforeach; ?>

	</main><!-- .content -->

	<div class="half-pad"></div>

<?php

get_footer();
