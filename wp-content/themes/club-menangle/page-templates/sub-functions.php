<?php

/**
 * Template Name: Sub-Functions
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$page_id  = get_the_ID();
$sections = get_post_meta( $page_id, 'cmau_page_sections', false );
$gallery  = get_post_meta( $page_id, 'cmau_page_gallery', true );
$gallery  = array_merge( array(
	'heading' => '',
	'images'  => array(),
), (array) $gallery );

get_header();

?>

	<main class="content">

		<?php
			while ( have_posts() ) {
				the_post();

				$content = get_the_content();

				if ( $content ) {
					echo '<div class="section full-pad container">';
					echo apply_filters( 'the_content', $content );
					echo '</div>';
				}
			}
		?>

		<?php foreach ( $sections as $index => $section ) : ?>
			<?php
				$section = array_merge( array(
					'image'   => '',
					'heading' => '',
					'content' => '',
					'link'    => array(),
				), $section );

				$section['link'] = array_merge( array(
					'url'  => '',
					'text' => '',
				), $section['link'] );
			?>

			<?php if ( 1 === ( $index + 1 ) ) : ?>
				<div class="section cm-1 full-pad">
					<div class="text full-pad">
						<div class="full-pad">
							<div class="inner full-pad">
								<?php if ( $section['heading'] ) : ?>
									<h2><?php echo $section['heading']; ?></h2>
								<?php endif; ?>

								<?php echo apply_filters( 'the_content', $section['content'] ); ?>

								<?php if ( $section['link']['url'] && $section['link']['text'] ) : ?>
									<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text']; ?></a>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="image half-pad">
						<div class="holder">
							<?php echo wp_get_attachment_image( $section['image'], 'tile_1-2' ); ?>
						</div>
					</div>
				</div>
			<?php elseif ( 0 === ( $index + 1 ) % 2 ) : ?>
				<div class="section cm-2 full-pad">
					<div class="image">
						<div class="holder">
							<?php echo wp_get_attachment_image( $section['image'], 'tile_1-2' ); ?>
						</div>
					</div>

					<div class="text full-pad">
						<div class="full-pad">
							<div class="inner full-pad">
								<?php if ( $section['heading'] ) : ?>
									<h2><?php echo $section['heading']; ?></h2>
								<?php endif; ?>

								<?php echo apply_filters( 'the_content', $section['content'] ); ?>

								<?php if ( $section['link']['url'] && $section['link']['text'] ) : ?>
									<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text']; ?></a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="section cm-3 full-pad">
					<div class="text full-pad">
						<?php if ( $section['heading'] ) : ?>
							<h2><?php echo $section['heading']; ?></h2>
						<?php endif; ?>

						<?php echo apply_filters( 'the_content', $section['content'] ); ?>

						<?php if ( $section['link']['url'] && $section['link']['text'] ) : ?>
							<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text']; ?></a>
						<?php endif; ?>
					</div>

					<div class="image half-pad">
						<div class="holder">
							<?php echo wp_get_attachment_image( $section['image'], 'tile_1-2' ); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>

		<?php endforeach; ?>

		<?php if ( $gallery['heading'] || $gallery['images'] ) : ?>
			<div class="section cm-4 full-pad">
				<div class="quarter-pad">
					<?php if ( $gallery['heading'] ) : ?>
						<h2><?php echo $gallery['heading']; ?></h2>
					<?php endif; ?>
				</div>

				<div class="gallery">
					<div class="images">
						<?php foreach ( $gallery['images'] as $image ) : ?>
							<div class="image">
								<?php echo wp_get_attachment_image( $image, 'full' ); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/function', 'enquiry' ); ?>

	</main><!-- .content -->

<?php

get_footer();
