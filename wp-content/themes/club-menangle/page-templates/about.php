<?php

/**
 * Template Name: About Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php
				the_post();

				$pid      = get_the_ID();
				$content  = get_the_content();
				$sections = get_post_meta( $id, 'cmau_about_sections', false );

				foreach ( $sections as &$section ) {
					$section = array_merge( array(
						'image'   => '',
						'heading' => '',
						'content' => '',
					), $section );
				}
			?>

			<?php if ( $content ) : ?>
				<div class="section full-pad container">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>

			<div class="section cm-1 full-pad">
				<div class="text full-pad">
					<div class="full-pad">
						<div class="inner full-pad">
							<?php if ( $sections[0]['heading'] ) : ?>
								<h2><?php echo $sections[0]['heading']; ?></h2>
							<?php endif; ?>

							<?php echo apply_filters( 'the_content', $sections[0]['content'] ); ?>
						</div>
					</div>
				</div>

				<div class="image half-pad">
					<div class="holder">
						<?php echo wp_get_attachment_image( $sections[0]['image'], 'tile_1-2' ); ?>
					</div>
				</div>
			</div>

			<div class="section cm-2 full-pad">
				<div class="image">
					<div class="holder">
						<?php echo wp_get_attachment_image( $sections[1]['image'], 'tile_1-2' ); ?>
					</div>
				</div>

				<div class="text full-pad">
					<div class="full-pad">
						<div class="inner full-pad">
							<?php if ( $sections[1]['heading'] ) : ?>
								<h2><?php echo $sections[1]['heading']; ?></h2>
							<?php endif; ?>

							<?php echo apply_filters( 'the_content', $sections[1]['content'] ); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="section cm-3 full-pad">
				<div class="text full-pad">
					<?php if ( $sections[2]['heading'] ) : ?>
						<h2><?php echo $sections[2]['heading']; ?></h2>
					<?php endif; ?>

					<?php echo apply_filters( 'the_content', $sections[2]['content'] ); ?>
				</div>

				<div class="image half-pad">
					<div class="holder">
						<?php echo wp_get_attachment_image( $sections[2]['image'], 'tile_1-2' ); ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>

	</main><!-- .content -->

<?php

get_footer();
