<?php

/**
 * Template Name: Racing Schedule
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<div class="section full-pad container racing-schedule">
				<?php the_post(); ?>
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>

	</main><!-- .content -->

	<div class="quarter-pad"></div>

<?php

get_footer();
