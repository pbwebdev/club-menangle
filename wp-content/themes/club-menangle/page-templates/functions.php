<?php

/**
 * Template Name: Functions Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$theID = get_the_ID();

$query = new WP_Query(
	array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	)
);

$n = 0;

get_header();

?>

	<main class="content">

		<?php
			while ( have_posts() ) {
				the_post();

				$content = get_the_content();

				if ( $content ) {
					echo '<div class="section full-pad container">';
					echo apply_filters( 'the_content', $content );
					echo '</div>';
				}
			}
		?>

		<?php while ( $query->have_posts() ) : ?>
			<?php
				$query->the_post();
				$n++;

				$zigzag = get_post_meta( get_the_ID(), 'cmau_zigzag_function_button', false )[0];

				$links_label = ( $zigzag['link']['text'] ? $zigzag['link']['text'] : 'Find out more' );
				$links = $zigzag['link']['url'];

			?>

			<?php if ( 1 === $n ) : ?>
				<div class="section cm-1 full-pad">
					<div class="text full-pad">
						<div class="full-pad">
							<div class="inner full-pad">
								<h2><?php the_title(); ?></h2>

								<?php the_excerpt(); ?>

								<?php if($links) {?>

									<a href="<?php echo $links; ?>" class="btn btn-link"><?php echo $links_label; ?></a>

								<?php } else { ?>

									<a href="<?php the_permalink(); ?>" class="btn btn-link"><?php echo $links_label; ?></a>

								<?php } ?>

							</div>
						</div>
					</div>

					<div class="image half-pad">
						<div class="holder">
							<?php the_post_thumbnail( 'tile_1-2' ); ?>
						</div>
					</div>
				</div>
			<?php elseif ( 0 === $n % 2 ) : ?>
				<div class="section cm-2 full-pad">
					<div class="image">
						<div class="holder">
							<?php the_post_thumbnail( 'tile_1-2' ); ?>
						</div>
					</div>

					<div class="text full-pad">
						<div class="full-pad">
							<div class="inner full-pad">
								<h2><?php the_title(); ?></h2>

								<?php the_excerpt(); ?>

								<?php if($links) {?>

									<a href="<?php echo $links; ?>" class="btn btn-link"><?php echo $links_label; ?></a>

								<?php } else { ?>

									<a href="<?php the_permalink(); ?>" class="btn btn-link"><?php echo $links_label; ?></a>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="section cm-3 full-pad">
					<div class="text full-pad">
						<h2><?php the_title(); ?></h2>

						<?php the_excerpt(); ?>

						<?php if($links) {?>

							<a href="<?php echo $links; ?>" class="btn btn-link"><?php echo $links_label; ?></a>

						<?php } else { ?>

							<a href="<?php the_permalink(); ?>" class="btn btn-link"><?php echo $links_label; ?> </a>

						<?php } ?>
					</div>

					<div class="image half-pad">
						<div class="holder">
							<?php the_post_thumbnail( 'tile_1-2' ); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>

		<div class="half-pad"></div>

	</main><!-- .content -->

<?php

get_footer();
