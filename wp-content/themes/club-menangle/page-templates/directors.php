<?php

/**
 * Template Name: Directors Page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'director',
		'posts_per_page' => -1,
	)
);

get_header();

?>

	<main class="content">

		<?php
			while ( have_posts() ) {
				the_post();

				$content = get_the_content();

				if ( $content ) {
					echo '<div class="section full-pad container">';
					echo apply_filters( 'the_content', $content );
					echo '</div>';
				}
			}
		?>

		<?php if ( $query->have_posts() ) : ?>
			<div class="directors full-pad">
				<?php while ( $query->have_posts() ) : ?>
					<?php
						$query->the_post();
						$d_id     = get_the_ID();
						$position = get_post_meta( $d_id, 'cmau_director_position', true );
					?>

					<div class="director quarter-pad">
						<div class="image">
							<?php the_post_thumbnail( 'full' ); ?>
						</div>

						<h3 class="name"><?php the_title(); ?></h3>
						<p class="position"><span><?php echo $position; ?></span></p>

						<div class="info">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</div>
		<?php endif; ?>


	</main><!-- .content -->

<?php

get_footer();
