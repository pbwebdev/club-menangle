<?php

/**
 * The template for displaying the footer
 *
 * @package Club Menangle
 * @since 0.1.0
 */

?>

				<?php
					if ( ! is_front_page() ) {
						get_template_part( 'template-parts/global', 'sponsors' );
						get_template_part( 'template-parts/global', 'instagram' );
					}
				?>
				<?php get_template_part( 'template-parts/global', 'subcon' ); ?>

			</div><!-- .site-content -->

			<footer class="site-footer full-offset">
				<nav class="sitelinks">
					<?php club_menangle_footer_menu(); ?>
				</nav>

				<div class="copyright">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a> &copy; <?php echo esc_html( gmdate( 'Y' ) ); ?> <span>|</span> Experience by <a href="https://pbwebdev.com" target="_blank" rel="noopener noreferrer" title="Custom WordPress Design & Development">PB Web Development</a>
				</div>
			</footer><!-- .site-footer -->
		</div><!-- .site-wrapper -->

		<?php wp_footer(); ?>

		<noscript>
			<style type="text/css">
				.no-js .loading-wrapper {
					transform: translateY( -100% );
				}

				.no-js body {
					overflow: unset;
				}

				.no-js .main-toggle,
				.no-js .book-toggle {
					pointer-events: none;
				}
			</style>
		</noscript>

		<!-- Hotjar Tracking Code for http://www.clubmenangle.com.au/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1831067,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>


	</body>
</html>

<!-- A generator-themeplate project -->
