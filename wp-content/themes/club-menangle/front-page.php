<?php

/**
 * The template for displaying the front page
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$feats = get_post_meta( get_the_ID(), 'cmau_featured_pages', false );

get_header();

?>

	<main class="content">
		<?php get_template_part( 'template-parts/front', 'super' ); ?>

		<?php if ( ! empty( $feats ) ) : ?>
			<div class="section featured">
				<?php foreach ( $feats as $page ) : ?>
					<?php
						$page = array_merge( array(
							'heading' => '',
							'image'   => '',
							'link'    => array(),
						), $page );
					?>
					<div class="page">
						<h2 class="quarter-pad"><?php echo $page['heading']; ?></h2>

						<div class="tile">
							<?php echo wp_get_attachment_image( $page['image'], 'tile-1_3' ); ?>
							<a href="<?php echo $page['link']['url']; ?>"><div class="btn circle"><span><?php echo $page['link']['text']; ?></span></div></a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="quarter-pad"></div>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/front', 'events' ); ?>
		<?php get_template_part( 'template-parts/global', 'sponsors' ); ?>
		<?php get_template_part( 'template-parts/front', 'about' ); ?>


	</main><!-- .content -->

<?php

get_footer();
