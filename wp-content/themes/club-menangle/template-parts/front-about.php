<?php

/**
 * The template for displaying the about section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$pid = get_post_meta( get_the_ID(), 'cmau_featured_about', true );

if ( ! $pid ) {
	return;
}

$about = get_post( $pid );

if ( ! $about || ( ! has_post_thumbnail( $pid ) ) ) {
	return;
}

$super   = get_post_meta( $pid, 'cmau_banner_super', true );
$heading = get_post_meta( $pid, 'cmau_banner_heading', true );
$content = get_post_meta( $pid, 'cmau_banner_content', true );

if ( ! $heading ) {
	$heading = get_the_title( $pid );
}

if ( ! $content ) {
	$content = $about->post_excerpt;
}

?>

<div class="section about banner full-pad">
	<div class="container-fluid">
		<div class="content quarter-pad">
			<?php if ( $super ) : ?>
				<p class="super"><?php echo $super; ?></p>
			<?php endif; ?>

			<h2><?php echo $heading; ?></h2>

			<?php echo apply_filters( 'the_content', $content ); ?>
		</div>

		<div class="button quarter-pad">
			<a href="<?php echo get_the_permalink( $pid ); ?>" class="btn btn-secondary circle">Read more</a>
		</div>
	</div>
</div>
