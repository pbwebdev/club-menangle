<?php

/**
 * The template for displaying the enquiry section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$event_options = get_option( 'cmau-event' );

if ( ! $event_options['enquiry_heading'] && ! $event_options['enquiry_form'] ) {
	return;
}


?>

<div class="enquiry section cm-2 full-pad">
	<div class="image">
		<div class="holder">
			<?php
				if ( $event_options['enquiry_images'] ) {

					$rand = rand( 0, count( $event_options['enquiry_images'] ) - 1 );

					echo wp_get_attachment_image( $event_options['enquiry_images'][ $rand ], 'full', false, array( 'alt' => 'Enquiry ' . get_bloginfo( 'name' ) ) );
				}
			?>
		</div>
	</div>

	<div class="text full-pad">
		<div class="form full-pad">
			<div class="inner full-pad">
				<?php if ( $event_options['enquiry_heading'] ) : ?>
					<h3><?php echo $event_options['enquiry_heading']; ?></h3>
				<?php endif; ?>

				<?php if ( function_exists( 'gravity_form' ) && $event_options['enquiry_form'] ) : ?>
					<p>&nbsp;</p>
					<?php gravity_form( $event_options['enquiry_form'], false, false, false, '', true ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="half-pad"></div>
