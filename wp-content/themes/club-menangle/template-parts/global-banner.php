<?php

/**
 * The template for displaying the banner
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$images = $heading = $content = null;

if ( is_archive() ) {
	$object = get_queried_object();

	$images  = get_option( 'cmau-' . $object->name )['banner_image'];
	$super   = get_option( 'cmau-' . $object->name )['banner_super'];
	$heading = get_option( 'cmau-' . $object->name )['banner_heading'];
	$content = get_option( 'cmau-' . $object->name )['banner_content'];
	$button  = get_option( 'cmau-' . $object->name )['banner_button'];
	$scroll  = get_option( 'cmau-' . $object->name )['banner_scroll'];

	if ( ! $heading ) {
		$heading = $object->labels->archives;
	}
} else {
	$p_id = get_the_ID();

	if ( is_home() ) {
		$p_id = get_option( 'page_for_posts' );
	}

	$super   = get_post_meta( $p_id, 'cmau_banner_super', true );
	$heading = get_post_meta( $p_id, 'cmau_banner_heading', true );
	$content = get_post_meta( $p_id, 'cmau_banner_content', true );
	$button  = get_post_meta( $p_id, 'cmau_banner_button', true );
	$images  = get_post_meta( $p_id, 'cmau_banner_images', true );
	$scroll  = get_post_meta( $p_id, 'cmau_banner_scroll', true );

	if ( ! $heading ) {
		$heading = single_post_title( '', false );
	}

	if ( ! $images && has_post_thumbnail( $p_id ) ) {
		$images = get_post_thumbnail_id( $p_id );
	}

	if ( 'event' === get_post_type( $p_id ) && ! $super ) {
		$date = get_post_meta( $p_id, 'cmau_event_date', true );
		$time = get_post_meta( $p_id, 'cmau_event_time', true );

		if ( $date ) {
			$super = date( 'jS M Y', strtotime( $date ) ) . ' ' . $time;
		}
	}
}

$button = array_merge( array(
	'url'  => '',
	'text' => '',
), (array) $button );

?>

<div class="banner full-pad">
	<div class="container-fluid">
		<div class="content quarter-pad">
			<?php if ( $super ) : ?>
				<p class="super"><?php echo $super; ?></p>
			<?php endif; ?>

			<?php if ( $heading ) : ?>
				<h1><?php echo $heading; ?></h1>
			<?php endif; ?>

			<?php if ( $content ) : ?>
				<p><?php echo $content; ?></p>
			<?php endif; ?>
		</div>

		<div class="button quarter-pad">
			<?php if ( $button && $button['url'] && $button['text'] ) : ?>
				<a href="<?php echo $button['url']; ?>" class="btn btn-primary circle"><?php echo $button['text']; ?></a>
			<?php endif; ?>
		</div>
	</div>

	<?php if ( $images ) {
		$images = (array) $images;

		if ( count( $images ) > 1 ) {
			echo '<div class="images">';
		}

		foreach ( $images as $image ) {
			echo '<div class="image">' . wp_get_attachment_image( $image, 'full', false, array( 'data-unlazy' => 'true' ) ) . '</div>';
		}

		if ( count( $images ) > 1 ) {
			echo '</div>';
		}
	} ?>

	<?php
		if($scroll != 2) {
	?>
		<a href="#" class="scroll-down">
			<span class="fas fa-angle-double-down fa-3x animate"></span>
			<p>Scroll down to find more</p>
		</a>
	<?php
		}
	?>

</div><!-- .banner -->
