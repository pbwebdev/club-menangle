<?php

/**
 * The template for displaying the enquiry section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$page_id = get_the_ID();
$contact = get_post_meta( $page_id, 'cmau_page_contact', true );

if ( empty( $contact ) ) {
	return;
}

$contact = array_merge( array(
	'heading' => '',
	'content' => '',
	'form'    => '',
), $contact );

?>

<div class="enquiry section full-pad">
	<div class="text">
		<div class="inner full-pad">
			<div class="full-pad">
				<div class="half-pad">
					<?php if ( $contact['heading'] ) : ?>
						<h3><?php echo $contact['heading']; ?></h3>
					<?php endif; ?>

					<?php echo apply_filters( 'the_content', $contact['content'] ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form full-pad">
		<div class="quarter-pad">
			<?php if ( function_exists( 'gravity_form' ) && $contact['form'] ) : ?>
				<p>&nbsp;</p>
				<?php gravity_form( $contact['form'], false, false, false, '', true ); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
