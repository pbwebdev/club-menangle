<?php

/**
 * The template for displaying the events section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$count = 0;
$event = get_post_type_object( 'event' );
$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'event',
		'posts_per_page' => -1,
		'orderby'        => array(
			'menu_order' => 'DESC',
			'post_date'  => 'ASC',
		),
	)
);

if ( ! $query->have_posts() ) {
	return;
}

$class= 'event-carousel half-pad';

if ( $query->found_posts > 3 ) {
	$class .= ' slider';
}

?>

<div class="section events">
	<div class="intro quarter-pad">
		<h2><span><a href="<?php echo get_post_type_archive_link( 'event' ); ?>"><?php echo $event->labels->archives; ?></a></span></h2>
	</div>

	<div class="<?php echo $class; ?>">
		<?php while ( $query->have_posts() ) : ?>
			<?php
				$query->the_post();
				$count++;

				$date    = get_post_meta( get_the_ID(), 'cmau_event_date', true );
				$overlay = get_post_meta( get_the_ID(), 'cmau_featured_overlay', true );
				$venue   = get_post_meta( get_the_ID(), 'cmau_event_venue', true );

				if ( ! $date ) {
					$date = get_the_date();
				}
			?>

			<article id="event-<?php the_ID(); ?>" class="event-item">
				<a href="<?php the_permalink(); ?>">
					<div class="image">
						<?php the_post_thumbnail( 'tile-1_4' ); ?>

						<?php if ( $overlay ) : ?>
							<pre class="overlay quarter-pad"><?php echo $overlay; ?></pre>
						<?php endif; ?>
					</div>

					<div class="info quarter-pad">
						<p><?php echo date( 'j M', strtotime( $date ) ); ?></p>
						<h3><?php the_title(); ?></h3>
						<p class="venue"><?php echo $venue; ?></p>
					</div>
				</a>
			</article>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	</div>
</div>
