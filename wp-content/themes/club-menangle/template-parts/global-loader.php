<?php

/**
 * The template for displaying the loader
 *
 * @package Club Menangle
 * @since 0.1.0
 */

?>

<div class="loading-wrapper">
	<span class="bar"></span>
	<span class="bar"></span>
	<span class="bar"></span>
</div>

<style type="text/css">
	body:not( .loaded ) {
		overflow: hidden;
	}

	.loading-wrapper {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 10000;
		background-color: #edece3;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
			flex-wrap: wrap;
		-ms-flex-pack: center;
			justify-content: center;
		-ms-flex-align: center;
			align-items: center;
		transition: transform 0.4s;
		transition-delay: 0.4s;
	}

	.loading-wrapper .bar {
		height: 4.5rem;
		width: 0.75rem;
		background-color: #373636;
		animation: loader 1s infinite ease-in-out;
	}

	.loading-wrapper .bar + .bar {
		margin-left: 0.1875rem;
	}

	.loading-wrapper .bar:first-child {
		animation-delay: -1.2s;
	}

	.loading-wrapper .bar:last-child {
		animation-delay: -0.8s;
	}

	.loaded .loading-wrapper {
		transform: translateY( -100% );
	}

	@keyframes loader {
		0%, 50%, 100% {
			transform: scaleY(1);
		}
		25% {
			transform: scaleY(1.75);
		}
	}
</style>
