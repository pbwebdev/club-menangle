<?php

/**
 * The template for displaying the sponsors
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$args = array(
	'post_status'    => 'publish',
	'post_type'      => 'sponsor',
	'posts_per_page' => -1,
);

$query = new WP_Query( $args );

if ( ! $query->have_posts() ) {
	return;
}

global $cmau_options;

$heading = $cmau_options['sponsors_heading'];
$number  = $cmau_options['sponsors_number'];

$i = 0;

?>

<div class="sponsors section">
	<div class="container">
		<?php if ( $heading ) : ?>
			<h3><?php echo $heading; ?></h3>
		<?php endif; ?>

		<?php if ( $number < $query->found_posts ) : ?>
			<div class="sponsors-carousel"><div>
		<?php endif; ?>

			<ul class="sponsors-list">
				<?php while ( $query->have_posts() ) : ?>
					<?php
						$query->the_post();
						$i++;

					switch ( get_post_meta( get_the_ID(), 'cmau_sponsor_logo', true ) ) {
						default:
						case 1:
							$size = 'sponsor-boxed';
							break;

						case 2:
							$size = 'sponsor-wide';
							break;
					}

						$link = get_post_meta( get_the_ID(), 'cmau_sponsor_link', true );
					?>

					<li>
						<?php if ( $link ) : ?>
							<a href="<?php echo $link; ?>" target="_blank" rel="noopener noreferrer"><?php the_post_thumbnail( $size ); ?></a>
						<?php else : ?>
							<?php the_post_thumbnail( $size ); ?>
						<?php endif; ?>
					</li>

					<?php if ( 0 === $i % $number && $query->found_posts !== $i ) : ?>
						</ul><ul class="sponsors-list">
					<?php endif; ?>
				<?php endwhile; ?>
			</ul>

		<?php if ( $number < $query->found_posts ) : ?>
			</div></div>
		<?php endif; ?>

		<?php wp_reset_query(); ?>
	</div>
</div><!-- .sponsors -->
