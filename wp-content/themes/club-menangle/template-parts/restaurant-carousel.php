<?php

/**
 * The template for displaying the restaurant carousel
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$carousel = get_post_meta( get_the_ID(), 'cmau_function_carousel', true );

if ( empty( $carousel ) ) {
	return;
}

$carousel = array_merge( array(
	'heading' => '',
	'content' => '',
	'link'    => array(),
	'images'  => array(),
), $carousel );

$carousel['link'] = array_merge( array(
	'url'  => '',
	'text' => '',
), $carousel['link'] );

?>

<div class="super section cm-8 full-pad">
	<div class="gallery full-pad">
		<div class="wrapper full-pad">
			<div class="slider">
				<?php foreach ( $carousel['images'] as $image ) : ?>
					<div class="image">
						<?php echo wp_get_attachment_image( $image, 'tile-1_2' ); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="info full-pad">
		<h2><?php echo $carousel['heading']; ?></h2>
		<?php echo apply_filters( 'the_content', $carousel['content'] ); ?>

		<?php if ( $carousel['link']['url'] && $carousel['link']['text'] ) : ?>
			<div class="button">
				<a href="<?php echo $carousel['link']['url']; ?>" class="btn btn-link"><?php echo $carousel['link']['text'] ?></a>
			</div>
		<?php endif; ?>
	</div>
</div>
