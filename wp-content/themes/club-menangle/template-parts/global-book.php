<?php

/**
 * The template for displaying the booking widget
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

if ( isset( $cmau_options['global_eat'] ) ) {
	$query = new WP_Query(
		array(
			'post_parent'    => $cmau_options['global_eat'],
			'post_type'      => 'page',
			'posts_per_page' => -1,
			'post_status'    => 'publish',
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
		)
	);
}

?>

<div class="head quarter-pad">
	<p>Booking in progress...</p>

	<a href="#" class="book-close" aria-expanded="false">
		<span class="lines">
			<i class="line"></i>
			<i class="line"></i>
		</span>
		<span class="text">Close</span>
	</a>
</div>

<form class="restaurant-select">
	<div class="body quarter-pad">
		<label for="book-restaurant">Where would you like to eat?</label>
		<select id="book-restaurant" name="restaurant">
			<option disabled hidden selected>&mdash; Select &mdash;</option>
			<?php if ( isset( $cmau_options['global_eat'] ) && $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : ?>
					<?php $query->the_post(); ?>
					<option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
		</select>
	</div>
	<div class="foot quarter-pad">
		<input type="hidden" name="action" value="cmau_book" />
		<button type="submit" class="btn btn-link" disabled>Next</button>
	</div>
</form>

<div class="restaurant-widget quarter-pad"></div>
