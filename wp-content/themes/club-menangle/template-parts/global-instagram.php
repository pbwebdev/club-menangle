<?php

/**
 * The template for displaying the instagram section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$media = cmau_get_instagram_data();

if ( ! $media ) {
	return;
}

global $cmau_options;

$handle = $cmau_options['instagram_handle'];

?>

<div class="section instagram">
	<?php if ( $handle ) : ?>
		<p class="quarter-pad"><a href="https://www.instagram.com/<?php echo $handle; ?>" target="_blank" rel="noopener noreferrer"><span class="fab fa-instagram"></span><span><?php echo '@' . ltrim( $handle, '@' ); ?></span></a></p>
	<?php endif; ?>

	<div class="feed">
		<?php for ( $i = 0; $i < 6; $i++ ) : ?>
			<div class="tile">
				<a href="<?php echo $media[ $i ]['permalink']; ?>" target="_blank" rel="noopener noreferrer"><img class="lazyload" data-src="<?php echo $media[ $i ]['media_type'] === 'VIDEO' ? $media[ $i ]['thumbnail_url'] : $media[ $i ]['media_url']; ?>"></a>
			</div>
		<?php endfor; ?>
	</div>
</div>
