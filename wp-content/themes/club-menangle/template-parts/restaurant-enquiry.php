<?php

/**
 * The template for displaying the enquiry section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$page_id = get_the_ID();
$contact = get_post_meta( $page_id, 'cmau_page_contact', true );

if ( empty( $contact ) ) {
	return;
}

global $cmau_options;

$contact = array_merge( array(
	'heading' => '',
	'content' => '',
	'book'    => '',
	'label'    => '',
	'map'     => array(),
), $contact );

$contact['map'] = array_merge( array(
	'lat' => '',
	'lng' => '',
	'zml' => '',
), $contact['map'] );

$api = $cmau_options['global_map'];
$lat = $contact['map']['lat'];
$lng = $contact['map']['lng'];
$zml = $contact['map']['zml'];

?>

<div class="enquiry section">
	<div class="text full-pad">
		<div class="inner full-pad">
			<div class="full-pad">
				<div class="half-pad">
					<?php if ( $contact['heading'] ) : ?>
						<h3><?php echo $contact['heading']; ?></h3>
					<?php endif; ?>

					<?php echo apply_filters( 'the_content', $contact['content'] ); ?>
				</div>
			</div>

			<?php if ( $contact['book'] ) : ?>
				<?php
					if($contact['label']) {
				?>
					<a href="<?php echo $contact['book']; ?>" class="btn btn-primary small circle"><?php echo (($contact['label']) ? $contact['label'] : "BOOK" ); ?></a>
				<?php } else { ?>
					<a href="<?php echo $contact['book']; ?>" class="btn btn-primary circle">BOOK</a>
				<?php } ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="image">
		<?php if ( $api && $lat && $lng ) : ?>
			<div id="cmau_map" class="holder" data-api="<?php echo $api; ?>" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-zml="<?php echo $zml; ?>"></div>
		<?php endif; ?>
	</div>
</div>
