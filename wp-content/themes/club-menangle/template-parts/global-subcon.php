<?php

/**
 * The template for displaying the subscribe-contact section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

global $cmau_options;

if ( ! $cmau_options['subscribe_heading'] && ! $cmau_options['subscribe_form'] && ! $cmau_options['contact_heading'] ) {
	return;
}

?>

<div class="subscribe-contact">
	<div class="subscribe full-pad">
		<?php echo wp_get_attachment_image( $cmau_options['subscribe_logo'], 'full', false, array( 'alt' => get_bloginfo( 'name' ) ) ); ?>

		<?php if ( $cmau_options['subscribe_heading'] ) : ?>
			<h3><?php echo $cmau_options['subscribe_heading']; ?></h3>
		<?php endif; ?>

		<?php if ( $cmau_options['subscribe_content'] ) : ?>
			<p><?php echo $cmau_options['subscribe_content']; ?></p>
		<?php endif; ?>

		<?php if ( function_exists( 'gravity_form' ) && $cmau_options['subscribe_form'] ) : ?>
			<p>&nbsp;</p>
			<?php gravity_form( $cmau_options['subscribe_form'], false, false, false, '', true ); ?>
		<?php endif; ?>
	</div>

	<div class="contact full-pad">
		<?php if ( $cmau_options['subscribe_heading'] ) : ?>
			<h3><?php echo $cmau_options['contact_heading']; ?></h3>
		<?php endif; ?>

		<?php echo apply_filters( 'the_content', $cmau_options['contact_content'] ); ?>

		<?php if ( ! empty( $cmau_options['social_profiles'] ) ) : ?>
			<ul class="socials">
				<?php foreach ( $cmau_options['social_profiles'] as $social ) : ?>
					<li><a href="<?php echo $social['link']; ?>" target="_blank" rel="noopener noreferrer" aria-label="<?php echo ucfirst( $social['provider'] ); ?>"><span class="fab fa-<?php echo $social['provider']; ?>"></span></a></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>

		<?php
		echo wp_get_attachment_image( $cmau_options['contact_background'], 'full', false, array(
			'alt'   => get_bloginfo( 'name' ),
			'class' => 'background',
		) );
		?>
	</div>
</div>
