<?php

/**
 * The template for displaying the events section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$count = 0;
$event = get_post_type_object( 'event' );
$query = new WP_Query(
	array(
		'post_status'    => 'publish',
		'post_type'      => 'event',
		'posts_per_page' => 5,
		'orderby'        => array(
			'menu_order' => 'DESC',
			'post_date'  => 'ASC',
		),
	)
);

if ( ! $query->have_posts() ) {
	return;
}

$class = 'block';

if ( '1' === $query->found_posts ) {
	$class .= ' single';
}

?>

<div class="section events event-gallery">
	<div class="intro quarter-pad">
		<h2><span><?php echo $event->labels->archives; ?></span></h2>
	</div>

	<div class="<?php echo $class; ?>">
		<?php while ( $query->have_posts() ) : ?>
			<?php
				$query->the_post();
				$count++;

				$date    = get_post_meta( get_the_ID(), 'cmau_event_date', true );
				$overlay = get_post_meta( get_the_ID(), 'cmau_featured_overlay', true );

				if ( ! $date ) {
					$date = get_the_date();
				}

				$size = 1 === $count ? 'tile-1_2' : 'tile-1_4';
			?>

			<article id="event-<?php the_ID(); ?>" class="event-item">
				<a href="<?php the_permalink(); ?>">
					<div class="image">
						<?php the_post_thumbnail( 'tile-1_2' ); ?>

						<?php if ( $overlay ) : ?>
							<pre class="overlay quarter-pad"><?php echo $overlay; ?></pre>
						<?php endif; ?>
					</div>

					<div class="info quarter-pad">
						<p><?php echo date( 'j M', strtotime( $date ) ); ?></p>
						<h3><?php the_title(); ?></h3>
					</div>
				</a>
			</article>

			<?php if ( $query->found_posts > 2 ) : ?>
				<?php if ( 1 === $count ) : ?>
					<div class="event-item block">
				<?php endif; ?>

				<?php if ( $query->post_count === $count ) : ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>

		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	</div>
</div>
