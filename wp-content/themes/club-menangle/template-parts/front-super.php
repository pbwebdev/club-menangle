<?php

/**
 * The template for displaying the super section
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$supers = get_post_meta( get_the_ID(), 'cmau_featured_supers', false );

if ( empty( $supers ) ) {
	return;
}

if ( is_front_page() ) {
	$isactive = 'animated';
} else {
	$isactive = '';
}

?>

<div class="supers section full-pad <?php echo $isactive; ?>">
	<?php foreach ( $supers as $section ) : ?>
		<?php
			$section = array_merge( array(
				'heading' => '',
				'content' => '',
				'link'    => array(),
				'image'   => '',
			), $section );

			$section['link'] = array_merge( array(
				'url'  => '',
				'text' => '',
			), $section['link'] );
		?>

		<div class="page">
			<div class="info full-pad">
				<h2><a href="<?php echo $section['link']['url']; ?>" class="title-section"><?php echo $section['heading']; ?></a></h2>
				<?php echo apply_filters( 'the_content', $section['content'] ); ?>
			</div>

			<div class="image full-pad">
				<?php if ( $section['link']['url'] && $section['link']['text'] ) : ?>
					<div class="button">
						<a href="<?php echo $section['link']['url']; ?>" class="btn btn-link"><?php echo $section['link']['text'] ?></a>
					</div>
				<?php endif; ?>

				<div class="holder">
					<?php echo wp_get_attachment_image( $section['image'], 'tile_1-2' ); ?>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>
