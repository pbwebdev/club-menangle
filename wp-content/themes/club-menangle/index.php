<?php

/**
 * The main template file
 *
 * @package Club Menangle
 * @since 0.1.0
 */

get_header();

?>

	<main class="content">

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="entry-wrap full-pad container">
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>

						<div class="entry-meta">
							<time><?php the_date(); ?></time>
							<span><?php the_author(); ?></span>
						</div>
					</header>

					<div class="entry-content half-pad px-0">
						<?php the_excerpt(); ?>
					</div>

					<footer class="entry-footer">
						<?php the_tags(); ?>
					</footer>
				</div>
			</article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; ?>

	</main><!-- .content -->

<?php

get_footer();
