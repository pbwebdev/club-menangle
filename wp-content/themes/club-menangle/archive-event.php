<?php

/**
 * The template for displaying the event archive
 *
 * @package Club Menangle
 * @since 0.1.0
 */

$archive = false;
$p_title = 'Upcoming Events';

if ( get_query_var( 'at-archive' ) ) {
	$archive = true;
	$p_title = 'Archived Events';

	add_filter( 'document_title_parts', function( $parts ) {
		global $p_title;

		$parts['title'] = $p_title;

		return $parts;
	} );
}

get_header();

$args = array(
	'posts_per_page' => -1,
	'meta_key'       => 'cmau_event_date',
);

if ( ! $archive ) {
	$args['orderby'] = array(
		'meta_value' => 'ASC',
		'menu_order' => 'DESC',
		'post_date'  => 'ASC',
	);
} else {
	$args['orderby'] = array(
		'meta_value' => 'DESC',
		'menu_order' => 'ASC',
		'post_date'  => 'DESC',
	);
}

query_posts( add_query_arg( $args, $query_string ) );

?>

	<main class="content">

		<?php if ( have_posts() ) : ?>
			<h2 class="full-pad"><?php echo $p_title; ?></h1>

			<div class="event-list full-pad">
				<?php while ( have_posts() ) : ?>
					<?php
						the_post();

						$date    = get_post_meta( get_the_ID(), 'cmau_event_date', true );
						$time    = get_post_meta( get_the_ID(), 'cmau_event_time', true );
						$overlay = get_post_meta( get_the_ID(), 'cmau_featured_overlay', true );
						$venue = get_post_meta( get_the_ID(), 'cmau_event_venue', true );

						if ( ! $date ) {
							$date = get_the_date();
						}
					?>

					<article id="event-<?php the_ID(); ?>" class="event-item">
						<a href="<?php the_permalink(); ?>">
							<div class="image">
								<?php the_post_thumbnail( 'tile-1_2' ); ?>

								<?php if ( $overlay ) : ?>
									<pre class="overlay quarter-pad"><?php echo $overlay; ?></pre>
								<?php endif; ?>
							</div>

							<div class="info quarter-pad">
								<p><?php echo date( 'j M', strtotime( $date ) ); ?> <?php echo $time; ?></p>
								<h3><?php the_title(); ?></h3>
								<p class="venue"><?php echo $venue; ?></p>
							</div>
						</a>
					</article>
				<?php endwhile; ?>
			</div>

		<?php endif; ?>

	</main><!-- .content -->

<?php

get_footer();
