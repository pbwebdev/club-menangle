<?php

/**
 * Enqueue scripts and styles
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'club_menangle_scripts_styles_early' ) ) {
	function club_menangle_scripts_styles_early() {
		$suffix = ( SCRIPT_DEBUG || CLUB_MENANGLE_THEME_DEBUG ) ? '' : '.min';
		$theme  = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		// Deregister the jquery version bundled with WordPress
		wp_deregister_script( 'jquery-core' );
		wp_deregister_script( 'jquery-migrate' );
		// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header
		wp_register_script( 'jquery-core', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery' . $suffix . '.js', array(), '2.2.4', false );
		wp_register_script( 'jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate' . $suffix . '.js', array(), '1.4.1', false );
		// wp_add_inline_script( 'jquery-core', 'jQuery.noConflict();' );

		// jQuery
		wp_enqueue_script( 'jquery-core' );
		// Font Awesome
		wp_enqueue_script( 'club_menangle-fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js', array(), '5.13.0', false );
		// wp_add_inline_script( 'club_menangle-fontawesome', 'FontAwesomeConfig = { searchPseudoElements: true };' );
		// jQuery.appear
		wp_enqueue_script( 'club_menangle-appear', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.4.1/jquery.appear' . $suffix . '.js', array(), '0.4.1', true );
		// lazysizes
		wp_enqueue_script( 'club_menangle-lazysizes', 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.2.0/lazysizes.min.js', array(), '5.2.0', true );
		// wp_add_inline_script( 'club_menangle-lazysizes', 'window.lazySizesConfig = window.lazySizesConfig || {}; window.lazySizesConfig.preloadAfterLoad = true;', 'before' );
		// Slick Carousel
		wp_enqueue_style( 'club_menangle-slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick' . $suffix . '.css', array(), '1.9.0' );
		wp_enqueue_script( 'club_menangle-slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick' . $suffix . '.js', array(), '1.9.0', true );

		wp_register_script( 'dummy-handle', '', array(), '', true );
		wp_enqueue_script( 'dummy-handle' );
		wp_add_inline_script( 'dummy-handle', 'jQuery.noConflict(); window.FontAwesomeConfig = { searchPseudoElements: true }; window.lazySizesConfig = window.lazySizesConfig || {}; window.lazySizesConfig.preloadAfterLoad = true;' );
	}
	add_action( 'wp_enqueue_scripts', 'club_menangle_scripts_styles_early', 5 );
}

if ( ! function_exists( 'club_menangle_scripts_styles_late' ) ) {
	function club_menangle_scripts_styles_late() {
		$suffix = ( SCRIPT_DEBUG || CLUB_MENANGLE_THEME_DEBUG ) ? '' : '.min';
		$theme  = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		// Site scripts and styles
		wp_enqueue_style( 'club_menangle-style', CLUB_MENANGLE_THEME_URL . 'assets/css/club-menangle' . $suffix . '.css', array(), $theme->get( 'Version' ) );
		wp_enqueue_script( 'club_menangle-script', CLUB_MENANGLE_THEME_URL . 'assets/js/club-menangle' . $suffix . '.js', array(), $theme->get( 'Version' ), true );

		$cmau_options = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'club_menangle-script', 'cmau_options', apply_filters( 'club_menangle_localize_script', $cmau_options ) );
	}
	add_action( 'wp_enqueue_scripts', 'club_menangle_scripts_styles_late', 20 );
}

// Async Scripts
if ( ! function_exists( 'club_menangle_async_scripts' ) ) {
	function club_menangle_async_scripts( $tag, $handle ) {
		// Add script handles
		$scripts = array(
			'club_menangle-appear',
			'club_menangle-lazysizes',
			'club_menangle-slick',
			'club_menangle-script',
		);

		if ( in_array( $handle, $scripts, true ) ) {
			return str_replace( ' src', ' async="async" src', $tag );
		}

		return $tag;
	}
	add_filter( 'script_loader_tag', 'club_menangle_async_scripts', 10, 2 );
}

// Defer Scripts
if ( ! function_exists( 'club_menangle_defer_scripts' ) ) {
	function club_menangle_defer_scripts( $tag, $handle ) {
		// Add script handles
		$scripts = array(
			'club_menangle-fontawesome',
		);

		if ( in_array( $handle, $scripts, true ) ) {
			return str_replace( ' src', ' defer="defer" src', $tag );
		}

		return $tag;
	}
	add_filter( 'script_loader_tag', 'club_menangle_defer_scripts', 10, 2 );
}
