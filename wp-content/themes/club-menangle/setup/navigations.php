<?php

/**
 * Register navigation menus
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'club_menangle_navigations' ) ) {
	function club_menangle_navigations() {
		// Bootstrap Nav Walker
		require_once CLUB_MENANGLE_THEME_PATH . 'includes/class-bootstrap-navbar.php';

		register_nav_menus( array(
			'primary-1' => __( 'Primary Col-1', 'club-menangle' ),
			'primary-2' => __( 'Primary Col-2', 'club-menangle' ),
			'primary-3' => __( 'Primary Col-3', 'club-menangle' ),
			'primary-4' => __( 'Primary Col-4', 'club-menangle' ),
			'footer'    => __( 'Footer Menu', 'club-menangle' ),
		) );
	}
	add_action( 'after_setup_theme', 'club_menangle_navigations' );
}

// Primary Menu
if ( ! function_exists( 'club_menangle_primary_menu' ) ) {
	function club_menangle_primary_menu() {
		for ( $i = 1; $i <= 4; $i++ ) {
			wp_nav_menu( array(
				'theme_location' => 'primary-' . $i,
				'menu_class'     => 'menu stacked',
				'depth'          => 0,
			) );
		}
	}
}

// Footer Menu
if ( ! function_exists( 'club_menangle_footer_menu' ) ) {
	function club_menangle_footer_menu() {
		wp_nav_menu( array(
			'theme_location' => 'footer',
			'menu_class'     => 'menu inline',
			'depth'          => 1,
		) );
	}
}
