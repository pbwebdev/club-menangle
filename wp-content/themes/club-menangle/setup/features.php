<?php

/**
 * Register theme features
 *
 * @package Club Menangle
 * @since 0.1.0
 */

if ( ! function_exists( 'club_menangle_setup' ) ) {
	function club_menangle_setup() {
		// Make theme available for Translation
		load_theme_textdomain( 'club-menangle', CLUB_MENANGLE_THEME_PATH . 'languages' );
		// Add theme support for Post Formats
		add_theme_support( 'post-formats', array( 'link', 'image', 'quote', 'video', 'audio' ) );
		// Add theme support for Featured Images
		add_theme_support( 'post-thumbnails' );
		// Add theme support for Automatic Feed Links
		add_theme_support( 'automatic-feed-links' );
		// Add theme support for HTML5 Semantic Markup
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
		// Add theme support for document Title tag
		add_theme_support( 'title-tag' );

		// Add theme image sizes
		add_image_size( 'max', 1920, 1080 );
		add_image_size( 'tile-1_2', 1060, 795, true );
		add_image_size( 'tile-1_3', 720, 720, true );
		add_image_size( 'tile-1_4', 560, 560, true );
		add_image_size( 'sponsor-wide', 250, 150, false );
		add_image_size( 'sponsor-boxed', 150, 150, false );
	}
	add_action( 'after_setup_theme', 'club_menangle_setup' );
}

if ( ! function_exists( 'club_menangle_credit' ) ) {
	function club_menangle_credit() {
		$theme = wp_get_theme( CLUB_MENANGLE_THEME_BASE );

		return sprintf(
			'<a href="%1$s" target="_blank" rel="noopener noreferrer">%2$s %3$s</a> %4$s <span class="dashicons dashicons-heart"></span> by <a href="%5$s" target="_blank" rel="noopener noreferrer">%6$s</a>.',
			$theme->get( 'ThemeURI' ),
			$theme->get( 'Name' ),
			$theme->get( 'Version' ),
			__( 'designed and developed with', 'club-menangle' ),
			$theme->get( 'AuthorURI' ),
			$theme->get( 'Author' )
		);
	}
	// Add to the admin footer
	add_filter( 'admin_footer_text', 'club_menangle_credit' );
}

if ( ! function_exists( 'club_menangle_updates' ) ) {
	function club_menangle_updates( $value ) {
		unset( $value->response[ get_stylesheet() ] );
		return $value;
	}
	// Disable update notification from WordPress.org repository theme
	add_filter( 'pre_set_site_transient_update_themes', 'club_menangle_updates' );
}
