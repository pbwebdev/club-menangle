<?php

/**
 * Club Menangle functions and definitions
 *
 * @package Club Menangle
 * @since 0.1.0
 */

/* generator-themeplate v1.13.0 */

/*
 * ==================================================
 * Global constants
 * ==================================================
 */
// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma
define( 'CLUB_MENANGLE_THEME_BASE',  basename( __DIR__ ) );
define( 'CLUB_MENANGLE_THEME_URL',   get_theme_root_uri( basename( __DIR__ ) ) . '/' . basename( __DIR__ ) . '/' );
define( 'CLUB_MENANGLE_THEME_PATH',  get_theme_root( basename( __DIR__ ) ) . '/' . basename( __DIR__ ) . '/' );
define( 'CLUB_MENANGLE_THEME_DEBUG', false );
// phpcs:enable


/*
 * ==================================================
 * Setup Theme
 * ==================================================
 */

require_once 'setup/features.php';
require_once 'setup/navigations.php';
require_once 'setup/scripts-styles.php';
require_once 'setup/actions-filters.php';


/*
 * ==================================================
 * Extra custom functions
 * ==================================================
 */

$cmau_options = get_option( 'cmau-options' );

add_filter( 'gform_init_scripts_footer', '__return_true' );
add_filter( 'gform_cdata_open', 'cmau_gform_cdata_open', 1 );
add_filter( 'gform_cdata_close', 'cmau_gform_cdata_close', 99 );
add_filter( 'gform_force_hooks_js_output', '__return_true' );

function cmau_gform_cdata_open( $content = '' ) {
	if ( ! do_cmau_gform_cdata() ) {
		return $content;
	}
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ' . $content;
	return $content;
}

function cmau_gform_cdata_close( $content = '' ) {
	if ( ! do_cmau_gform_cdata() ) {
		return $content;
	}
	$content .= ' }, false );';
	return $content;
}

function do_cmau_gform_cdata() {
	if ( is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) || isset( $_GET['gf_page'] ) || doing_action( 'wp_footer' ) || did_action( 'wp_footer' ) ) {
		return false;
	}

	return true;
}


function cmau_fallback_event_date( $post_id, $post ) {
	if ( 'event' !== $post->post_type ) {
		return;
	}

	$date = get_post_meta( $post_id, 'cmau_event_date', true );

	if ( $date ) {
		return;
	}

	$date = explode( ' ', $post->post_date );

	update_post_meta( $post_id, 'cmau_event_date', $date[0] );
}
add_action( 'save_post', 'cmau_fallback_event_date', 20, 2 );


function cmau_adjacent_post_where( $sql ) {
	if ( ! is_singular( 'event' ) ) {
		return $sql;
	}

	$the_post       = get_post( get_the_ID() );
	$patterns       = array();
	$patterns[]     = '/post_date/';
	$patterns[]     = '/\'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\'/';
	$replacements   = array();
	$replacements[] = 'menu_order';
	$replacements[] = $the_post->menu_order;

	return preg_replace( $patterns, $replacements, $sql );
}
add_filter( 'get_next_post_where', 'cmau_adjacent_post_where' );
add_filter( 'get_previous_post_where', 'cmau_adjacent_post_where' );

function cmau_adjacent_post_sort( $sql ) {
	if ( ! is_singular( 'event' ) ) {
		return $sql;
	}

	$pattern     = '/post_date/';
	$replacement = 'menu_order';

	return preg_replace( $pattern, $replacement, $sql );
}
add_filter( 'get_next_post_sort', 'cmau_adjacent_post_sort' );
add_filter( 'get_previous_post_sort', 'cmau_adjacent_post_sort' );


function cmau_image_attributes( $attr ) {
	if ( is_admin() || isset( $attr['data-unlazy'] ) ) {
		return $attr;
	}

	$attr['data-src'] = $attr['src'];

	unset( $attr['src'] );

	if ( isset( $attr['srcset'] ) ) {
		$attr['data-srcset'] = $attr['srcset'];

		unset( $attr['srcset'] );
	}

	if ( isset( $attr['class'] ) ) {
		$attr['class'] .= ' lazyload';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'cmau_image_attributes' );


function cmau_get_restaurant_widget() {
	$code = get_post_meta( $_GET['restaurant'], 'cmau_page_thefork', true );

	if ( strpos( $code, 'bookingWidgetLoaded( this )' ) === false ) {
		$code = str_replace( '<iframe ', '<iframe onload="bookingWidgetLoaded( this )" ', $code );
	}

	$code = preg_replace( '/<link(.*)href="(.*)height=(\d+)(.*)"(.*)>/', '<link$1href="$2height=400$4"$5>', $code );

	echo json_encode( $code );

	wp_die();
}
add_action( 'wp_ajax_cmau_book', 'cmau_get_restaurant_widget' );
add_action( 'wp_ajax_nopriv_cmau_book', 'cmau_get_restaurant_widget' );

function cmau_get_download_link( $atts, $content ) {

	$args = shortcode_atts( array (
				'id' => $atts['id'],
				'url' => $atts['url'],
				'newtab' => $atts['newtab'],
				'text' => $atts['label']
				), $atts
			);

	if($args['newtab']) {
		$newtab = $args['newtab'];
	}

	if($args['text']) {
		$label = $args['text'];
	}

	if($args['url']) {

		$link = $args['url'];

	} else {

		if ( empty( $args['id'] ) ) {
			return;
		}

		$links = get_post_meta( get_the_ID(), 'cmau_download_links', false );

		if ( empty( $links ) ) {
			return;
		}

		$index = array_search(
			strtolower( $atts['id'] ),
			array_map( 'strtolower', array_column( $links, 'text' ) )
		);

		if ( false === $index ) {
			return;
		}

		if ( empty( $links[ $index ]['url'] ) ) {
			return;
		}

		$link = $links[ $index ]['url'];
		$label = $links[ $index ]['text'];
	}

	$newtab = (($newtab == '1') ? 'target="_blank" rel="noopener noreferrer"' : '');

	return '<a href="' . $link . '" class="btn btn-primary circle download" '.$newtab.'><svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"><path fill="currentColor" d="M18.562 18.25c.47 0 .868-.164 1.196-.492.328-.328.492-.727.492-1.196v-4.5c0-.468-.164-.867-.492-1.195a1.627 1.627 0 00-1.196-.492h-3.234l1.617-1.617c.352-.352.522-.756.51-1.213a1.704 1.704 0 00-.492-1.178c-.317-.328-.72-.492-1.213-.492H13.5V1.938c0-.47-.164-.868-.492-1.196A1.627 1.627 0 0011.812.25H8.437c-.468 0-.867.164-1.195.492a1.627 1.627 0 00-.492 1.196v3.937H4.5a1.57 1.57 0 00-1.178.492c-.316.328-.48.72-.492 1.178a1.66 1.66 0 00.475 1.213l1.617 1.617H1.687c-.468 0-.867.164-1.195.492A1.627 1.627 0 000 12.063v4.5c0 .468.164.867.492 1.195.328.328.727.492 1.195.492h16.875zm-8.437-5.062L4.5 7.563h3.937V1.938h3.375v5.625h3.938l-5.625 5.625zm8.437 3.374H1.687v-4.5H6.61l2.32 2.32c.329.329.727.493 1.196.493s.867-.164 1.195-.492l2.32-2.32h4.922v4.5zm-2.25-1.406a.814.814 0 00.598-.246.814.814 0 00.246-.597.814.814 0 00-.246-.598.814.814 0 00-.598-.246.814.814 0 00-.597.246.814.814 0 00-.246.598c0 .234.082.433.246.597a.814.814 0 00.597.246z"/></svg>' . $label . '</a>';
}
add_shortcode( 'cm_download', 'cmau_get_download_link' );


function cmau_refresh_cache() {
	update_option( 'cmau-w3tc', substr( md5( time() ), 0, 5 ) );
	delete_transient( 'cmau-instagram' );
}
add_action( 'w3tc_flush_all', 'cmau_refresh_cache' );

function cmau_w3tc_filename( $minify_filename ) {
	$key = get_option( 'cmau-w3tc' );

	return str_replace( '.', $key . '.', $minify_filename );
}
add_filter( 'w3tc_minify_urls_for_minification_to_minify_filename', 'cmau_w3tc_filename' );


function cmau_get_instagram_data() {
	$cache = get_transient( 'cmau-instagram' );

	if ( false === $cache ) {
		global $cmau_options;

		$token = $cmau_options['instagram_token'];

		if ( $token ) {
			$request = add_query_arg( array(
				'fields'       => 'id,media_url,media_type,thumbnail_url,permalink',
				'access_token' => $token,
			), 'https://graph.instagram.com/me/media' );

			$response = wp_remote_get( $request );
			$res_body = wp_remote_retrieve_body( $response );
			$decoded  = json_decode( $res_body, true );

			if ( isset( $decoded['data'] ) ) {
				$cache = $decoded['data'];

				set_transient( 'cmau-instagram', $cache, 1 * HOUR_IN_SECONDS );
			}
		}
	}

	return $cache;
}
