/*!
 *  Club Menangle 0.1.0
 *  Copyright (C) 2020 Gene Alyson Fortunado Torcende
 *  Licensed under GPL-2.0-only.
 */

(function ($) {
	'use strict';

	$ = $ && Object.prototype.hasOwnProperty.call($, 'default') ? $['default'] : $;

	// External jQuery

	$(document).ready(function () {
	  // eslint-disable-next-line no-console
	  console.log('Everything is ready. ThemePlate!');
	  $('body').addClass('loaded').delay(800).queue(function (next) {
	    $('.site-content > .banner').addClass('animated');
	    next();
	  }); // sticky scroll during scroll up

	  var didScroll = false;
	  var lastScrollTop = 0;
	  var delta = 5;
	  var navbarHeight = $('header.site-header').outerHeight();
	  $(window).scroll(function () {
	    didScroll = true;
	  });
	  setInterval(function () {
	    if (didScroll) {
	      hasScrolled();
	      didScroll = false;
	    }
	  }, 250);

	  function hasScrolled() {
	    var st = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop || document.scrollingElement;

	    if (Math.abs(lastScrollTop - st) <= delta) {
	      return;
	    }

	    if (st > lastScrollTop && st > navbarHeight) {
	      $('header.site-header').removeClass('nav-down').addClass('nav-up').addClass('sticky-header');
	    } else if (st + $(window).height() < $(document).height()) {
	      $('header.site-header').removeClass('nav-up').addClass('nav-down');
	    }

	    if (st < 160) {
	      $('header.site-header').removeClass('sticky-header');
	    }

	    if (st < 260) {
	      $('header.site-header').removeClass('sticky-header');
	    }

	    if (st < 380) {
	      $('header.site-header').removeClass('sticky-header');
	    }

	    lastScrollTop = st;
	  }
	});
	$(window).on('unload', function () {
	  $(this).scrollTop(0);
	});
	$('.main-toggle').on('click', function (e) {
	  e.preventDefault();

	  if ($('body').hasClass('menu-open')) {
	    $('body').removeClass('menu-open');
	    $(this).attr('aria-expanded', 'false').find('.text').text('Menu');
	  } else {
	    $('body').addClass('menu-open');
	    $(this).attr('aria-expanded', 'true').find('.text').text('Close');
	  }

	  if ($('.site-header').hasClass('sticky-header')) {
	    $('.site-header').toggleClass('sticky-header');
	  }
	});
	$('.book-toggle, .book-close').on('click', function (e) {
	  e.preventDefault();

	  if ($('body').hasClass('book-open')) {
	    $('body').removeClass('book-open');
	    $(this).attr('aria-expanded', 'false');
	    $('.restaurant-select').show('400', function () {
	      $('.restaurant-widget').html('');
	    });
	  } else {
	    $('.restaurant-widget').hide();
	    $('body').addClass('book-open');
	    $(this).attr('aria-expanded', 'true');
	  }
	});
	$('.scroll-down').on('click', function (e) {
	  e.preventDefault();
	  var headerHeight = $('.site-header').outerHeight();
	  var bannerHeight = $('.site-content > .banner').outerHeight();
	  $('html, body').animate({
	    scrollTop: headerHeight + bannerHeight
	  }, 800);
	});
	setInterval(function () {
	  $('.scroll-down svg').toggleClass('animate');
	}, 3000);
	$('.gform_button').css('margin', 0).after($('<i class="fas fa-chevron-right"></i>'));

	if (document.getElementById('cmau_map')) {
	  var initMap = function initMap() {
	    var mapOptions = {
	      center: new google.maps.LatLng($('#cmau_map').data('lat'), $('#cmau_map').data('lng')),
	      zoom: Number($('#cmau_map').data('zml'))
	    };
	    var map = new google.maps.Map(document.getElementById('cmau_map'), mapOptions);
	    new google.maps.Marker({
	      map: map,
	      position: new google.maps.LatLng($('#cmau_map').data('lat'), $('#cmau_map').data('lng'))
	    });
	  };

	  window.initMap = initMap;
	  $(window).on('load', function () {
	    var script = document.createElement('script');
	    script.type = 'text/javascript';
	    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + $('#cmau_map').data('api') + '&callback=initMap';
	    document.body.appendChild(script);
	  });
	}

	function initSlick() {
	  if (typeof $.fn.slick === 'function') {
	    $('.banner .images').slick({
	      rows: 0,
	      slidesToShow: 1,
	      prevArrow: false
	    });
	    $('.cm-4 .images').slick({
	      rows: 0,
	      arrows: false,
	      dots: true,
	      slidesToShow: 1,
	      slidesToScroll: 1
	    });
	    $('.gallery .slider').each(function () {
	      $(this).addClass('primary').clone().removeClass('primary').addClass('secondary').appendTo($(this).parents('.wrapper'));
	    });
	    $('.gallery .slider.primary').each(function () {
	      $(this).slick({
	        rows: 0,
	        dots: true,
	        draggable: false,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        initialSlide: $(this).parents('.section').hasClass('cm-9')
	      }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
	        var slide = nextSlide + 1;

	        if ($(this).parents('.section').hasClass('cm-9')) {
	          slide = nextSlide - 1;
	        }

	        if (slide < 0) {
	          slide = slick.slideCount - 1;
	        }

	        $(this).siblings('.slider').slick('slickGoTo', slide);
	      });
	    });
	    $('.gallery .slider.secondary').each(function () {
	      $(this).slick({
	        rows: 0,
	        arrows: false,
	        draggable: false,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        initialSlide: $(this).parents('.section').hasClass('cm-8')
	      });
	    });
	    $('.event-carousel.slider').slick({
	      rows: 0,
	      arrows: false,
	      slidesToShow: 3,
	      responsive: [{
	        breakpoint: 992,
	        settings: {
	          slidesToShow: 2
	        }
	      }, {
	        breakpoint: 576,
	        settings: {
	          slidesToShow: 1
	        }
	      }]
	    });
	    window.clearInterval(checkSlick);
	  }
	}

	var checkSlick = window.setInterval(initSlick, 200);

	function initAppear() {
	  if (typeof $.fn.appear === 'function') {
	    $('.section, .event-item, .director').appear().on('appear', function () {
	      $(this).addClass('animated');
	      $('.scroll-down').addClass('hide');
	    });
	    window.clearInterval(checkAppear);
	  }
	}

	var checkAppear = window.setInterval(initAppear, 200);
	$('#book-restaurant').on('change', function () {
	  $('.restaurant-select button').removeAttr('disabled');
	});
	$('.restaurant-select').on('submit', function (e) {
	  e.preventDefault();
	  var $this = $(this);
	  $.ajax({
	    url: cmau_options.ajaxurl,
	    data: $this.serialize(),
	    beforeSend: function beforeSend() {
	      $this.hide();
	      $('.restaurant-widget').show().html('<div class="booking-spinner"><i class="fas fa-spinner fa-spin fa-5x fa-fw"></i></div>');
	    },
	    success: function success(response) {
	      $('.restaurant-widget').append(JSON.parse(response));
	    },
	    error: function error() {
	      // eslint-disable-next-line no-console
	      console.log('There has been an error');
	    }
	  });
	}); // eslint-disable-next-line no-unused-vars

	window.bookingWidgetLoaded = function (caller) {
	  jQuery('.booking-spinner').remove();
	  jQuery(caller).parents('.dimmi-widget').show(400, function () {
	    jQuery(this).animate({
	      opacity: 1
	    }, 200);
	  }).end().show(400, function () {
	    jQuery(this).animate({
	      opacity: 1
	    }, 200);
	  });
	};

}(jQuery));

//# sourceMappingURL=club-menangle.js.map
